/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cnee.GestionDoc.ctrl;


import cnee.GestionDoc.bl.*;
import cnee.GestionDoc.model.*;
import cnee.GestionDoc.dal.*;
import cnee.GestionDoc.util.Util;

import org.omg.CORBA.portable.UnknownException;

import org.zkoss.zk.ui.Component;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Button;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Listbox;
import java.sql.SQLException;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Messagebox;

import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Window;
/*** 
* @author root
* */ 

public class CatobjetoMantCtrl  extends GenericForwardComposer  { 

private static final long serialVersionUID = 1L; 
private Label lblUser;
private Groupbox grxFormaInOut; 

private Listbox lbxCatalogo;
private Tabbox tbox;

private Button btnGuardar;
private Button btnEliminar;
private Button btnLimpiar;
private Combobox cbxTipoObjeto;
private Combobox cbxActivo;
	
	private Textbox txtIdobjeto;
	private Textbox txtIdtipoobjeto;
	private Textbox txtNombre;
	private Textbox txtDescripcion;
	private Textbox txtActivo;
	private Textbox txtFechaingreso;
	private Textbox txtUsuarioingreso;
	private Textbox txtFechamodificacion;
	private Textbox txtUsuariomodificacion;
	private Textbox txtIdpadre;
	private Textbox txtUrl;
        Util Utilitario = new Util();
        

	@Override 
	public void doAfterCompose(Component comp) throws Exception { 
            super.doAfterCompose(comp);
            SegobjetoDal datoGraba = new SegobjetoDal();
            
            Session TicketSession = Sessions.getCurrent();
            String op = TicketSession.getAttribute("OP").toString(); 
            String sQuery = "select id_tipo_objeto, descripcion from seg_tipo_objeto where activo = 1";
            Utilitario.cargaCombox(sQuery, cbxTipoObjeto);
           
            
            if (op.equals("NEW")) {
                this.btnEliminar.setVisible(false);
                this.txtIdobjeto.setText(String.valueOf(datoGraba.NextValue()));
            } else {
               String IdPerfil = TicketSession.getAttribute("IdPerfil").toString();  
               
               SegobjetoMd dato;
               dato = datoGraba.busca(Integer.parseInt(IdPerfil));
               this.txtIdobjeto.setText(String.valueOf(dato.getId_objeto()));
               this.txtIdtipoobjeto.setText(String.valueOf(dato.getId_tipo_objeto()));
               this.txtNombre.setText(dato.getNombre());
               this.txtDescripcion.setText(dato.getDescripcion());
               this.txtActivo.setText(String.valueOf(dato.getActivo()));
               this.txtUrl.setText(dato.getUrl());
               this.txtIdpadre.setText(String.valueOf(dato.getId_padre()));
               this.cbxActivo.setSelectedIndex(dato.getActivo());
               SegtipoobjetoDal Obj = new SegtipoobjetoDal();
               SegtipoobjetoMd ObjDato;
               ObjDato = Obj.busca(dato.getId_tipo_objeto());
               this.cbxTipoObjeto.setValue(ObjDato.getDescripcion());
               this.cbxActivo.setSelectedIndex(dato.getActivo());
            }
	}

       
	public void LimpiarCampos() 
	{
		 txtIdobjeto.setValue(null);
		 txtIdtipoobjeto.setValue(null);
		 txtNombre.setValue(null);
		 txtDescripcion.setValue(null);
		 txtActivo.setValue(null);
		 txtIdpadre.setValue(null);
		 txtUrl.setValue(null);
	} 
        
         
	public void onClick$btnLimpiar(Event evt){ 
	     LimpiarCampos();
	}
       
                  
        public void onClick$linkSalir(Event evt){
            Window win = (Window)this.self ;
            win.detach() ;
	}
	
        public void onClick$btnGuardar(Event evt) throws SQLException{ 
            Guardar(); 
            Window win = (Window)this.self ;
            win.detach() ;
  	}

	public void onClick$btnEliminar(Event evt) throws SQLException{ 
            Eliminar_Pregunta();
              Window win = (Window)this.self ;
             win.detach() ;
	}
        
      
	public void Eliminar_Pregunta() { 
		Messagebox.show("Esta seguro de eliminar ?","SELECCIONE UNA OPCION", Messagebox.OK | Messagebox.NO, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() { 
		@Override
		public void onEvent(Event evt) throws InterruptedException, SQLException {
			if (evt.getName().equals("onOK")) { 
				Eliminar();
			}
			}
		});
	}
        
     public void Eliminar() 
	{ 
		SegobjetoDal datoBuscado = new SegobjetoDal(); 
		int iResp; 
		if(!txtIdobjeto.getText().equals("")){
		try 
		{ 
		 iResp = datoBuscado.Eliminar ( Integer.parseInt(this.txtIdobjeto.getText())); 
		 if (iResp > 0) {  
				 this.LimpiarCampos(); 
				 Messagebox.show(" Eliminado con Exito ");
		   } else { 
				 Messagebox.show(" Error en Eliminacion ");
			 } 
		} 
			catch (SQLException e ) {  
			Messagebox.show(" error: +" +e.getMessage()); 
			} 
		}else{Messagebox.show(" --INGRESE CODIGO A ELIMINAR-- ");
}	}

     
        
      public void Guardar() 
	{ 
	int iResp; 
		if(!txtIdobjeto.getText().equals("")){
		SegobjetoMd datoCrear = new SegobjetoMd(Integer.parseInt(txtIdobjeto.getText()),
                                                        Integer.parseInt(this.cbxTipoObjeto.getSelectedItem().getValue().toString()),
                                                        txtNombre.getText(),
                                                        txtDescripcion.getText(),
                                                        Integer.parseInt(this.cbxActivo.getSelectedItem().getValue().toString()),
                                                        null,
                                                        null,
                                                        null,
                                                        null,
                                                        Integer.parseInt(txtIdpadre.getText()),
                                                        txtUrl.getText());
                
		SegobjetoDal datoGraba = new SegobjetoDal();
		SegobjetoBl datoValida = new SegobjetoBl();
		String sMensaje = datoValida.Validar(datoCrear);		
		if (sMensaje == "") { 
			 try {
				 String sAccion = " Creado ";
				 if (datoGraba.existe(Integer.parseInt(txtIdobjeto.getText())) == true ) 
				 {
					  sAccion = " Actualizado ";
					 iResp = datoGraba.Modificar(datoCrear);
				 } else {
					 iResp = datoGraba.Crear(datoCrear);
				 } 
				 if (iResp > 0) { 
					     this.LimpiarCampos(); 
					 Messagebox.show(sAccion + " con Exito ");
				 } else { 
					 Messagebox.show(" Error en " + sAccion);
				 } 
				 } 
			   catch (SQLException e ) {  
				  Messagebox.show("Error: " + e.getMessage());
			 } 
			  catch (UnknownException e ) {  
				  Messagebox.show("Error: " + e.getMessage());
			 } 
				 } else { 
				  Messagebox.show(sMensaje);
				 } 
		 } else { 
				  Messagebox.show("--INGRESE CODIGOS--");
				 } 
		 } 


} 