

package cnee.GestionDoc.ctrl;

import cnee.GestionDoc.model.*;
import cnee.GestionDoc.dal.*;
import org.zkoss.zk.ui.Component;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Button;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Listbox;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;
/*** 
* @author root
* */ 

public class CatusuarioCtrl  extends GenericForwardComposer  { 

private static final long serialVersionUID = 1L; 
private Label lblUser;
private Groupbox grxFormaInOut; 

private Listbox lbxCatalogo;
private Tabbox tbox;
private Button btnGuardar;
private Button btnBuscar;
private Button btnEliminar;
private Button btnLimpiar;

private Intbox txtId;
private Textbox txtDepencod;
private Textbox txtTema;
private Textbox txtTemades;
private Textbox txtClasif;
private Textbox txtActivo;
private Textbox txtUsuarioingreso;
private Textbox txtFechaingreso;
private Textbox txtFechamodificacion;
private Textbox txtUsuariomodificacion;

      
	@Override 
	public void doAfterCompose(Component comp) throws Exception { 
            super.doAfterCompose(comp);
            this.llena_grid();
	}
     

        @SuppressWarnings("rawtypes")
         EventListener evtAut = new EventListener<Event>() {
         @Override
         public void onEvent(Event evt) throws Exception { 
                Listitem itm = (Listitem) evt.getTarget(); 
                SegusuarioMd cell = itm.getValue(); 
                
                Session InventarioSession = Sessions.getCurrent();
                InventarioSession.setAttribute("IdPerfil",  cell.getUsuario_login());
                InventarioSession.setAttribute("OP",  "MODIF");
               
                Window wdwReporte = null;
                Window w = (Window) execution.createComponents("Seguridad/CatusuarioMant.zul", wdwReporte, null);
                w.doHighlighted();
            }
        };

      
        
         public void llena_grid() { 
            List<SegusuarioMd> lstTickets = new ArrayList<SegusuarioMd>();
            SegusuarioDal datoBuscado = new SegusuarioDal();
            lstTickets = datoBuscado.getAll();
            this.Llenar_Catalogo(lstTickets);
         }

        public void Llenar_Catalogo(List<SegusuarioMd> lstTickets) 
       {
          Listitem item;
          Listcell cell;
          lbxCatalogo.getItems().clear();
            for(SegusuarioMd Ticket : lstTickets)
           {
                   item = new Listitem(); 

                   cell = new Listcell();
                   cell.setLabel(String.valueOf(Ticket.getId_usuario()));
                   cell.setParent(item);

                   cell = new Listcell();
                   cell.setLabel(Ticket.getCoddep().toString());
                   cell.setParent(item);

                   cell = new Listcell();
                   cell.setLabel(Ticket.getUsuario_login().toString());
                   cell.setParent(item);

                   cell = new Listcell();
                   cell.setLabel(Ticket.getPassword().toString());
                   cell.setParent(item);

                   cell = new Listcell();
                   cell.setLabel(Ticket.getNombre().toString());
                   cell.setParent(item);

                   cell = new Listcell();
                   cell.setLabel(Ticket.getApellido().toString());
                   cell.setParent(item);

                   String sActivo;
                   cell = new Listcell();
                   if (Ticket.getActivo() == 0) {
                       sActivo = "Inactivo";
                   } else {
                       sActivo = "Activo";
                   } 
                       
                   cell.setLabel(sActivo);
                   cell.setParent(item);

                   cell = new Listcell();
                   cell.setLabel(Ticket.getFecha_ingreso().toString());
                   cell.setParent(item);

                   cell = new Listcell();
                   cell.setLabel(Ticket.getUsuario_ingreso().toString());
                   cell.setParent(item);

                   cell = new Listcell();
                   cell.setLabel(Ticket.getFecha_modificacion().toString());
                   cell.setParent(item);

                   cell = new Listcell();
                   cell.setLabel(Ticket.getUsuario_modificacion().toString());
                   cell.setParent(item);

                   cell.setParent(item);
                   item.setValue(Ticket);
                   item.addEventListener("onClick", evtAut);
                   item.setParent(lbxCatalogo);
           }
               lbxCatalogo.invalidate();
       }
        
       
        public void onClick$linkNew(Event evt) throws SQLException{ 
	      Session InventarioSession = Sessions.getCurrent();
              InventarioSession.setAttribute("OP",  "NEW");

              Window wdwReporte = null;
              Window w = (Window) execution.createComponents("Seguridad/CatusuarioMant.zul", wdwReporte, null);
              w.doHighlighted();
	}
        
         public void onClick$linkActualizar(Event evt) throws SQLException{ 
               this.llena_grid();
         }
        
} 