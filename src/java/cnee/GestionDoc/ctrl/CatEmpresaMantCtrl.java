/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cnee.GestionDoc.ctrl;


import cnee.GestionDoc.bl.*;
import cnee.GestionDoc.model.*;
import cnee.GestionDoc.dal.*;

import org.omg.CORBA.portable.UnknownException;

import org.zkoss.zk.ui.Component;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Button;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Listbox;
import java.sql.SQLException;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Messagebox;

import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;
/*** 
* @author root
* */ 

public class CatEmpresaMantCtrl  extends GenericForwardComposer  { 

private static final long serialVersionUID = 1L; 
private Label lblUser;
private Groupbox grxFormaInOut; 

private Listbox lbxCatalogo;
private Tabbox tbox;
private Button btnGuardar;
private Button btnEliminar;
private Button btnLimpiar;

	private Textbox txtCodigo;
	private Textbox txtNombre;
	private Textbox txtDireccion;
	private Textbox txtTelefono;
	private Textbox txtFax;
	private Textbox txtEmail;
	private Textbox txtWeb;
	private Textbox txtAreageo;
	private Textbox txtCodcategoria;
	private Textbox txtActivo;
	private Textbox txtFechaingreso;
	private Textbox txtUsuarioingreso;
	private Textbox txtFechamodificacion;
	private Textbox txtUsuariomodificacion;


	@Override 
	public void doAfterCompose(Component comp) throws Exception { 
            super.doAfterCompose(comp);
            Session TicketSession = Sessions.getCurrent();
            String op = TicketSession.getAttribute("OP").toString(); 
            
            if (op.equals("NEW")) {
                CatdependenciaDal datoGraba = new CatdependenciaDal();
                this.txtCodigo.setText(String.valueOf(datoGraba.NextValue()));
                this.btnEliminar.setVisible(false);
            } else {
                        
               String codigo = TicketSession.getAttribute("Codigo").toString(); 
               String descripcion = TicketSession.getAttribute("Nombre").toString();
               String Direccion = TicketSession.getAttribute("Direccion").toString();
               String Telefono = TicketSession.getAttribute("Telefono").toString();
               String Fax = TicketSession.getAttribute("Fax").toString();
               String Email = TicketSession.getAttribute("Email").toString();
               String Web = TicketSession.getAttribute("Web").toString();
               
                 this.txtCodigo.setText(codigo);
                 this.txtNombre.setText(descripcion);
                 this.txtDireccion.setText(Direccion);
                 this.txtFax.setText(Fax);
                 this.txtEmail.setText(Email);
                 this.txtWeb.setText(Web);
                 this.btnEliminar.setVisible(true);
            }
	}

	public void LimpiarCampos() 
	{
            txtCodigo.setValue(null);
            txtNombre.setValue(null);
            txtDireccion.setValue(null);
            txtTelefono.setValue(null);
            txtFax.setValue(null);
            txtEmail.setValue(null);
            txtWeb.setValue(null);
            txtAreageo.setValue(null);
            txtCodcategoria.setValue(null);
            txtActivo.setValue(null);
            txtFechaingreso.setValue(null);
            txtUsuarioingreso.setValue(null);
            txtFechamodificacion.setValue(null);
            txtUsuariomodificacion.setValue(null);
	} 


	public void onClick$btnLimpiar(Event evt){ 
	     LimpiarCampos();
	}
       
                  
        public void onClick$linkSalir(Event evt){
            Window win = (Window)this.self ;
                 win.detach() ;
	}
	
        public void onClick$btnGuardar(Event evt) throws SQLException{ 
	       Guardar(); 
               Window win = (Window)this.self ;
               win.detach() ;
  	}

	public void onClick$btnEliminar(Event evt) throws SQLException{ 
		Eliminar_Pregunta();
                  Window win = (Window)this.self ;
                 win.detach() ;
	}
        
       
	public void Eliminar_Pregunta() { 
		Messagebox.show("Esta seguro de eliminar ?","SELECCIONE UNA OPCION", Messagebox.OK | Messagebox.NO, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() { 
		@Override
		public void onEvent(Event evt) throws InterruptedException, SQLException {
			if (evt.getName().equals("onOK")) { 
				Eliminar();
			}
			}
		});
	}

	public void Eliminar() 
	{ 
		CatempresaDal datoBuscado = new CatempresaDal(); 
		int iResp; 
		if(!txtCodigo.getText().equals("")){
		try 
		{ 
		 iResp = datoBuscado.Eliminar ( Integer.parseInt(this.txtCodigo.getText())); 
		 if (iResp > 0) {  
				 this.LimpiarCampos(); 
				 Messagebox.show(" Eliminado con Exito ");
		   } else { 
				 Messagebox.show(" Error en Eliminacion ");
			 } 
		} 
			catch (SQLException e ) {  
			Messagebox.show(" error: +" +e.getMessage()); 
			} 
		}else{Messagebox.show(" --INGRESE CODIGO A ELIMINAR-- ");
}	}
	

	public void Guardar() 
	{ 
	int iResp; 
        
		if(!txtCodigo.getText().equals("")  ){
		
                    CatempresaMd datoCrear = new CatempresaMd(txtCodigo.getText(),txtNombre.getText(),txtDireccion.getText(),txtTelefono.getText(),txtFax.getText(),
                        txtEmail.getText(),txtWeb.getText(),txtAreageo.getText(),txtCodcategoria.getText(),0,txtFechaingreso.getText(),txtUsuarioingreso.getText(),
                        txtFechamodificacion.getText(),txtUsuariomodificacion.getText());
                    CatempresaDal datoGraba = new CatempresaDal();
                    CatempresaBl datoValida = new CatempresaBl();
                    String sMensaje = datoValida.Validar(datoCrear);		
                    
		if (sMensaje == "") { 
			 try {
				 String sAccion = " Creado ";
				 if (datoGraba.existe(Integer.parseInt(txtCodigo.getText())) == true ) 
				 {
					  sAccion = " Actualizado ";
					 iResp = datoGraba.Modificar(datoCrear);
				 } else {
					 iResp = datoGraba.Crear(datoCrear);
				 } 
				 if (iResp > 0) { 
                                    this.LimpiarCampos(); 
                                    Messagebox.show(sAccion + " con Exito ");
				 } else { 
                                    Messagebox.show(" Error en " + sAccion);
				 } 
				 } 
			   catch (SQLException e ) {  
				  Messagebox.show("Error: " + e.getMessage());
			 } 
			  catch (UnknownException e ) {  
				  Messagebox.show("Error: " + e.getMessage());
			 } 
				 } else { 
				  Messagebox.show(sMensaje);
				 } 
		 } else { 
				  Messagebox.show("--INGRESE CODIGOS--");
                    } 
		 } 


} 