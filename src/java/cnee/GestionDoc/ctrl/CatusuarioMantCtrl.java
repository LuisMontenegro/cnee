/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cnee.GestionDoc.ctrl;


import cnee.GestionDoc.bl.*;
import cnee.GestionDoc.model.*;
import cnee.GestionDoc.dal.*;
import cnee.GestionDoc.util.Util;

import org.omg.CORBA.portable.UnknownException;

import org.zkoss.zk.ui.Component;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Button;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Listbox;
import java.sql.SQLException;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Messagebox;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Window;


public class CatusuarioMantCtrl  extends GenericForwardComposer  { 

private static final long serialVersionUID = 1L; 
private Label lblUser;
private Groupbox grxFormaInOut; 

private Listbox lbxCatalogo;
private Tabbox tbox;

private Button btnGuardar;
private Button btnEliminar;
private Button btnLimpiar;
private Combobox cbxDependencia;
private Combobox cbxActivo;
private Combobox cbxPerfil;

Util Utilitario = new Util();
	
	private Textbox txtIdusuario;
	private Textbox txtCoddep;
	private Textbox txtUsuariologin;
	private Textbox txtPassword;
	private Textbox txtNombre;
	private Textbox txtApellido;
	private Textbox txtActivo;
	private Textbox txtFechaingreso;
	private Textbox txtUsuarioingreso;
	private Textbox txtFechamodificacion;
	private Textbox txtUsuariomodificacion;
        
     
	@Override 
	public void doAfterCompose(Component comp) throws Exception { 
            super.doAfterCompose(comp);
            // ComboBox
            String sQuery = "select codigo, descripcion from cat_dependencia where activo = 1 ";
            Utilitario.cargaCombox(sQuery, cbxDependencia);
            sQuery = "select id_perfil,  nombre from seg_perfil";
            Utilitario.cargaCombox(sQuery, cbxPerfil);
                    
            SegusuarioDal datoGraba = new SegusuarioDal();
            Session TicketSession = Sessions.getCurrent();
            String op = TicketSession.getAttribute("OP").toString(); 
            
            if (op.equals("NEW")) {
                this.btnEliminar.setVisible(false);
                this.txtIdusuario.setText(String.valueOf(datoGraba.NextValue()));
            } 
            else 
            {
               String IdPerfil = TicketSession.getAttribute("IdPerfil").toString();  
               SegusuarioMd dato;
             
               this.txtUsuariologin.setReadonly(true);
               this.txtPassword.setReadonly(true);
                
               dato = datoGraba.busca(IdPerfil);
               String sPasswordDesEncriptado = Utilitario.Desencriptar(dato.getPassword());
               this.txtPassword.setText(sPasswordDesEncriptado);
               this.txtIdusuario.setText(String.valueOf(dato.getId_usuario()));
               this.txtNombre.setText(dato.getNombre());
               txtApellido.setText(dato.getApellido());
               this.txtUsuariologin.setText(dato.getUsuario_login());
               this.txtCoddep.setText(dato.getCoddep());
               
               CatdependenciaDal Dep = new CatdependenciaDal();
               CatdependenciaMd CatDep;
               CatDep = Dep.busca(dato.getCoddep());
               String sPerfil = datoGraba.perfil_usuario(dato.getId_usuario());
                  
               this.cbxDependencia.setValue(CatDep.getDescripcion());
               this.cbxActivo.setSelectedIndex(dato.getActivo());
               this.cbxPerfil.setValue(sPerfil);
            }
	}

	
	
	public void LimpiarCampos() 
	{
            txtIdusuario.setValue(null);
            txtCoddep.setValue(null);
            txtUsuariologin.setValue(null);
            txtPassword.setValue(null);
            txtNombre.setValue(null);
            txtApellido.setValue(null);
            txtActivo.setValue(null);
        } 
        
        
        
	public void onClick$btnLimpiar(Event evt){ 
	     LimpiarCampos();
	}
       
                  
        public void onClick$linkSalir(Event evt){
            Window win = (Window)this.self ;
            win.detach() ;
	}
	
        public void onClick$btnGuardar(Event evt) throws SQLException{ 
            Guardar(); 
            Window win = (Window)this.self ;
            win.detach() ;
  	}

	public void onClick$btnEliminar(Event evt) throws SQLException{ 
            Eliminar_Pregunta();
            Window win = (Window)this.self ;
            win.detach() ;
	}
        
      
	public void Eliminar_Pregunta() { 
		Messagebox.show("Esta seguro de eliminar ?","SELECCIONE UNA OPCION", Messagebox.OK | Messagebox.NO, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() { 
		@Override
		public void onEvent(Event evt) throws InterruptedException, SQLException {
			if (evt.getName().equals("onOK")) { 
				Eliminar();
			}
			}
		});
	}
        
        
               
	public void Eliminar() throws SQLException 
	{ 
		SegusuarioDal datoBuscado = new SegusuarioDal(); 
		int iResp; 
		if(!txtIdusuario.getText().equals("")){
                    iResp = datoBuscado.Eliminar ( Integer.parseInt(this.txtIdusuario.getText()));
                    if (iResp > 0) {
                        this.LimpiarCampos();
                        Messagebox.show(" Eliminado con Exito ");
                    } else {
                        Messagebox.show(" Error en Eliminacion ");
                    } 
		}else{Messagebox.show(" --INGRESE CODIGO A ELIMINAR-- ");
}	}


        
               
	
	public void Guardar() 
	{ 
	int iResp; 
                 SegusuarioDal datoGraba = new SegusuarioDal();
		SegusuarioBl datoValida = new SegusuarioBl();
                 SegusuarioMd datoCrear;
                
		if(!txtIdusuario.getText().equals("") ){
                String sPasswordEncriptado = Utilitario.Encriptar(txtPassword.getText());
                
                // si es existente
                 if (datoGraba.existe(Integer.parseInt(txtIdusuario.getText())) == true )  {
                
                       
                        datoCrear = new SegusuarioMd(Integer.parseInt(txtIdusuario.getText()),
                            this.cbxDependencia.getSelectedItem().getValue().toString(),
                            txtUsuariologin.getText(),
                            sPasswordEncriptado,
                            txtNombre.getText(),
                            txtApellido.getText(),
                            Integer.parseInt(this.cbxActivo.getSelectedItem().getValue().toString()),
                            null,
                            null,
                            null,
                            null);
                 } else {
                      
                     
                      datoCrear = new SegusuarioMd(Integer.parseInt(txtIdusuario.getText()),
                            this.cbxDependencia.getSelectedItem().getValue().toString(),
                            txtUsuariologin.getText(),
                            sPasswordEncriptado,
                            txtNombre.getText(),
                            txtApellido.getText(),
                            Integer.parseInt(this.cbxActivo.getSelectedItem().getValue().toString()),
                            null,
                            null,
                            null,
                            null);
                 }
                
		
		String sMensaje = datoValida.Validar(datoCrear);		
		if (sMensaje == "") { 
			 try {
				 String sAccion = " Creado ";
				 if (datoGraba.existe(Integer.parseInt(txtIdusuario.getText())) == true ) 
				 {
					  sAccion = " Actualizado ";
					 iResp = datoGraba.Modificar(datoCrear);
                                         this.graba_perfil();
				 } else {
					 iResp = datoGraba.Crear(datoCrear);
                                        this.graba_perfil();
				 } 
				 if (iResp > 0) { 
					     this.LimpiarCampos(); 
					 Messagebox.show(sAccion + " con Exito ");
				 } else { 
					 Messagebox.show(" Error en " + sAccion);
				 } 
				 } 
			   catch (SQLException e ) {  
				  Messagebox.show("Error: " + e.getMessage());
			 } 
			  catch (UnknownException e ) {  
				  Messagebox.show("Error: " + e.getMessage());
			 } 
				 } else { 
				  Messagebox.show(sMensaje);
				 } 
		 } else { 
				  Messagebox.show("--INGRESE CODIGOS--");
				 } 
		 } 

        
                 
      public void graba_perfil() 
	{ 
	int iResp; 
		SegperfilusuarioMd datoCrear = new SegperfilusuarioMd(0,
                                                                      Integer.parseInt(this.cbxPerfil.getSelectedItem().getValue().toString()),
                                                                      Integer.parseInt(txtIdusuario.getText()),
                                                                      1,
                                                                      "",
                                                                      "",
                                                                      "",
                                                                      "");
                
                
		SegperfilusuarioDal datoGraba2 = new SegperfilusuarioDal();
		String sMensaje = "";
                
                try {
                        String sAccion = " Creado ";
                        if (datoGraba2.existe(Integer.parseInt(this.txtIdusuario.getText())) == true ) 
                        {
                                 sAccion = " Actualizado ";
                                iResp = datoGraba2.Modificar(datoCrear);
                        } else {
                                iResp = datoGraba2.Crear(datoCrear);
                        } 

                        } 
                  catch (SQLException e ) {  
                         Messagebox.show("Error: " + e.getMessage());
                } 
                 catch (UnknownException e ) {  
                         Messagebox.show("Error: " + e.getMessage());
                } 
            } 
         

                    
        


} 