

package cnee.GestionDoc.ctrl;

import cnee.GestionDoc.model.*;
import cnee.GestionDoc.dal.*;
import cnee.GestionDoc.util.Util;
import org.zkoss.zk.ui.Component;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Button;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Listbox;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Window;
/*** 
* @author root
* */ 

public class CatperfilobjetoCtrl  extends GenericForwardComposer  { 

private static final long serialVersionUID = 1L; 
private Label lblUser;
private Groupbox grxFormaInOut; 

private Listbox lbxCatalogo;
private Tabbox tbox;
private Button btnGuardar;
private Button btnBuscar;
private Button btnEliminar;
private Button btnLimpiar;
private Combobox cbxPerfil;

Util Utilitario = new Util();
   
	
	private Intbox txtId;
	private Textbox txtDepencod;
	private Textbox txtTema;
	private Textbox txtTemades;
	private Textbox txtClasif;
	private Textbox txtActivo;
	private Textbox txtUsuarioingreso;
	private Textbox txtFechaingreso;
	private Textbox txtFechamodificacion;
	private Textbox txtUsuariomodificacion;
        
      
	@Override 
	public void doAfterCompose(Component comp) throws Exception { 
            super.doAfterCompose(comp);
            String sQuery = "select id_perfil, nombre from seg_perfil where estatus = 1";
            Utilitario.cargaCombox(sQuery, cbxPerfil);
	}
        
        
        public void onSelect$cbxPerfil(Event evt) throws SQLException {
           this.llena_grid();
        }

        @SuppressWarnings("rawtypes")
         EventListener evtAut = new EventListener<Event>() {
         @Override
         public void onEvent(Event evt) throws Exception { 
                Listitem itm = (Listitem) evt.getTarget(); 
                SegperfilMd cell = itm.getValue(); 
                
                Session InventarioSession = Sessions.getCurrent();
                InventarioSession.setAttribute("IdPerfil",  cell.getId_perfil());
                InventarioSession.setAttribute("OP",  "MODIF");
               
                Window wdwReporte = null;
                Window w = (Window) execution.createComponents("Seguridad/CatperfilMant.zul", wdwReporte, null);
                w.doHighlighted();
            }
        };
      
        
          public void llena_grid() { 
            List<SegobjetoMd> lstTickets = new ArrayList<SegobjetoMd>();
           SegobjetoDal datoBuscado = new SegobjetoDal();
            lstTickets = datoBuscado.getAll();
            this.Llenar_Catalogo(lstTickets);
           }

        public void Llenar_Catalogo(List<SegobjetoMd> lstTickets) 
       {
          Listitem item;
          Listcell cell;
          lbxCatalogo.getItems().clear();
                for(SegobjetoMd Ticket : lstTickets)
               {
                       item = new Listitem(); 

                       cell = new Listcell();
                       cell.setLabel(String.valueOf(Ticket.getId_objeto()));
                       cell.setParent(item);

                       cell = new Listcell();
                       cell.setLabel(String.valueOf(Ticket.getId_tipo_objeto()));
                       cell.setParent(item);

                       cell = new Listcell();
                       cell.setLabel(Ticket.getNombre().toString());
                       cell.setParent(item);

                       cell = new Listcell();
                       cell.setLabel(Ticket.getDescripcion().toString());
                       cell.setParent(item);

                       cell = new Listcell();
                       cell.setLabel(Ticket.getUrl().toString());
                       cell.setParent(item);

                       cell.setParent(item);
                       item.setValue(Ticket);
                      
                       item.setParent(lbxCatalogo);
               }
               lbxCatalogo.invalidate();
       }

       

          public void onClick$linkActualizar(Event evt) throws SQLException{ 
               this.llena_grid();
          }
        
      
                 


} 