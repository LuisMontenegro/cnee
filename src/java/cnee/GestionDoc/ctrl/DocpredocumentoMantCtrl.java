/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cnee.GestionDoc.ctrl;


import cnee.GestionDoc.bl.*;
import cnee.GestionDoc.model.*;
import cnee.GestionDoc.dal.*;
import cnee.GestionDoc.util.Util;

import org.omg.CORBA.portable.UnknownException;

import org.zkoss.zk.ui.Component;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Button;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Listbox;
import java.sql.SQLException;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Messagebox;

import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Window;
/*** 
* @author root
* */ 

public class DocpredocumentoMantCtrl  extends GenericForwardComposer  { 

private static final long serialVersionUID = 1L; 
private Label lblUser;
private Groupbox grxFormaInOut; 

private Listbox lbxCatalogo;
private Tabbox tbox;

private Button btnGuardar;
private Button btnEliminar;
private Button btnLimpiar;

private Combobox cbxEmpOrigen;
private Combobox cbxDependenciaOrigen;
private Combobox cbxPersonaOrigen;
private Combobox cbxDestino;
	
	private Textbox txtCodigo;
	private Textbox txtCodigoorigen;
	private Textbox txtEmpresaorigen;
	private Textbox txtPersonaorigen;
	private Textbox txtDestino;
	private Textbox txtTemas;
	private Textbox txtFechaingreso;
	private Textbox txtHoraingreso;
	private Textbox txtUsuarioingreso;
	private Textbox txtFechamodificacion;
	private Textbox txtUsuariomodificacion;


        Util Utilitario = new Util();
        

	@Override 
	public void doAfterCompose(Component comp) throws Exception { 
            super.doAfterCompose(comp);
            DocpredocumentoDal datoGraba = new DocpredocumentoDal();
            
            Session TicketSession = Sessions.getCurrent();
            String op = TicketSession.getAttribute("OP").toString(); 
            
            String sQuery = "select codigo, nombre from cat_empresa";
            Utilitario.cargaCombox(sQuery, cbxEmpOrigen);
            
            sQuery = "select codigo, Descripcion from cat_dependencia where activo = 1";
            Utilitario.cargaCombox(sQuery, cbxDependenciaOrigen);
            
            sQuery = "select codigo, nombres from cat_persona";
            Utilitario.cargaCombox(sQuery, cbxPersonaOrigen);
            
            sQuery = "select codigo, Descripcion from cat_dependencia where activo = 1";
            Utilitario.cargaCombox(sQuery, cbxDestino);
            
            this.txtCodigo.setReadonly(true);
            this.txtTemas.setMultiline(true);
                
            if (op.equals("NEW")) {
                this.btnEliminar.setVisible(false);
                this.txtCodigo.setText(String.valueOf(datoGraba.NextValue()));
                this.txtFechaingreso.setText(Util.getCurrentDateString());
                this.txtHoraingreso.setText(Util.getHora()); 
            } else {
               String IdPerfil = TicketSession.getAttribute("IdPerfil").toString();  
               DocpredocumentoMd dato;
               dato = datoGraba.busca(Integer.parseInt(IdPerfil));
               SegtipoobjetoDal Obj = new SegtipoobjetoDal();
               SegtipoobjetoMd ObjDato;
               
               this.txtCodigo.setText(String.valueOf(dato.getCodigo()));
               this.txtTemas.setText(dato.getTemas());
               this.txtHoraingreso.setText(dato.getHora_ingreso());
               this.txtFechaingreso.setText(dato.getFecha_ingreso());
              
               CatempresaDal catEmp = new CatempresaDal();
               CatempresaMd catEmpmd;
               catEmpmd = catEmp.busca(dato.getEmpresaorigen());
               this.cbxEmpOrigen.setValue(catEmpmd.getNombre());
               
               CatdependenciaDal CatDep = new CatdependenciaDal();
               CatdependenciaMd catDepmd;
               catDepmd = CatDep.busca(dato.getCodigoorigen());
               this.cbxDependenciaOrigen.setValue(catDepmd.getDescripcion());
               catDepmd = CatDep.busca(dato.getDestino());
               this.cbxDestino.setValue(catDepmd.getDescripcion());
            
               CatpersonaDal CatPer = new CatpersonaDal();
               CatpersonaMd CatPermd;
               CatPermd = CatPer.busca(dato.getCodigo());
               this.cbxPersonaOrigen.setValue(CatPermd.getNombres());
             }
	}

       
	public void LimpiarCampos() 
	{
            txtCodigo.setValue(null);
            txtCodigoorigen.setValue(null);
            txtEmpresaorigen.setValue(null);
            txtPersonaorigen.setValue(null);
            txtDestino.setValue(null);
            txtTemas.setValue(null);
            txtFechaingreso.setValue(null);
            txtHoraingreso.setValue(null);
        }
         
	public void onClick$btnLimpiar(Event evt){ 
	     LimpiarCampos();
	}
       
                  
        public void onClick$linkSalir(Event evt){
            Window win = (Window)this.self ;
            win.detach() ;
	}
	
        public void onClick$btnGuardar(Event evt) throws SQLException{ 
            Guardar(); 
            Window win = (Window)this.self ;
            win.detach() ;
  	}

	public void onClick$btnEliminar(Event evt) throws SQLException{ 
            Eliminar_Pregunta();
              Window win = (Window)this.self ;
             win.detach() ;
	}
        
      
	public void Eliminar_Pregunta() { 
		Messagebox.show("Esta seguro de eliminar ?","SELECCIONE UNA OPCION", Messagebox.OK | Messagebox.NO, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() { 
		@Override
		public void onEvent(Event evt) throws InterruptedException, SQLException {
			if (evt.getName().equals("onOK")) { 
				Eliminar();
			}
			}
		});
	}
        
    
        
        public void Eliminar() 
	{ 
		DocpredocumentoDal datoBuscado = new DocpredocumentoDal(); 
		int iResp; 
		if(!txtCodigo.getText().equals("")){
		try 
		{ 
		 iResp = datoBuscado.Eliminar ( Integer.parseInt(this.txtCodigo.getText())); 
		 if (iResp > 0) {  
				 this.LimpiarCampos(); 
				 Messagebox.show(" Eliminado con Exito ");
		   } else { 
				 Messagebox.show(" Error en Eliminacion ");
			 } 
		} 
			catch (SQLException e ) {  
			Messagebox.show(" error: +" +e.getMessage()); 
			} 
		}else{Messagebox.show(" --INGRESE CODIGO A ELIMINAR-- ");
}	}

        
        
     
     
        
	public void Guardar() 
	{ 
	int iResp; 
		if(!txtCodigo.getText().equals("")){
                  
		DocpredocumentoMd datoCrear = new DocpredocumentoMd(Integer.parseInt(txtCodigo.getText()),
                                                                   cbxDependenciaOrigen.getSelectedItem().getValue().toString(),
                                                                   cbxEmpOrigen.getSelectedItem().getValue().toString(),
                                                                   cbxPersonaOrigen.getSelectedItem().getValue().toString(),
                                                                   cbxDestino.getSelectedItem().getValue().toString(),
                                                                   txtTemas.getText(),
                                                                   txtFechaingreso.getText(),
                                                                   txtHoraingreso.getText(),
                                                                   "",
                                                                   "",
                                                                   "");
                
                
		DocpredocumentoDal datoGraba = new DocpredocumentoDal();
		DocpredocumentoBl datoValida = new DocpredocumentoBl();
		String sMensaje = datoValida.Validar(datoCrear);		
		if (sMensaje == "") { 
			 try {
				 String sAccion = " Creado ";
				 if (datoGraba.existe(Integer.parseInt(txtCodigo.getText())) == true ) 
				 {
					  sAccion = " Actualizado ";
					 iResp = datoGraba.Modificar(datoCrear);
				 } else {
					 iResp = datoGraba.Crear(datoCrear);
				 } 
				 if (iResp > 0) { 
					     this.LimpiarCampos(); 
					 Messagebox.show(sAccion + " con Exito ");
				 } else { 
					 Messagebox.show(" Error en " + sAccion);
				 } 
				 } 
			   catch (SQLException e ) {  
				  Messagebox.show("Error: " + e.getMessage());
			 } 
			  catch (UnknownException e ) {  
				  Messagebox.show("Error: " + e.getMessage());
			 } 
				 } else { 
				  Messagebox.show(sMensaje);
				 } 
		 } else { 
				  Messagebox.show("--INGRESE CODIGOS--");
				 } 
		 } 


} 