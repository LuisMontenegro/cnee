/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cnee.GestionDoc.ctrl;


import cnee.GestionDoc.bl.*;
import cnee.GestionDoc.model.*;
import cnee.GestionDoc.dal.*;
import cnee.GestionDoc.util.Commons;

import org.omg.CORBA.portable.UnknownException;

import org.zkoss.zk.ui.Component;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Button;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Listbox;
import java.sql.SQLException;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Messagebox;

import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;
/*** 
* @author root
* */ 

public class CatperfilMantCtrl  extends GenericForwardComposer  { 

private static final long serialVersionUID = 1L; 
private Label lblUser;
private Groupbox grxFormaInOut; 

private Listbox lbxCatalogo;
private Tabbox tbox;

private Button btnGuardar;
private Button btnEliminar;
private Button btnLimpiar;

       
	private Textbox txtIdperfil;
	private Textbox txtNombre;
	private Textbox txtDescripcion;
	private Textbox txtEstatus;
	private Textbox txtFechaingreso;
	private Textbox txtUsuarioingreso;
	private Textbox txtFechamodificacion;
	private Textbox txtUsuariomodificacion;



	@Override 
	public void doAfterCompose(Component comp) throws Exception { 
            super.doAfterCompose(comp);
            SegperfilDal datoGraba = new SegperfilDal();
            
            Session TicketSession = Sessions.getCurrent();
            String op = TicketSession.getAttribute("OP").toString(); 
           
            
            if (op.equals("NEW")) {
                this.btnEliminar.setVisible(false);
            } else {
                SegperfilMd dato;
                 String IdPerfil = TicketSession.getAttribute("IdPerfil").toString();  
                dato = datoGraba.busca(Integer.parseInt(IdPerfil));
               this.txtIdperfil.setText(String.valueOf(dato.getId_perfil()));
               this.txtNombre.setText(dato.getNombre());
               this.txtDescripcion.setText(dato.getDescripcion());
            }
	}

	
	public void LimpiarCampos() 
	{
            txtIdperfil.setValue(null);
            txtNombre.setValue(null);
            txtDescripcion.setValue(null);
	} 
        
        
	public void onClick$btnLimpiar(Event evt){ 
	     LimpiarCampos();
	}
       
                  
        public void onClick$linkSalir(Event evt){
            Window win = (Window)this.self ;
            win.detach();
            execution.sendRedirect("/Seguridad/Catperfil.zul");
	}
	
        public void onClick$btnGuardar(Event evt) throws SQLException{ 
            Guardar(); 
            Window win = (Window)this.self ;
            win.detach() ;
  	}

	public void onClick$btnEliminar(Event evt) throws SQLException{ 
            Eliminar_Pregunta();
              Window win = (Window)this.self ;
             win.detach() ;
	}
        
       
	public void Eliminar_Pregunta() { 
		Messagebox.show("Esta seguro de eliminar ?","SELECCIONE UNA OPCION", Messagebox.OK | Messagebox.NO, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() { 
		@Override
		public void onEvent(Event evt) throws InterruptedException, SQLException {
			if (evt.getName().equals("onOK")) { 
				Eliminar();
			}
			}
		});
	}

        
        
        
	public void Eliminar() 
	{ 
		SegperfilDal datoBuscado = new SegperfilDal(); 
		int iResp; 
		if(!txtIdperfil.getText().equals("")){
		try 
		{ 
		 iResp = datoBuscado.Eliminar ( Integer.parseInt(this.txtIdperfil.getText())); 
		 if (iResp > 0) {  
				 this.LimpiarCampos(); 
				 Messagebox.show(" Eliminado con Exito ");
		   } else { 
				 Messagebox.show(" Error en Eliminacion ");
			 } 
		} 
			catch (SQLException e ) {  
			Messagebox.show(" error: +" +e.getMessage()); 
			} 
		}else{Messagebox.show(" --INGRESE CODIGO A ELIMINAR-- ");
}	}

        
        
        
     
	public void Guardar() 
	{ 
                        int iResp; 
		if(!txtIdperfil.getText().equals("") ){
                    
		SegperfilMd datoCrear = new SegperfilMd(Integer.parseInt(txtIdperfil.getText()),txtNombre.getText(),txtDescripcion.getText(),1,null,null,null,null);
		SegperfilDal datoGraba = new SegperfilDal();
		SegperfilBl datoValida = new SegperfilBl();
		String sMensaje = datoValida.Validar(datoCrear);	
                
		if (sMensaje == "") { 
			 try {
				 String sAccion = " Creado ";
				 if (datoGraba.existe(Integer.parseInt(txtIdperfil.getText())) == true ) 
				 {
					  sAccion = " Actualizado ";
					 iResp = datoGraba.Modificar(datoCrear);
				 } else {
					 iResp = datoGraba.Crear(datoCrear);
				 } 
				 if (iResp > 0) { 
					     this.LimpiarCampos(); 
					 Messagebox.show(sAccion + " con Exito ");
				 } else { 
					 Messagebox.show(" Error en " + sAccion);
				 } 
				 } 
			   catch (SQLException e ) {  
				  Messagebox.show("Error: " + e.getMessage());
			 } 
			  catch (UnknownException e ) {  
				  Messagebox.show("Error: " + e.getMessage());
			 } 
				 } else { 
				  Messagebox.show(sMensaje);
				 } 
		 } else { 
				  Messagebox.show("--INGRESE CODIGOS--");
				 } 
		 } 



} 