

package cnee.GestionDoc.ctrl;

import cnee.GestionDoc.model.*;
import cnee.GestionDoc.dal.*;
import cnee.GestionDoc.util.Util;
import org.zkoss.zk.ui.Component;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Button;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Listbox;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Window;
/*** 
* @author root
* */ 

public class CatobjetoCtrl  extends GenericForwardComposer  { 

private static final long serialVersionUID = 1L; 
private Label lblUser;
private Groupbox grxFormaInOut; 

private Listbox lbxCatalogo;
private Tabbox tbox;
private Button btnGuardar;
private Button btnBuscar;
private Button btnEliminar;
private Button btnLimpiar;
private Combobox cbxPerfil;

	
private Textbox txtIdobjeto;
	private Textbox txtIdtipoobjeto;
	private Textbox txtNombre;
	private Textbox txtDescripcion;
	private Textbox txtActivo;
	private Textbox txtFechaingreso;
	private Textbox txtUsuarioingreso;
	private Textbox txtFechamodificacion;
	private Textbox txtUsuariomodificacion;
	private Intbox txtIdpadre;
	private Textbox txtUrl;
        Util Utilitario = new Util();
      
	@Override 
	public void doAfterCompose(Component comp) throws Exception { 
            super.doAfterCompose(comp);
            this.llena_grid();
          
           
	}
     

        @SuppressWarnings("rawtypes")
         EventListener evtAut = new EventListener<Event>() {
         @Override
         public void onEvent(Event evt) throws Exception { 
                Listitem itm = (Listitem) evt.getTarget(); 
                SegobjetoMd cell = itm.getValue(); 
                
                Session InventarioSession = Sessions.getCurrent();
                InventarioSession.setAttribute("IdPerfil",  cell.getId_objeto());
                InventarioSession.setAttribute("OP",  "MODIF");
               
                Window wdwReporte = null;
                Window w = (Window) execution.createComponents("Seguridad/CatobjetoMant.zul", wdwReporte, null);
                w.doHighlighted();
            }
        };

      
        
      
                public void llena_grid() { 
			 List<SegobjetoMd> lstTickets = new ArrayList<SegobjetoMd>();
			SegobjetoDal datoBuscado = new SegobjetoDal();
			 lstTickets = datoBuscado.getAll();
			 this.Llenar_Catalogo(lstTickets);
                }

		 public void Llenar_Catalogo(List<SegobjetoMd> lstTickets) 
		{
		   Listitem item;
		   Listcell cell;
		   lbxCatalogo.getItems().clear();
                   
                   SegtipoobjetoDal tipoOb = new SegtipoobjetoDal();
                   SegtipoobjetoMd Consulta;
		    
                   for(SegobjetoMd Ticket : lstTickets)
			{
                            item = new Listitem(); 

                            cell = new Listcell();
                            cell.setLabel(String.valueOf(Ticket.getId_objeto()));
                            cell.setParent(item);

                            Consulta = tipoOb.busca(Ticket.getId_tipo_objeto());

                            cell = new Listcell();
                            cell.setLabel(Consulta.getDescripcion());
                            cell.setParent(item);

                            cell = new Listcell();
                            cell.setLabel(Ticket.getNombre().toString());
                            cell.setParent(item);

                            cell = new Listcell();
                            cell.setLabel(Ticket.getDescripcion().toString());
                            cell.setParent(item);

                            String sActivo;
                            cell = new Listcell();
                            if (Ticket.getActivo() == 0) {
                                sActivo = "Inactivo";
                            } else {
                                sActivo = "Activo";
                            } 
                             cell.setLabel(sActivo);
                             cell.setParent(item);


                            cell = new Listcell();
                            cell.setLabel(Ticket.getFecha_ingreso().toString());
                            cell.setParent(item);

                            cell = new Listcell();
                            cell.setLabel(Ticket.getUsuario_ingreso().toString());
                            cell.setParent(item);

                            cell = new Listcell();
                            cell.setLabel(Ticket.getFecha_modificacion().toString());
                            cell.setParent(item);

                            cell = new Listcell();
                            cell.setLabel(Ticket.getUsuario_modificacion().toString());
                            cell.setParent(item);

                            cell = new Listcell();
                            cell.setLabel(String.valueOf(Ticket.getId_padre()));
                            cell.setParent(item);

                            cell = new Listcell();
                            cell.setLabel(Ticket.getUrl().toString());
                            cell.setParent(item);

                            cell.setParent(item);
                            item.setValue(Ticket);
                            item.addEventListener("onClick", evtAut);
                            item.setParent(lbxCatalogo);
			}
			lbxCatalogo.invalidate();
		}
                 
       
        public void onClick$linkNew(Event evt) throws SQLException{ 
	      Session InventarioSession = Sessions.getCurrent();
              InventarioSession.setAttribute("OP",  "NEW");

              Window wdwReporte = null;
              Window w = (Window) execution.createComponents("Seguridad/CatobjetoMant.zul", wdwReporte, null);
              w.doHighlighted();
	}
        
          public void onClick$linkActualizar(Event evt) throws SQLException{ 
               this.llena_grid();
          }
        
      
                 


} 