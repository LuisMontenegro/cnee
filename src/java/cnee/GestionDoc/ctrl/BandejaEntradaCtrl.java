

package cnee.GestionDoc.ctrl;

import cnee.GestionDoc.model.*;
import cnee.GestionDoc.dal.*;
import org.zkoss.zk.ui.Component;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Button;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Listbox;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;
/*** 
* @author root
* */ 

public class BandejaEntradaCtrl  extends GenericForwardComposer  { 

private static final long serialVersionUID = 1L; 
private Label lblUser;
private Groupbox grxFormaInOut; 

private Listbox lbxCatalogo;
private Tabbox tbox;
private Button btnGuardar;
private Button btnBuscar;
private Button btnEliminar;
private Button btnLimpiar;

	private Intbox txtCodigo;
	private Textbox txtDescripcion;
	private Textbox txtActivo;
	private Textbox txtFechaingreso;
	private Textbox txtUsuarioingreso;
	private Textbox txtFechamodificacion;
	private Textbox txtUsuariomodificacion;
        
      
	@Override 
	public void doAfterCompose(Component comp) throws Exception { 
            super.doAfterCompose(comp);
            this.llena_grid();
	}
     

//        @SuppressWarnings("rawtypes")
//         EventListener evtAut = new EventListener<Event>() {
//         @Override
//         public void onEvent(Event evt) throws Exception { 
//                Listitem itm = (Listitem) evt.getTarget(); 
//                CatempresaMd cell = itm.getValue(); 
//                
//                Session InventarioSession = Sessions.getCurrent();
//                InventarioSession.setAttribute("Codigo",  cell.getCodigo());
//                InventarioSession.setAttribute("Nombre",  cell.getNombre());
//                InventarioSession.setAttribute("Direccion",  cell.getDireccion());
//                InventarioSession.setAttribute("Telefono",  cell.getTelefono());
//                InventarioSession.setAttribute("Fax",  cell.getFax());
//                InventarioSession.setAttribute("Email",  cell.getEmail());
//                InventarioSession.setAttribute("Web",  cell.getWeb());
//                InventarioSession.setAttribute("OP",  "MODIF");
//               
//                Window wdwReporte = null;
//                Window w = (Window) execution.createComponents("Catalogos/CatEmpresaMant.zul", wdwReporte, null);
//                w.doHighlighted();
//            }
//        };

       
         public void onClick$linkDoctosFuentes(Event evt) throws SQLException{ 
	      Session InventarioSession = Sessions.getCurrent();
              InventarioSession.setAttribute("OP",  "NEW");

              Window wdwReporte = null;
              Window w = (Window) execution.createComponents("Documentos/DocFuente.zul", wdwReporte, null);
              w.doHighlighted();
	}         

       
        public void onClick$linkNew(Event evt) throws SQLException{ 
	      Session InventarioSession = Sessions.getCurrent();
              InventarioSession.setAttribute("OP",  "NEW");

              Window wdwReporte = null;
              Window w = (Window) execution.createComponents("Documentos/DocumentosMant.zul", wdwReporte, null);
              w.doHighlighted();
	}
        
          public void onClick$linkActualizar(Event evt) throws SQLException{ 
               this.llena_grid();
          }
        
      
            public void llena_grid() { 
                List<Pro1csdoctosMd> lstTickets = new ArrayList<Pro1csdoctosMd>();
               Pro1csdoctosDal datoBuscado = new Pro1csdoctosDal();
                lstTickets = datoBuscado.getAll();
                this.Llenar_Catalogo(lstTickets);
            }

            public void Llenar_Catalogo(List<Pro1csdoctosMd> lstTickets) 
           {
              Listitem item;
              Listcell cell;
              lbxCatalogo.getItems().clear();
                    for(Pro1csdoctosMd Ticket : lstTickets)
                   {
                           item = new Listitem(); 

                           cell = new Listcell();
                           cell.setLabel(Ticket.getLastfecha().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getLasthora().toString());
                           cell.setParent(item);


                           cell = new Listcell();
                           cell.setLabel(Ticket.getFecharecep().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getTiporeg().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getDepencod().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getDoccod().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getDocnum().toString());
                           cell.setParent(item);


                           cell = new Listcell();
                           cell.setLabel(Ticket.getOrigempresa().toString());
                           cell.setParent(item);


                           cell = new Listcell();
                           cell.setLabel(Ticket.getOrigpersona().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getDestempresa().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getDestpersona().toString());
                           cell.setParent(item);


                           cell = new Listcell();
                           cell.setLabel(Ticket.getDocref().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getFechaemision().toString());
                           cell.setParent(item);



                           cell = new Listcell();
                           cell.setLabel(Ticket.getHorarecep().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getTema().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getDescripcion().toString());
                           cell.setParent(item);




                           cell = new Listcell();
                           cell.setLabel(Ticket.getOrigdependencia().toString());
                           cell.setParent(item);





                           cell = new Listcell();
                           cell.setLabel(Ticket.getDestdependencia().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getEstado().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getObservaciones().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getDocusovis().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getLastdepencod().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getLastpersona().toString());
                           cell.setParent(item);



                           cell = new Listcell();
                           cell.setLabel(Ticket.getDocfojas().toString());
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(String.valueOf(Ticket.getMonto()));
                           cell.setParent(item);

                           cell = new Listcell();
                           cell.setLabel(Ticket.getUsuariosaccesos().toString());
                           cell.setParent(item);

                           cell.setParent(item);
                           item.setValue(Ticket);
//				item.addEventListener("onClick", evtAut);
                           item.setParent(lbxCatalogo);
                   }
                   lbxCatalogo.invalidate();
           }
       


} 