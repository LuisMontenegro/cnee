/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cnee.GestionDoc.environment;

import cnee.GestionDoc.dal.SegusuarioDal;
import cnee.GestionDoc.model.SegusuarioMd;
import cnee.GestionDoc.util.Commons;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Div;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Window;

import java.util.HashMap;
import java.util.Map;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Groupbox;

@SuppressWarnings("serial")
public class menuCtrl extends GenericForwardComposer  {

	@Wire
	private Window wdwMenu;
	@Wire
	private Label lblUser;
        @Wire
	private Label lblDependencia;
        
        
	@Wire
	private A aSalir;
	@Wire
	private Tabbox tbxMenu;
	@Wire
	private Tabs tbsCont;
	@Wire
	private Tabpanels tbpnlCont;
	@Wire
	private Tab tabMenu;
	
	@Wire
	private Listbox lbxMants;
	
	@Wire
	private Listitem itopA;
	@Wire
	private Listitem itopB;
	@Wire
	private Listitem itopC;
	@Wire
	private Listitem itopD;
        @Wire
	private Listitem itopE;
        @Wire
	private Listitem itopF;
        @Wire
	private Listitem itopG;
        @Wire
	private Listitem itopH;
        @Wire
	private Listitem itopI;
        @Wire
	private Listitem itopJ;
        @Wire
	private Listitem itopK;
        @Wire
	private Listitem itopL;
        @Wire
	private Listitem itopM;
        @Wire
	private Listitem itopN;
        @Wire
	private Listitem itopO;
        @Wire
	private Listitem itopP;
        @Wire
	private Listitem itopQ;
        @Wire
	private Listitem itopR;
        @Wire
	private Listitem itopS;
        @Wire
	private Listitem itopT;
               
      
  
        @Wire
        private Groupbox OpAdmin;
	
        
	@SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Component comp) throws Exception {
            Session TicketSession = Sessions.getCurrent();
            String user = TicketSession.getAttribute("USUARIO").toString(); 
            SegusuarioDal User = new SegusuarioDal(); 
            SegusuarioMd sNombre = User.busca(user);
                                
		super.doAfterCompose(comp);

                if ((user.equals("")) || (user.equals(""))||(user.equals(""))) {
                    OpAdmin.setVisible(true);
                }
                itopA.addEventListener("onClick", evtOp);
		itopB.addEventListener("onClick", evtOp);
		itopC.addEventListener("onClick", evtOp);
		itopD.addEventListener("onClick", evtOp);
                itopE.addEventListener("onClick", evtOp);
                itopF.addEventListener("onClick", evtOp);
                itopG.addEventListener("onClick", evtOp);
                itopH.addEventListener("onClick", evtOp);
                itopI.addEventListener("onClick", evtOp);
                // documentos
                itopJ.addEventListener("onClick", evtOp);
                itopK.addEventListener("onClick", evtOp);
                itopL.addEventListener("onClick", evtOp);
//              itopM.addEventListener("onClick", evtOp);
//           	tabMenu.addEventListener("onSelect", evtSelTab);
                lblUser.setValue(desktop.getSession().getAttribute(Commons.USER).toString() + "-" +  sNombre.getNombre() + " " + sNombre.getApellido());
                SegusuarioDal Seg = new SegusuarioDal();
                lblDependencia.setValue(Seg.dependencia_usuario(sNombre.getId_usuario()));
	}
        
        
        /** paginas para cada tabpanel segun la opcion seleccionada*/
	public static final Map<String,String> mapMenu = new HashMap<String, String>(){
		{
                        // Seguridad
			put("opA","/Seguridad/Catperfil.zul");
			put("opB","/Seguridad/Catusuario.zul");
                        put("opC","/Seguridad/Catobjeto.zul");
                        put("opD","/Seguridad/Cattipoobjeto.zul");
                        put("opL","/Seguridad/Catperfilobjeto.zul");
                        // Catalogos
                        put("opE","/Catalogos/Catdependencia.zul");
                        put("opF","/Catalogos/CatEmpresa.zul");
                        put("opG","/Catalogos/CatEstado.zul");
                        put("opH","/Catalogos/CatTema.zul");
                        put("opI","/Catalogos/CatPersona.zul");
                        // Documentos
                        put("opJ","/Documentos/DocFuente.zul");
                        put("opK","/Documentos/BandejaEntrada.zul");
                        
                        
		}
	};
	
	/**titulos para los tabs segun la opcion seleccionada*/
	public static final Map<String, String> mapTitleTabs = new HashMap<String, String>(){
		{
			put("opA","Seguridad Perfiles");
                        put("opB","Usuarios");
                        put("opC","Objetos");
                        put("opD","Seguridad Objetos");
                        put("opE","Catalogo Dependencia");
                        put("opF","Catalogo Empresa");
                        put("opG","Catalogo Estados");
                        put("opH","Catalogo Temas");
                        put("opI","Catalogo Personas");
                        put("opJ","Pre Registro Documento");
                        put("opK","Bandeja de Entrada");
                        put("opL","Asociar Objeto Perfil");
		}
	};

        
        public void onClick$linkSalir(Event evt){
            desktop.getSession().invalidate();
            execution.sendRedirect("/login.zul");
	}
	
        
	@SuppressWarnings("rawtypes")
	EventListener evtOp = new EventListener() {
		@Override
		public void onEvent(Event evt) throws Exception {
			String id = evt.getTarget().getId();
			
			id = id.replace("it", "");
			findTabSel(id);
		}
	};
	
	public void findItemSel(String id){
		lbxMants.clearSelection();
		for (int i = 0; i < lbxMants.getItemCount(); i++) {
			if(lbxMants.getItems().get(i).getId().equals("it"+id)){
				lbxMants.getItems().get(i).setSelected(true);
				break;
			}
		}
	}
	
	public void findTabSel(String id){
		Tab tb;
		int band = 0;
		for (int i = 0; i < tbsCont.getChildren().size(); i++) {
			tb = (Tab) tbsCont.getChildren().get(i);
                       //tbsCont.getChildren().remove(i);
			if(tb.getId().equals("tab"+id)){
				tb.setSelected(true);
				band = 1;
				findItemSel(id);
			}
		}
		
		if (band == 0){
			createNewTab(id);
			createNewTabpanel(id);	
		}
		
	}
	
	@SuppressWarnings("rawtypes")
	EventListener evtSelTab = new EventListener() {

		@Override
		public void onEvent(Event evt) throws Exception {
			String id = evt.getTarget().getId();
			id = id.replace("tab", "");
			findItemSel(id);
		}
	};
	
	@SuppressWarnings("unchecked")
	public void createNewTab(String id){
                Tab tb = new Tab();
                tb.setId("tab"+id);
    		tb.setLabel(mapTitleTabs.get(id));
		tb.setParent(tbsCont);
		tb.setClosable(true);
		tb.setSelected(true);
		tb.addEventListener("onSelect", evtSelTab);
		findItemSel(id);
//                if (tbsCont.getChildren().size() > 2) {
//                    tbsCont.getChildren().remove(1);
//                }
	}
	
	public void createNewTabpanel(String id){
		Tabpanel tbPanel = new Tabpanel();
		tbPanel.setParent(tbpnlCont);
		Include inc = new Include(); 
		inc.setParent(tbPanel);
		inc.setSrc(mapMenu.get(id));
	}
	
        
                  
	
        
}
