package cnee.GestionDoc.environment;

import cnee.GestionDoc.dal.SegusuarioDal;
import cnee.GestionDoc.authbd.authbd;
import cnee.GestionDoc.model.SegusuarioMd;

import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Textbox;

import cnee.GestionDoc.util.Commons;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

public class login extends Commons {

	private static final long serialVersionUID = 1L;
        private Textbox txtUser;
	private Textbox txtPass;
        String[] resp;
        
        
        @Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		
	}
        
	 
        authbd SegepcCtrl = new authbd();
        
	public void onClick$btnLogin(Event evt){
		if(txtUser.getValue() == null ||
			txtUser.getValue().toString().trim().equals("")){
			return;
		}
		
		if(txtPass.getValue() == null ||
                        txtPass.getValue().toString().trim().equals("")){
			return;
		} 
		try {
                        String[] resp = SegepcCtrl.autUser(txtUser.getValue(), txtPass.getValue());
                        
			if(resp[0].equals("ok")){
                                Session InventarioSession = Sessions.getCurrent();
                                InventarioSession.setAttribute("USUARIO", txtUser.getValue());
                                InventarioSession.setAttribute("PASSWORD", txtPass.getValue());
                                desktop.getSession().setAttribute(Commons.USER, txtUser.getValue());
				execution.sendRedirect(Commons.PATH_INICIAL);
			}else{
				Messagebox.show(resp[1], "Informacion", Messagebox.OK, Messagebox.INFORMATION);
			}	
		} catch (Exception e) {
			Messagebox.show("Ocurrió un error al intentar validar el usuario, intente nuevamente.",
					"Informacion", Messagebox.OK, Messagebox.INFORMATION);
		}
	}
	
	public void onOK$txtPass(Event evt){
		onClick$btnLogin(evt);
	}
	
}
