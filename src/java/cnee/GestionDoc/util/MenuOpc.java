
package cnee.GestionDoc.util;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;

@SuppressWarnings("serial")
public class MenuOpc extends SelectorComposer<Component> {

	/** paginas para cada tabpanel segun la opcion seleccionada*/
	public static final Map<String,String> mapMenu = new HashMap<String, String>(){
		{
			put("opA","Views/clientes_1.zul");
			put("opB","Views/plantilla.zul");
			put("opC","Views/plantilla.zul");
			put("opD","Views/plantilla.zul");
		}
	};
	
	/**titulos para los tabs segun la opcion seleccionada*/
	public static final Map<String, String> mapTitleTabs = new HashMap<String, String>(){
		{
			put("opA","Clientes");
			put("opB","Opcion B");
			put("opC","Opcion C");
			put("opD","Opcion D");
		}
	};
	
	
}

