package cnee.GestionDoc.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericForwardComposer;



@SuppressWarnings("rawtypes")
public class Commons extends GenericForwardComposer {

    private static final long serialVersionUID = 1L;
    public static final String APPNAME = "Gestion Documental";
    public static final String USER = "";
    public static final String PATH_INICIAL = "menu.zul";
    
    
	@SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
	}
	
	
	private static final DateFormat fmtFec = new SimpleDateFormat("dd/MM/yyyy");
	private static final DateFormat fmtHr = new SimpleDateFormat("HH:mm:ss");
	private static final DateFormat fmtHrMin = new SimpleDateFormat("HH:mm"); 
	public String getStringOfDateTime(Date f, Date h){
		String result = fmtFec.format(f);
		String hr = fmtHr.format(h);
		return result+" "+hr;
	}
	
	public Date[] getDateOfString(String f){
		String fecha = f.substring(0,10);
		String hora = f.substring(f.indexOf(" ")).trim();
		//System.out.println(fecha);
		//System.out.println(hora);
		Date d_fecha;
		Date d_hora;
		Date[] fh = new Date[2];
		try {
			d_fecha = fmtFec.parse(fecha);
			d_hora = fmtHrMin.parse(hora);
			fh[0] = d_fecha;
			fh[1] = d_hora;
			} catch (ParseException e) {
			e.printStackTrace();
		}
		return fh;
	}

	
	public void salir(){
		desktop.getSession().invalidate();
		execution.sendRedirect("login.zul");
	}
	
	public String getUsuario(){
		if(desktop.getSession().getAttribute(USER) == null){
			salir();
			return "";
		}
		return desktop.getSession().getAttribute(USER).toString();
	}
	
	public boolean validarSesion(){
		if(desktop.getSession().getAttribute(USER) != null){
			return true;
		}
		return false;
	}
	
        
        
        
    
        
}
