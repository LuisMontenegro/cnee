
package cnee.GestionDoc.util;

import cnee.GestionDoc.conexion.conexion;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;

public class Util {

    
    public int Mes_Actual()
        {
            Calendar fec = Calendar.getInstance();
            return fec.get(Calendar.MONTH)+1;
        }     
    
     public void cargaCombox(String paquete, Combobox co) throws SQLException {
            PreparedStatement smt = null;
            Connection conn;
            conexion conex = new conexion();
            conn = conex.getConnection();
            ResultSet rst=null;
	
            co.getItems().clear();	
            
            try {
            
                Comboitem item = new Comboitem();
		smt = conn.prepareStatement(paquete);
                rst = smt.executeQuery();
			
                while(rst.next()){
                        item = new Comboitem();
                        item.setLabel(rst.getString(2));
                        item.setValue(rst.getString(1));
                        item.setParent(co);
                }
                        
                co.setValue(null);    
                     
		}
                catch(SQLException e){
			e.printStackTrace();
                    }finally{
                        if(smt != null){
                              smt.close();
                        }
                        if (rst != null){
                            smt.close();
                        }
                        if(conn!=null){
                            conn = conex.desconectar();
                        }
                    }
        }
     
     
     public int calcula_Dias_Mes(String Mes,String anio){
            int dias =0;
            if(!Mes.equals("All")){
             int mes = Integer.valueOf(Mes);
            
                
                switch (mes) {
                    case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                      dias = 31;
                      break;
                    case 4: case 6: case 9: case 11:
                      dias = 30;
                      break;
                    case 2:			
                      if(!anio.equals("2012") && !anio.equals("2016") && !anio.equals("2020") && !anio.equals("2024")){
                          dias= 28;
                      }else{
                          dias= 29;
                      }
                      break;			
                }
            }
            
            return dias;
        }
     
     public void cargaComboxDias(Combobox co,String Mes,String anio) throws SQLException {
          
            co.getItems().clear();	
            
            int dias = calcula_Dias_Mes(Mes, anio);
            String valor="";
            
            
                Comboitem item = new Comboitem();
		     
                    item = new Comboitem();
                    item.setLabel("All");
                    item.setValue("All");
                    item.setParent(co);
                
                
               for(int i =1; i<=dias; i++){
                   
                        if(i<10){valor="0"+String.valueOf(i);}else{valor=String.valueOf(i);}
                        item = new Comboitem();
                        item.setLabel(valor);
                        item.setValue(valor);
                        item.setParent(co);
                }
                        
                co.setValue(null);    
                     
		}
       
        public void cargaComboxOneValue(String paquete, Combobox co, Boolean AgregaItemDefault) throws SQLException {
            PreparedStatement smt = null;
            Connection conn;
            conexion conex = new conexion();
            conn = conex.getConnection();
            ResultSet rst=null;
	
            co.getItems().clear();	
            
            try {
            
                Comboitem item = new Comboitem();
		smt = conn.prepareStatement(paquete);
                rst = smt.executeQuery();
                    
                if (AgregaItemDefault) {
                    item = new Comboitem();
                    item.setLabel("All");
                    item.setValue("All");
                    item.setParent(co);
                }
                
                while(rst.next()){
                        item = new Comboitem();
                        item.setLabel(rst.getString(1));
                        item.setValue(rst.getString(1));
                        item.setParent(co);
                }
                        
                co.setValue(null);    
                     
		}
                catch(SQLException e){
			e.printStackTrace();
                    }finally{
                        if(smt != null){
                              smt.close();
                        }
                        if (rst != null){
                            smt.close();
                        }
                        if(conn!=null){
                            conn = conex.desconectar();
                        }
                    }
        }
     
        public static String getHora()
        {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            return sdf.format(cal.getTime());
        }
    
	public static String getCurrentDateString(){
		
		Calendar fec = Calendar.getInstance();
		int day = fec.get(Calendar.DATE);
		int month = fec.get(Calendar.MONTH)+1;
		int year = fec.get(Calendar.YEAR);
		int hh = fec.get(Calendar.HOUR_OF_DAY);
		int mi = fec.get(Calendar.MINUTE);
		int ss = fec.get(Calendar.SECOND);
		
		String dd = ""+day;
		if (day >0 && day < 10)
			dd = "0"+dd;
		
		String mes = ""+month;
		if(month > 0 && month<10)
			mes = "0"+mes;
		
		String hr = ""+hh;
		if (hh >= 0 && hh < 10)
			hr = "0"+hr;
		
		String min = ""+mi;
		if(mi >= 0 && mi<10)
			min = "0"+min;
		
		String seg = ""+ss;
		if(ss >= 0 && ss<10)
			seg = "0"+seg;
		
		String fecha = dd+"/"+mes+"/"+year;
		System.out.println(fecha);
		return fecha;
		
	}
        
        
        
        public  String Encriptar(String texto) 
        {
            String secretKey = "cne2019"; //llave para encriptar datos
            String base64EncryptedString = "";

            try {

                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
                byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

                SecretKey key = new SecretKeySpec(keyBytes, "DESede");
                Cipher cipher = Cipher.getInstance("DESede");
                cipher.init(Cipher.ENCRYPT_MODE, key);

                byte[] plainTextBytes = texto.getBytes("utf-8");
                byte[] buf = cipher.doFinal(plainTextBytes);
                byte[] base64Bytes = Base64.encodeBase64(buf);
                base64EncryptedString = new String(base64Bytes);

            } catch (Exception ex) {
            }
            return base64EncryptedString;
        }   
        
        
        public  String Desencriptar(String textoEncriptado) throws Exception 
        {
            String secretKey = "cne2019"; //llave para desenciptar datos
            String base64EncryptedString = "";

            try {
                byte[] message = Base64.decodeBase64(textoEncriptado.getBytes("utf-8"));
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
                byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
                SecretKey key = new SecretKeySpec(keyBytes, "DESede");

                Cipher decipher = Cipher.getInstance("DESede");
                decipher.init(Cipher.DECRYPT_MODE, key);

                byte[] plainText = decipher.doFinal(message);

                base64EncryptedString = new String(plainText, "UTF-8");

            } catch (Exception ex) {
            }
            return base64EncryptedString;
        }
        
	
		
}
