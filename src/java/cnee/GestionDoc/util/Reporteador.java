/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cnee.GestionDoc.util;

import cnee.GestionDoc.conexion.conexion;
import cnee.GestionDoc.util.Commons;
import cnee.GestionDoc.util.Util;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;

import org.zkoss.zul.Window;
import org.zkoss.zk.ui.util.GenericForwardComposer;


/***
 * @author carlosd
 */
public class Reporteador  extends GenericForwardComposer { 
        
    private Window wdwReporte;
	 
       
        @Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
   	}
	
               
           public void Reporteador(String sQuery,Map<String, Object> Parametros, String  sNombreReporte, String sArchivoReporte, org.zkoss.zk.ui.Desktop Escritorio ) throws JRException, IOException, SQLException{
		// Nombre Archivo PDF a generar
                String fecha_rep = Util.getCurrentDateString();
                String User = Escritorio.getSession().getAttribute(Commons.USER).toString();
		fecha_rep = fecha_rep.replace("/", "").replace(" ", "").replace(":", "");
               	String nom_rep = sNombreReporte+User+"_"+fecha_rep+".pdf";
                
                // Conexion a BD
                conexion cc = new conexion();
                Connection cn = cc.getConnection();
                PreparedStatement pst;
                
                // Obtenemos Datos
                pst = cn.prepareStatement(sQuery);
                ResultSet result= pst.executeQuery();
                
                // Transformamos ResulSet en un ResultSet de JasperDataSource
                JRResultSetDataSource dataSource = new JRResultSetDataSource(result);
                
                // Generando el Stream PDF
                InputStream inputStream = Escritorio.getWebApp().getResourceAsStream(File.separator+"rpt"+File.separator+sArchivoReporte);
		JasperReport jr = (JasperReport) JRLoader.loadObject(inputStream);
		
                // Llenando el Reporte con el DataSource
                JasperPrint jasperPrint = JasperFillManager.fillReport(jr, Parametros, dataSource);
                JasperExportManager.exportReportToPdfFile(jasperPrint, Escritorio.getWebApp().getRealPath("rpt")+File.separator+nom_rep);

		// Exportando el archivo del Stream
		File f = new File(Escritorio.getWebApp().getRealPath("rpt")+File.separator+nom_rep);
                byte[] buffer = new byte[(int) f.length()];
                FileInputStream fs = new FileInputStream(f);
                fs.read(buffer);
                fs.close();
        
                ByteArrayInputStream is = new ByteArrayInputStream(buffer);
                AMedia amedia = new AMedia(nom_rep, "pdf", "application/pdf", is);
                is.close();
                f.delete();
        
                // Creando Ventana para visualizar el reporte
                Escritorio.setAttribute("rep_base", amedia);
                Window w = (Window) execution.createComponents("ReportViewer.zul", wdwReporte, null);

                w.doHighlighted();
	}
        
        
    
}
