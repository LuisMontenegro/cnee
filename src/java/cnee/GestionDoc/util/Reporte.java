/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cnee.GestionDoc.util;


import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Iframe;

import cnee.GestionDoc.util.Commons;

public class Reporte extends Commons {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Iframe iframeReport;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		if(desktop.getAttribute("rep_base") != null){
			AMedia amedia = (AMedia) desktop.getAttribute("rep_base");	
			desktop.setAttribute("rep_base", null);
			iframeReport.setContent(amedia);
		}
	}
}
