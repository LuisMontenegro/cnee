/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cnee.GestionDoc.conexion;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author carlosd
 */
public class conexion {
     Connection conn = null;
    
   
    
    public Connection conexion()
    {
        Properties prop = new Properties();
        InputStream is = conexion.class.getResourceAsStream("config.properties");

        String driver = "";
        String db = "";
        String port = "";
        String sid = "";
        String user = "";
        String pwd = "";

         try {
                prop.load(is);
                
                driver = prop.getProperty("driver");
                db = prop.getProperty("ipdb");
                port = prop.getProperty("port");
                sid = prop.getProperty("sid");
                user = prop.getProperty("user");
                pwd = prop.getProperty("pwd");

                Class.forName(driver);
                
                conn = DriverManager.getConnection ("jdbc:mysql://localhost/cnee","root", "");
			
                return conn;
                
        } catch (IOException  e) {
           return null;
        } catch (ClassNotFoundException e) {
            return null;
        } catch (SQLException e) {
            return null;
        }
    }
    
   
    public Connection getConnection(){
	return conexion();
    }
	
    public Connection desconectar() throws SQLException{
	conn.close();   
        conn=null;
        return conn;
        
    }
}
