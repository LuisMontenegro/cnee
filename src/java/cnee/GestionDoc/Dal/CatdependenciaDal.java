

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.CatdependenciaMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class CatdependenciaDal {

	public int Crear(CatdependenciaMd CatdependenciaDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.cat_dependencia (codigo,descripcion,activo) values (?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setString(1,CatdependenciaDAL.getCodigo());
			smt.setString(2,CatdependenciaDAL.getDescripcion());
			smt.setInt(3,CatdependenciaDAL.getActivo());
			
			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(CatdependenciaMd CatdependenciaDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.cat_dependencia set Codigo = ?, Descripcion = ?, Activo = ? where Codigo = ? ";

			smt = conn.prepareStatement(sql);
			smt.setString(1,CatdependenciaDAL.getCodigo());
			smt.setString(2,CatdependenciaDAL.getDescripcion());
			smt.setInt(3,CatdependenciaDAL.getActivo());
                        smt.setString(4,CatdependenciaDAL.getCodigo());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public CatdependenciaMd busca(String sCodigo)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		CatdependenciaMd Buscar = new CatdependenciaMd(null, null, 0, null, null, null, null);

		String sql = "Select * from cnee.cat_dependencia where Codigo = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setString(1, sCodigo);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setCodigo(result.getString("codigo"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(String sCodigo)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Codigo from cnee.cat_dependencia where Codigo = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setString(1, sCodigo);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}

        
        
        public int NextValue()
	{
            PreparedStatement smt = null;
            Connection conn;
            conexion conex = new conexion();
            conn = conex.getConnection();
            int maximo =0;

            String sql = "Select Max(codigo)+1 as codigo"
                    + " from cnee.cat_dependencia";

            try {
                    smt = conn.prepareStatement(sql);
                    ResultSet result = smt.executeQuery();

                    while (result.next())
                    {
                        maximo = result.getInt("codigo");
                    }
            }catch(Exception e){
	   }
            return maximo;
	}

	public int Eliminar(String sCodigo)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.cat_dependencia where Codigo = ? ";

			smt = conn.prepareStatement(sql);
			smt.setString(1, sCodigo);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<CatdependenciaMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<CatdependenciaMd> lstCatdependencia = new ArrayList<CatdependenciaMd>();

		try{
			  
                        sql = "SELECT  codigo, ifnull(descripcion,' ') descripcion, activo, ifnull(Fecha_Ingreso,' ') fecha_ingreso, ifnull(usuario_ingreso,' ') usuario_ingreso, "
                                + " ifnull(Fecha_Modificacion,' ') fecha_modificacion,   ifnull(Usuario_Modificacion,' ') Usuario_Modificacion   FROM cnee.cat_dependencia order by codigo desc;";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			CatdependenciaMd Buscar = new CatdependenciaMd(" ", " ", 0, " ", " ", " ", " ");

			Buscar.setCodigo(result.getString("codigo"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			lstCatdependencia.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstCatdependencia;
	}
}

