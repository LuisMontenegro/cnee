

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.CatempresaMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class CatempresaDal {

	public int Crear(CatempresaMd CatempresaDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.cat_empresa values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setString(1,CatempresaDAL.getCodigo());
			smt.setString(2,CatempresaDAL.getNombre());
			smt.setString(3,CatempresaDAL.getDireccion());
			smt.setString(4,CatempresaDAL.getTelefono());
			smt.setString(5,CatempresaDAL.getFax());
			smt.setString(6,CatempresaDAL.getEmail());
			smt.setString(7,CatempresaDAL.getWeb());
			smt.setString(8,CatempresaDAL.getAreageo());
			smt.setString(9,CatempresaDAL.getCodcategoria());
			smt.setInt(10,CatempresaDAL.getActivo());
			smt.setString(11,CatempresaDAL.getFecha_ingreso());
			smt.setString(12,CatempresaDAL.getUsuario_ingreso());
			smt.setString(13,CatempresaDAL.getFecha_modificacion());
			smt.setString(14,CatempresaDAL.getUsuario_modificacion());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(CatempresaMd CatempresaDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.cat_empresa set Codigo = ?, Nombre = ?, Direccion = ?, Telefono = ?, Fax = ?, Email = ?, Web = ?, Areageo = ?, Codcategoria = ?, Activo = ?, Fecha_ingreso = ?, Usuario_ingreso = ?, Fecha_modificacion = ?, Usuario_modificacion = ?  where Codigo = ? ";

			smt = conn.prepareStatement(sql);
			smt.setString(1,CatempresaDAL.getCodigo());
			smt.setString(2,CatempresaDAL.getNombre());
			smt.setString(3,CatempresaDAL.getDireccion());
			smt.setString(4,CatempresaDAL.getTelefono());
			smt.setString(5,CatempresaDAL.getFax());
			smt.setString(6,CatempresaDAL.getEmail());
			smt.setString(7,CatempresaDAL.getWeb());
			smt.setString(8,CatempresaDAL.getAreageo());
			smt.setString(9,CatempresaDAL.getCodcategoria());
			smt.setInt(10,CatempresaDAL.getActivo());
			smt.setString(11,CatempresaDAL.getFecha_ingreso());
			smt.setString(12,CatempresaDAL.getUsuario_ingreso());
			smt.setString(13,CatempresaDAL.getFecha_modificacion());
			smt.setString(14,CatempresaDAL.getUsuario_modificacion());
			smt.setString(15,CatempresaDAL.getCodigo());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public CatempresaMd busca(String sCodigo)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		CatempresaMd Buscar = new CatempresaMd(null, null, null, null, null, null, null, null, null, 0, null, null, null, null);

		String sql = "Select * from cnee.cat_empresa where Codigo = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setString(1, sCodigo);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setCodigo(result.getString("codigo"));
			Buscar.setNombre(result.getString("nombre"));
			Buscar.setDireccion(result.getString("direccion"));
			Buscar.setTelefono(result.getString("telefono"));
			Buscar.setFax(result.getString("fax"));
			Buscar.setEmail(result.getString("email"));
			Buscar.setWeb(result.getString("web"));
			Buscar.setAreageo(result.getString("areageo"));
			Buscar.setCodcategoria(result.getString("codcategoria"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(int sCodigo)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Codigo from cnee.cat_empresa where Codigo = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sCodigo);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(int sCodigo)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.cat_empresa where Codigo = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1, sCodigo);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<CatempresaMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<CatempresaMd> lstCatempresa = new ArrayList<CatempresaMd>();

		try{
			sql = "select ifnull(codigo,' ') codigo, ifnull(nombre,' ') nombre, ifnull(direccion,' ') direccion, ifnull(telefono,' ') telefono, "
                                + "ifnull(fax,' ') fax, ifnull(email,' ') email, ifnull(web, ' ') web, ifnull(areageo, ' ') areageo, ifnull(codcategoria,' ') codcategoria, "
                                + "activo from cat_empresa ";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			CatempresaMd Buscar = new CatempresaMd(null, null, null, null, null, null, null, null, null, 0, null, null, null, null);

			Buscar.setCodigo(result.getString("codigo"));
			Buscar.setNombre(result.getString("nombre"));
			Buscar.setDireccion(result.getString("direccion"));
			Buscar.setTelefono(result.getString("telefono"));
			Buscar.setFax(result.getString("fax"));
			Buscar.setEmail(result.getString("email"));
			Buscar.setWeb(result.getString("web"));
			Buscar.setAreageo(result.getString("areageo"));
			Buscar.setCodcategoria(result.getString("codcategoria"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso("");
			Buscar.setUsuario_ingreso("");
			Buscar.setFecha_modificacion("");
			Buscar.setUsuario_modificacion("");
			lstCatempresa.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstCatempresa;
	}
}

