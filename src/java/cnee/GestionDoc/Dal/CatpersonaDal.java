

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.CatpersonaMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class CatpersonaDal {

	public int Crear(CatpersonaMd CatpersonaDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.cat_persona values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setInt(1,CatpersonaDAL.getId());
			smt.setString(2,CatpersonaDAL.getCodigo());
			smt.setString(3,CatpersonaDAL.getCodempresa());
			smt.setString(4,CatpersonaDAL.getNombres());
			smt.setString(5,CatpersonaDAL.getApellidos());
			smt.setString(6,CatpersonaDAL.getSexo());
			smt.setString(7,CatpersonaDAL.getTitulo());
			smt.setString(8,CatpersonaDAL.getPuesto());
			smt.setString(9,CatpersonaDAL.getDireccion());
			smt.setString(10,CatpersonaDAL.getEmail());
			smt.setInt(11,CatpersonaDAL.getAreageografica());
			smt.setString(12,CatpersonaDAL.getTelefono());
			smt.setString(13,CatpersonaDAL.getTelefonomovil());
			smt.setString(14,CatpersonaDAL.getEmercontacto());
			smt.setString(15,CatpersonaDAL.getEmercontactotel());
			smt.setString(16,CatpersonaDAL.getFecha_ingreso());
			smt.setString(17,CatpersonaDAL.getUsuario_ingreso());
			smt.setString(18,CatpersonaDAL.getFecha_modificacion());
			smt.setString(19,CatpersonaDAL.getUsuario_modificacion());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(CatpersonaMd CatpersonaDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.cat_persona set Id = ?, Codigo = ?, Codempresa = ?, Nombres = ?, Apellidos = ?, Sexo = ?, Titulo = ?, Puesto = ?, Direccion = ?, Email = ?, Areageografica = ?, Telefono = ?, Telefonomovil = ?, Emercontacto = ?, Emercontactotel = ?, Fecha_ingreso = ?, Usuario_ingreso = ?, Fecha_modificacion = ?, Usuario_modificacion = ?  where Id = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1,CatpersonaDAL.getId());
			smt.setString(2,CatpersonaDAL.getCodigo());
			smt.setString(3,CatpersonaDAL.getCodempresa());
			smt.setString(4,CatpersonaDAL.getNombres());
			smt.setString(5,CatpersonaDAL.getApellidos());
			smt.setString(6,CatpersonaDAL.getSexo());
			smt.setString(7,CatpersonaDAL.getTitulo());
			smt.setString(8,CatpersonaDAL.getPuesto());
			smt.setString(9,CatpersonaDAL.getDireccion());
			smt.setString(10,CatpersonaDAL.getEmail());
			smt.setInt(11,CatpersonaDAL.getAreageografica());
			smt.setString(12,CatpersonaDAL.getTelefono());
			smt.setString(13,CatpersonaDAL.getTelefonomovil());
			smt.setString(14,CatpersonaDAL.getEmercontacto());
			smt.setString(15,CatpersonaDAL.getEmercontactotel());
			smt.setString(16,CatpersonaDAL.getFecha_ingreso());
			smt.setString(17,CatpersonaDAL.getUsuario_ingreso());
			smt.setString(18,CatpersonaDAL.getFecha_modificacion());
			smt.setString(19,CatpersonaDAL.getUsuario_modificacion());
			smt.setInt(20,CatpersonaDAL.getId());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public CatpersonaMd busca(int sId)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		CatpersonaMd Buscar = new CatpersonaMd(0, null, null, null, null, null, null, null, null, null, 0, null, null, null, null, null, null, null, null);

		String sql = "Select * from cnee.cat_persona where Id = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setId(result.getInt("id"));
			Buscar.setCodigo(result.getString("codigo"));
			Buscar.setCodempresa(result.getString("codempresa"));
			Buscar.setNombres(result.getString("nombres"));
			Buscar.setApellidos(result.getString("apellidos"));
			Buscar.setSexo(result.getString("sexo"));
			Buscar.setTitulo(result.getString("titulo"));
			Buscar.setPuesto(result.getString("puesto"));
			Buscar.setDireccion(result.getString("direccion"));
			Buscar.setEmail(result.getString("email"));
			Buscar.setAreageografica(result.getInt("areageografica"));
			Buscar.setTelefono(result.getString("telefono"));
			Buscar.setTelefonomovil(result.getString("telefonomovil"));
			Buscar.setEmercontacto(result.getString("emercontacto"));
			Buscar.setEmercontactotel(result.getString("emercontactotel"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(int sId)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Id from cnee.cat_persona where Id = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(int sId)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.cat_persona where Id = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<CatpersonaMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<CatpersonaMd> lstCatpersona = new ArrayList<CatpersonaMd>();

		try{
			sql = "select  id, ifnull(codigo,'') codigo, ifnull(codempresa,'') codempresa, ifnull(nombres,'') nombres, "
                            + " ifnull(apellidos,'') apellidos, ifnull(sexo,'') sexo, ifnull(titulo,'') titulo, ifnull(puesto,'') puesto, "
                            + " ifnull(direccion,'') direccion, ifnull(email,'') email, ifnull(areageografica,'') areageografica, "
                            + " ifnull(telefono,'') telefono, ifnull(telefonomovil,'') telefonomovil, ifnull(emercontacto,'') emercontacto,"
                            + " ifnull(emercontactotel,'') emercontactotel  from cnee.cat_persona order by 1";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			CatpersonaMd Buscar = new CatpersonaMd(0, null, null, null, null, null, null, null, null, null, 0, null, null, null, null, null, null, null, null);

			Buscar.setId(result.getInt("id"));
			Buscar.setCodigo(result.getString("codigo"));
			Buscar.setCodempresa(result.getString("codempresa"));
			Buscar.setNombres(result.getString("nombres"));
			Buscar.setApellidos(result.getString("apellidos"));
			Buscar.setSexo(result.getString("sexo"));
			Buscar.setTitulo(result.getString("titulo"));
			Buscar.setPuesto(result.getString("puesto"));
			Buscar.setDireccion(result.getString("direccion"));
			Buscar.setEmail(result.getString("email"));
			Buscar.setAreageografica(result.getInt("areageografica"));
			Buscar.setTelefono(result.getString("telefono"));
			Buscar.setTelefonomovil(result.getString("telefonomovil"));
			Buscar.setEmercontacto(result.getString("emercontacto"));
			Buscar.setEmercontactotel(result.getString("emercontactotel"));
			Buscar.setFecha_ingreso("");
			Buscar.setUsuario_ingreso("");
			Buscar.setFecha_modificacion("");
			Buscar.setUsuario_modificacion("");
			lstCatpersona.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstCatpersona;
	}
}

