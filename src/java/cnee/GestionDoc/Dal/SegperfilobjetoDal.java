

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.SegperfilobjetoMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class SegperfilobjetoDal {

	public int Crear(SegperfilobjetoMd SegperfilobjetoDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.seg_perfil_objeto values (?,?,?,?,?,?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setInt(1,SegperfilobjetoDAL.getId_perfil_objeto());
			smt.setInt(2,SegperfilobjetoDAL.getId_perfil());
			smt.setInt(3,SegperfilobjetoDAL.getId_objeto());
			smt.setInt(4,SegperfilobjetoDAL.getActivo());
			smt.setString(5,SegperfilobjetoDAL.getFecha_ingreso());
			smt.setString(6,SegperfilobjetoDAL.getUsuario_ingreso());
			smt.setString(7,SegperfilobjetoDAL.getFecha_modificacion());
			smt.setString(8,SegperfilobjetoDAL.getUsuario_modificacion());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(SegperfilobjetoMd SegperfilobjetoDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.seg_perfil_objeto set Id_perfil_objeto = ?, Id_perfil = ?, Id_objeto = ?, Activo = ?, Fecha_ingreso = ?, Usuario_ingreso = ?, Fecha_modificacion = ?, Usuario_modificacion = ?  where Id_perfil_objeto = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1,SegperfilobjetoDAL.getId_perfil_objeto());
			smt.setInt(2,SegperfilobjetoDAL.getId_perfil());
			smt.setInt(3,SegperfilobjetoDAL.getId_objeto());
			smt.setInt(4,SegperfilobjetoDAL.getActivo());
			smt.setString(5,SegperfilobjetoDAL.getFecha_ingreso());
			smt.setString(6,SegperfilobjetoDAL.getUsuario_ingreso());
			smt.setString(7,SegperfilobjetoDAL.getFecha_modificacion());
			smt.setString(8,SegperfilobjetoDAL.getUsuario_modificacion());
			smt.setInt(9,SegperfilobjetoDAL.getId_perfil_objeto());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public SegperfilobjetoMd busca(int sId_perfil_objeto)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		SegperfilobjetoMd Buscar = new SegperfilobjetoMd(0, 0, 0, 0, null, null, null, null);

		String sql = "Select * from cnee.seg_perfil_objeto where Id_perfil_objeto = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_perfil_objeto);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setId_perfil_objeto(result.getInt("id_perfil_objeto"));
			Buscar.setId_perfil(result.getInt("id_perfil"));
			Buscar.setId_objeto(result.getInt("id_objeto"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(int sId_perfil_objeto)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Id_perfil_objeto from cnee.seg_perfil_objeto where Id_perfil_objeto = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_perfil_objeto);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(int sId_perfil_objeto)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.seg_perfil_objeto where Id_perfil_objeto = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_perfil_objeto);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<SegperfilobjetoMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<SegperfilobjetoMd> lstSegperfilobjeto = new ArrayList<SegperfilobjetoMd>();

		try{
			sql = "select * from cnee.seg_perfil_objeto order by 1";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			SegperfilobjetoMd Buscar = new SegperfilobjetoMd(0, 0, 0, 0, null, null, null, null);

			Buscar.setId_perfil_objeto(result.getInt("id_perfil_objeto"));
			Buscar.setId_perfil(result.getInt("id_perfil"));
			Buscar.setId_objeto(result.getInt("id_objeto"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			lstSegperfilobjeto.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstSegperfilobjeto;
	}
}

