

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.SegtipoobjetoMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class SegtipoobjetoDal {

	public int Crear(SegtipoobjetoMd SegtipoobjetoDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.seg_tipo_objeto values (?,?,?,?,?,?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setInt(1,SegtipoobjetoDAL.getId_tipo_objeto());
			smt.setString(2,SegtipoobjetoDAL.getNombre());
			smt.setString(3,SegtipoobjetoDAL.getDescripcion());
			smt.setInt(4,SegtipoobjetoDAL.getActivo());
			smt.setString(5,SegtipoobjetoDAL.getFecha_ingreso());
			smt.setString(6,SegtipoobjetoDAL.getUsuario_ingreso());
			smt.setString(7,SegtipoobjetoDAL.getFecha_modificacion());
			smt.setString(8,SegtipoobjetoDAL.getUsuario_modificacion());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(SegtipoobjetoMd SegtipoobjetoDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.seg_tipo_objeto set Id_tipo_objeto = ?, Nombre = ?, Descripcion = ?, Activo = ?, Fecha_ingreso = ?, Usuario_ingreso = ?, Fecha_modificacion = ?, Usuario_modificacion = ?  where Id_tipo_objeto = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1,SegtipoobjetoDAL.getId_tipo_objeto());
			smt.setString(2,SegtipoobjetoDAL.getNombre());
			smt.setString(3,SegtipoobjetoDAL.getDescripcion());
			smt.setInt(4,SegtipoobjetoDAL.getActivo());
			smt.setString(5,SegtipoobjetoDAL.getFecha_ingreso());
			smt.setString(6,SegtipoobjetoDAL.getUsuario_ingreso());
			smt.setString(7,SegtipoobjetoDAL.getFecha_modificacion());
			smt.setString(8,SegtipoobjetoDAL.getUsuario_modificacion());
			smt.setInt(9,SegtipoobjetoDAL.getId_tipo_objeto());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public SegtipoobjetoMd busca(int sId_tipo_objeto)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		SegtipoobjetoMd Buscar = new SegtipoobjetoMd(0, null, null, 0, null, null, null, null);

		String sql = "Select * from cnee.seg_tipo_objeto where Id_tipo_objeto = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_tipo_objeto);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setId_tipo_objeto(result.getInt("id_tipo_objeto"));
			Buscar.setNombre(result.getString("nombre"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(int sId_tipo_objeto)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Id_tipo_objeto from cnee.seg_tipo_objeto where Id_tipo_objeto = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_tipo_objeto);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(int sId_tipo_objeto)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.seg_tipo_objeto where Id_tipo_objeto = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_tipo_objeto);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<SegtipoobjetoMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<SegtipoobjetoMd> lstSegtipoobjeto = new ArrayList<SegtipoobjetoMd>();

		try{
                            sql = "select id_tipo_objeto, ifnull(nombre,'') nombre, ifnull(descripcion,'') descripcion, activo  from cnee.seg_tipo_objeto order by 1";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			SegtipoobjetoMd Buscar = new SegtipoobjetoMd(0, null, null, 0, null, null, null, null);

			Buscar.setId_tipo_objeto(result.getInt("id_tipo_objeto"));
			Buscar.setNombre(result.getString("nombre"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso("");
			Buscar.setUsuario_ingreso("");
			Buscar.setFecha_modificacion("");
			Buscar.setUsuario_modificacion("");
			lstSegtipoobjeto.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstSegtipoobjeto;
	}
}

