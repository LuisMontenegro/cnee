

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.SegusuarioMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class SegusuarioDal {

	public int Crear(SegusuarioMd SegusuarioDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.seg_usuario values (?,?,?,?,?,?,?,?,?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setInt(1,SegusuarioDAL.getId_usuario());
			smt.setString(2,SegusuarioDAL.getCoddep());
			smt.setString(3,SegusuarioDAL.getUsuario_login());
			smt.setString(4,SegusuarioDAL.getPassword());
			smt.setString(5,SegusuarioDAL.getNombre());
			smt.setString(6,SegusuarioDAL.getApellido());
			smt.setInt(7,SegusuarioDAL.getActivo());
			smt.setString(8,SegusuarioDAL.getFecha_ingreso());
			smt.setString(9,SegusuarioDAL.getUsuario_ingreso());
			smt.setString(10,SegusuarioDAL.getFecha_modificacion());
			smt.setString(11,SegusuarioDAL.getUsuario_modificacion());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(SegusuarioMd SegusuarioDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.seg_usuario set Id_usuario = ?, Coddep = ?, Usuario_login = ?, Password = ?, Nombre = ?, Apellido = ?, Activo = ?, Fecha_ingreso = ?, Usuario_ingreso = ?, Fecha_modificacion = ?, Usuario_modificacion = ?  where Id_usuario = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1,SegusuarioDAL.getId_usuario());
			smt.setString(2,SegusuarioDAL.getCoddep());
			smt.setString(3,SegusuarioDAL.getUsuario_login());
			smt.setString(4,SegusuarioDAL.getPassword());
			smt.setString(5,SegusuarioDAL.getNombre());
			smt.setString(6,SegusuarioDAL.getApellido());
			smt.setInt(7,SegusuarioDAL.getActivo());
			smt.setString(8,SegusuarioDAL.getFecha_ingreso());
			smt.setString(9,SegusuarioDAL.getUsuario_ingreso());
			smt.setString(10,SegusuarioDAL.getFecha_modificacion());
			smt.setString(11,SegusuarioDAL.getUsuario_modificacion());
			smt.setInt(12,SegusuarioDAL.getId_usuario());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public SegusuarioMd busca(String sId_usuario)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		SegusuarioMd Buscar = new SegusuarioMd(0, null, null, null, null, null, 0, null, null, null, null);

		String sql = "Select * from cnee.seg_usuario where usuario_login = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setString(1, sId_usuario);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setId_usuario(result.getInt("id_usuario"));
			Buscar.setCoddep(result.getString("coddep"));
			Buscar.setUsuario_login(result.getString("usuario_login"));
			Buscar.setPassword(result.getString("password"));
			Buscar.setNombre(result.getString("nombre"));
			Buscar.setApellido(result.getString("apellido"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(int sId_usuario)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Id_usuario from cnee.seg_usuario where Id_usuario = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_usuario);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(int sId_usuario)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.seg_usuario where Id_usuario = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_usuario);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<SegusuarioMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<SegusuarioMd> lstSegusuario = new ArrayList<SegusuarioMd>();

		try{
			sql = "select id_usuario, ifnull(coddep,'') coddep, ifnull(usuario_login,'') usuario_login, ifnull(password,'') password, ifnull(nombre,'') nombre, " +
                              " ifnull(apellido,'') apellido, activo " +   
                              " from cnee.seg_usuario order by 1";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			SegusuarioMd Buscar = new SegusuarioMd(0, null, null, null, null, null, 0, null, null, null, null);

			Buscar.setId_usuario(result.getInt("id_usuario"));
			Buscar.setCoddep(result.getString("coddep"));
			Buscar.setUsuario_login(result.getString("usuario_login"));
			Buscar.setPassword(result.getString("password"));
			Buscar.setNombre(result.getString("nombre"));
			Buscar.setApellido(result.getString("apellido"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso("");
			Buscar.setUsuario_ingreso("");
			Buscar.setFecha_modificacion("");
			Buscar.setUsuario_modificacion("");
			lstSegusuario.add(Buscar);		
                }
		}catch(Exception e){
	   }
	return lstSegusuario;
	}
        
        public String dependencia_usuario(int sId_usuario)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
                String resultado = " ";

		String sql = " select descripcion from cat_dependencia where codigo in (select coddep from seg_usuario where "
                        + "   (Id_usuario = ?)) ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_usuario);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				resultado = result.getString("descripcion");  
			}
		}catch(Exception e){
	   }
	return resultado;
	}
 
        
        public int NextValue()
	{
            PreparedStatement smt = null;
            Connection conn;
            conexion conex = new conexion();
            conn = conex.getConnection();
            int maximo =0;

            String sql = "Select Max(id_usuario)+1 as codigo"
                    + " from cnee.seg_usuario";

            try {
                    smt = conn.prepareStatement(sql);
                    ResultSet result = smt.executeQuery();

                    while (result.next())
                    {
                        maximo = result.getInt("codigo");
                    }
            }catch(Exception e){
	   }
            return maximo;
	}

        
        public String perfil_usuario(int sId_usuario)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
                String resultado = " ";

		String sql = "Select  b.nombre as perfil  FROM cnee.seg_perfil_usuario a,  cnee.seg_perfil b,  cnee.seg_usuario c "
                        + " Where a.id_perfil = b.id_perfil and a.id_usuario = c.id_usuario and c.id_usuario = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_usuario);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				resultado = result.getString("perfil");  
			}
		}catch(Exception e){
	   }
	return resultado;
	}
        
        
        
}

