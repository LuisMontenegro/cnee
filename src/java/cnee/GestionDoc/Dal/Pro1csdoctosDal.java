

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.Pro1csdoctosMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class Pro1csdoctosDal {

	public int Crear(Pro1csdoctosMd Pro1csdoctosDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.pro1_csdoctos values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setString(1,Pro1csdoctosDAL.getTiporeg());
			smt.setString(2,Pro1csdoctosDAL.getDepencod());
			smt.setString(3,Pro1csdoctosDAL.getDoccod());
			smt.setString(4,Pro1csdoctosDAL.getDocnum());
			smt.setString(5,Pro1csdoctosDAL.getDocref());
			smt.setString(6,Pro1csdoctosDAL.getFechaemision());
			smt.setString(7,Pro1csdoctosDAL.getFecharecep());
			smt.setString(8,Pro1csdoctosDAL.getHorarecep());
			smt.setString(9,Pro1csdoctosDAL.getTema());
			smt.setString(10,Pro1csdoctosDAL.getDescripcion());
			smt.setString(11,Pro1csdoctosDAL.getOrigpersona());
			smt.setString(12,Pro1csdoctosDAL.getOrigempresa());
			smt.setString(13,Pro1csdoctosDAL.getOrigdependencia());
			smt.setString(14,Pro1csdoctosDAL.getDestpersona());
			smt.setString(15,Pro1csdoctosDAL.getDestempresa());
			smt.setString(16,Pro1csdoctosDAL.getDestdependencia());
			smt.setString(17,Pro1csdoctosDAL.getEstado());
			smt.setString(18,Pro1csdoctosDAL.getObservaciones());
			smt.setString(19,Pro1csdoctosDAL.getDocusovis());
			smt.setString(20,Pro1csdoctosDAL.getLastdepencod());
			smt.setString(21,Pro1csdoctosDAL.getLastpersona());
			smt.setString(22,Pro1csdoctosDAL.getLastfecha());
			smt.setString(23,Pro1csdoctosDAL.getLasthora());
			smt.setString(24,Pro1csdoctosDAL.getDocfojas());
			smt.setInt(25,Pro1csdoctosDAL.getMonto());
			smt.setString(26,Pro1csdoctosDAL.getUsuariosaccesos());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(Pro1csdoctosMd Pro1csdoctosDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.pro1_csdoctos set Tiporeg = ?, Depencod = ?, Doccod = ?, Docnum = ?, Docref = ?, Fechaemision = ?, Fecharecep = ?, Horarecep = ?, Tema = ?, Descripcion = ?, Origpersona = ?, Origempresa = ?, Origdependencia = ?, Destpersona = ?, Destempresa = ?, Destdependencia = ?, Estado = ?, Observaciones = ?, Docusovis = ?, Lastdepencod = ?, Lastpersona = ?, Lastfecha = ?, Lasthora = ?, Docfojas = ?, Monto = ?, Usuariosaccesos = ?  where Tiporeg = ? ";

			smt = conn.prepareStatement(sql);
			smt.setString(1,Pro1csdoctosDAL.getTiporeg());
			smt.setString(2,Pro1csdoctosDAL.getDepencod());
			smt.setString(3,Pro1csdoctosDAL.getDoccod());
			smt.setString(4,Pro1csdoctosDAL.getDocnum());
			smt.setString(5,Pro1csdoctosDAL.getDocref());
			smt.setString(6,Pro1csdoctosDAL.getFechaemision());
			smt.setString(7,Pro1csdoctosDAL.getFecharecep());
			smt.setString(8,Pro1csdoctosDAL.getHorarecep());
			smt.setString(9,Pro1csdoctosDAL.getTema());
			smt.setString(10,Pro1csdoctosDAL.getDescripcion());
			smt.setString(11,Pro1csdoctosDAL.getOrigpersona());
			smt.setString(12,Pro1csdoctosDAL.getOrigempresa());
			smt.setString(13,Pro1csdoctosDAL.getOrigdependencia());
			smt.setString(14,Pro1csdoctosDAL.getDestpersona());
			smt.setString(15,Pro1csdoctosDAL.getDestempresa());
			smt.setString(16,Pro1csdoctosDAL.getDestdependencia());
			smt.setString(17,Pro1csdoctosDAL.getEstado());
			smt.setString(18,Pro1csdoctosDAL.getObservaciones());
			smt.setString(19,Pro1csdoctosDAL.getDocusovis());
			smt.setString(20,Pro1csdoctosDAL.getLastdepencod());
			smt.setString(21,Pro1csdoctosDAL.getLastpersona());
			smt.setString(22,Pro1csdoctosDAL.getLastfecha());
			smt.setString(23,Pro1csdoctosDAL.getLasthora());
			smt.setString(24,Pro1csdoctosDAL.getDocfojas());
			smt.setInt(25,Pro1csdoctosDAL.getMonto());
			smt.setString(26,Pro1csdoctosDAL.getUsuariosaccesos());
			smt.setString(27,Pro1csdoctosDAL.getTiporeg());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public Pro1csdoctosMd busca(String sTiporeg)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		Pro1csdoctosMd Buscar = new Pro1csdoctosMd(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, null);

		String sql = "Select * from cnee.pro1_csdoctos where Tiporeg = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setString(1, sTiporeg);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setTiporeg(result.getString("tiporeg"));
			Buscar.setDepencod(result.getString("depencod"));
			Buscar.setDoccod(result.getString("doccod"));
			Buscar.setDocnum(result.getString("docnum"));
			Buscar.setDocref(result.getString("docref"));
			Buscar.setFechaemision(result.getString("fechaemision"));
			Buscar.setFecharecep(result.getString("fecharecep"));
			Buscar.setHorarecep(result.getString("horarecep"));
			Buscar.setTema(result.getString("tema"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setOrigpersona(result.getString("origpersona"));
			Buscar.setOrigempresa(result.getString("origempresa"));
			Buscar.setOrigdependencia(result.getString("origdependencia"));
			Buscar.setDestpersona(result.getString("destpersona"));
			Buscar.setDestempresa(result.getString("destempresa"));
			Buscar.setDestdependencia(result.getString("destdependencia"));
			Buscar.setEstado(result.getString("estado"));
			Buscar.setObservaciones(result.getString("observaciones"));
			Buscar.setDocusovis(result.getString("docusovis"));
			Buscar.setLastdepencod(result.getString("lastdepencod"));
			Buscar.setLastpersona(result.getString("lastpersona"));
			Buscar.setLastfecha(result.getString("lastfecha"));
			Buscar.setLasthora(result.getString("lasthora"));
			Buscar.setDocfojas(result.getString("docfojas"));
			Buscar.setMonto(result.getInt("monto"));
			Buscar.setUsuariosaccesos(result.getString("usuariosaccesos"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(String sTiporeg)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Tiporeg from cnee.pro1_csdoctos where Tiporeg = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setString(1, sTiporeg);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(String sTiporeg)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.pro1_csdoctos where Tiporeg = ? ";

			smt = conn.prepareStatement(sql);
			smt.setString(1, sTiporeg);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<Pro1csdoctosMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<Pro1csdoctosMd> lstPro1csdoctos = new ArrayList<Pro1csdoctosMd>();

		try{
			sql = "select  DATE_FORMAT(fecharecep,'%d/%m/%Y') fecharecep, horarecep, doccod, tiporeg,depencod,docNum,docref,DATE_FORMAT(fechaemision,'%d/%m/%Y') fechaemision,  horarecep,tema,descripcion,origpersona,origempresa,origdependencia,destpersona,destempresa,destdependencia,estado,observaciones,docusovis,lastdepencod,lastpersona,DATE_FORMAT(LastFecha,'%d/%m/%Y') lastfecha,lasthora,docfojas,monto,usuariosaccesos from cnee.pro1_csdoctos;";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			Pro1csdoctosMd Buscar = new Pro1csdoctosMd(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, null);

			Buscar.setTiporeg(result.getString("tiporeg"));
			Buscar.setDepencod(result.getString("depencod"));
			Buscar.setDoccod(result.getString("doccod"));
			Buscar.setDocnum(result.getString("docnum"));
			Buscar.setDocref(result.getString("docref"));
			Buscar.setFechaemision(result.getString("fechaemision"));
			Buscar.setFecharecep(result.getString("fecharecep"));
			Buscar.setHorarecep(result.getString("horarecep"));
			Buscar.setTema(result.getString("tema"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setOrigpersona(result.getString("origpersona"));
			Buscar.setOrigempresa(result.getString("origempresa"));
			Buscar.setOrigdependencia(result.getString("origdependencia"));
			Buscar.setDestpersona(result.getString("destpersona"));
			Buscar.setDestempresa(result.getString("destempresa"));
			Buscar.setDestdependencia(result.getString("destdependencia"));
			Buscar.setEstado(result.getString("estado"));
			Buscar.setObservaciones(result.getString("observaciones"));
			Buscar.setDocusovis(result.getString("docusovis"));
			Buscar.setLastdepencod(result.getString("lastdepencod"));
			Buscar.setLastpersona(result.getString("lastpersona"));
			Buscar.setLastfecha(result.getString("lastfecha"));
			Buscar.setLasthora(result.getString("lasthora"));
			Buscar.setDocfojas(result.getString("docfojas"));
			Buscar.setMonto(result.getInt("monto"));
			Buscar.setUsuariosaccesos(result.getString("usuariosaccesos"));
			lstPro1csdoctos.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstPro1csdoctos;
	}
}

