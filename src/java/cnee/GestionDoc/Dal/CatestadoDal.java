

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.CatestadoMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class CatestadoDal {

	public int Crear(CatestadoMd CatestadoDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.cat_estado values (?,?,?,?,?,?,?,?,?,?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setInt(1,CatestadoDAL.getId());
			smt.setString(2,CatestadoDAL.getCoddep());
			smt.setString(3,CatestadoDAL.getEstado());
			smt.setString(4,CatestadoDAL.getDescripcion());
			smt.setInt(5,CatestadoDAL.getDias());
			smt.setString(6,CatestadoDAL.getHoras());
			smt.setString(7,CatestadoDAL.getActivo());
			smt.setString(8,CatestadoDAL.getEnviamail());
			smt.setString(9,CatestadoDAL.getUsuario_ingreso());
			smt.setString(10,CatestadoDAL.getFecha_ingreso());
			smt.setString(11,CatestadoDAL.getFecha_modificacion());
			smt.setString(12,CatestadoDAL.getUsuario_modificacion());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(CatestadoMd CatestadoDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.cat_estado set Id = ?, Coddep = ?, Estado = ?, Descripcion = ?, Dias = ?, Horas = ?, Activo = ?, Enviamail = ?, Usuario_ingreso = ?, Fecha_ingreso = ?, Fecha_modificacion = ?, Usuario_modificacion = ?  where Id = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1,CatestadoDAL.getId());
			smt.setString(2,CatestadoDAL.getCoddep());
			smt.setString(3,CatestadoDAL.getEstado());
			smt.setString(4,CatestadoDAL.getDescripcion());
			smt.setInt(5,CatestadoDAL.getDias());
			smt.setString(6,CatestadoDAL.getHoras());
			smt.setString(7,CatestadoDAL.getActivo());
			smt.setString(8,CatestadoDAL.getEnviamail());
			smt.setString(9,CatestadoDAL.getUsuario_ingreso());
			smt.setString(10,CatestadoDAL.getFecha_ingreso());
			smt.setString(11,CatestadoDAL.getFecha_modificacion());
			smt.setString(12,CatestadoDAL.getUsuario_modificacion());
			smt.setInt(13,CatestadoDAL.getId());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public CatestadoMd busca(int sId)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		CatestadoMd Buscar = new CatestadoMd(0, null, null, null, 0, null, null, null, null, null, null, null);

		String sql = "Select * from cnee.cat_estado where Id = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setId(result.getInt("id"));
			Buscar.setCoddep(result.getString("coddep"));
			Buscar.setEstado(result.getString("estado"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setDias(result.getInt("dias"));
			Buscar.setHoras(result.getString("horas"));
			Buscar.setActivo(result.getString("activo"));
			Buscar.setEnviamail(result.getString("enviamail"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(int sId)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Id from cnee.cat_estado where Id = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(int sId)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.cat_estado where Id = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<CatestadoMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<CatestadoMd> lstCatestado = new ArrayList<CatestadoMd>();

		try{
			sql = "select id, ifnull(coddep,'') coddep, ifnull(estado,'') estado,  ifnull(descripcion,'') descripcion,  "
                            + "dias, ifnull(horas,'') horas, ifnull(activo,'') activo, ifnull(enviamail,'') enviamail "
                            + "from cnee.cat_estado order by 1";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			CatestadoMd Buscar = new CatestadoMd(0, null, null, null, 0, null, null, null, null, null, null, null);

			Buscar.setId(result.getInt("id"));
			Buscar.setCoddep(result.getString("coddep"));
			Buscar.setEstado(result.getString("estado"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setDias(result.getInt("dias"));
			Buscar.setHoras(result.getString("horas"));
			Buscar.setActivo(result.getString("activo"));
			Buscar.setEnviamail(result.getString("enviamail"));
			Buscar.setUsuario_ingreso("");
			Buscar.setFecha_ingreso("");
			Buscar.setFecha_modificacion("");
			Buscar.setUsuario_modificacion("");
			lstCatestado.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstCatestado;
	}
}

