

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.SegperfilMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class SegperfilDal {

	public int Crear(SegperfilMd SegperfilDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.seg_perfil values (?,?,?,?,?,?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setInt(1,SegperfilDAL.getId_perfil());
			smt.setString(2,SegperfilDAL.getNombre());
			smt.setString(3,SegperfilDAL.getDescripcion());
			smt.setInt(4,SegperfilDAL.getEstatus());
			smt.setString(5,SegperfilDAL.getFecha_ingreso());
			smt.setString(6,SegperfilDAL.getUsuario_ingreso());
			smt.setString(7,SegperfilDAL.getFecha_modificacion());
			smt.setString(8,SegperfilDAL.getUsuario_modificacion());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(SegperfilMd SegperfilDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.seg_perfil set Id_perfil = ?, Nombre = ?, Descripcion = ?, Estatus = ?, Fecha_ingreso = ?, Usuario_ingreso = ?, Fecha_modificacion = ?, Usuario_modificacion = ?  where Id_perfil = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1,SegperfilDAL.getId_perfil());
			smt.setString(2,SegperfilDAL.getNombre());
			smt.setString(3,SegperfilDAL.getDescripcion());
			smt.setInt(4,SegperfilDAL.getEstatus());
			smt.setString(5,SegperfilDAL.getFecha_ingreso());
			smt.setString(6,SegperfilDAL.getUsuario_ingreso());
			smt.setString(7,SegperfilDAL.getFecha_modificacion());
			smt.setString(8,SegperfilDAL.getUsuario_modificacion());
			smt.setInt(9,SegperfilDAL.getId_perfil());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public SegperfilMd busca(int sId_perfil)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		SegperfilMd Buscar = new SegperfilMd(0, null, null, 0, null, null, null, null);

		String sql = "Select * from cnee.seg_perfil where Id_perfil = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_perfil);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setId_perfil(result.getInt("id_perfil"));
			Buscar.setNombre(result.getString("nombre"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setEstatus(result.getInt("estatus"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(int sId_perfil)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Id_perfil from cnee.seg_perfil where Id_perfil = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_perfil);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(int sId_perfil)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.seg_perfil where Id_perfil = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_perfil);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<SegperfilMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<SegperfilMd> lstSegperfil = new ArrayList<SegperfilMd>();

		try{
			sql = "select id_perfil, ifnull(nombre,'') nombre, ifnull(descripcion,'') descripcion, estatus  from cnee.seg_perfil order by 1";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			SegperfilMd Buscar = new SegperfilMd(0, null, null, 0, null, null, null, null);

			Buscar.setId_perfil(result.getInt("id_perfil"));
			Buscar.setNombre(result.getString("nombre"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setEstatus(result.getInt("estatus"));
			Buscar.setFecha_ingreso("");
			Buscar.setUsuario_ingreso("");
			Buscar.setFecha_modificacion("");
			Buscar.setUsuario_modificacion("");
			lstSegperfil.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstSegperfil;
	}
}

