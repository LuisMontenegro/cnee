

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.DocpredocumentoMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class DocpredocumentoDal {

	public int Crear(DocpredocumentoMd DocpredocumentoDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.doc_predocumento values (?,?,?,?,?,?,?,?,?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setInt(1,DocpredocumentoDAL.getCodigo());
			smt.setString(2,DocpredocumentoDAL.getCodigoorigen());
			smt.setString(3,DocpredocumentoDAL.getEmpresaorigen());
			smt.setString(4,DocpredocumentoDAL.getPersonaorigen());
			smt.setString(5,DocpredocumentoDAL.getDestino());
			smt.setString(6,DocpredocumentoDAL.getTemas());
			smt.setString(7,DocpredocumentoDAL.getFecha_ingreso());
			smt.setString(8,DocpredocumentoDAL.getHora_ingreso());
			smt.setString(9,DocpredocumentoDAL.getUsuario_ingreso());
			smt.setString(10,DocpredocumentoDAL.getFecha_modificacion());
			smt.setString(11,DocpredocumentoDAL.getUsuario_modificacion());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(DocpredocumentoMd DocpredocumentoDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.doc_predocumento set Codigo = ?, Codigoorigen = ?, Empresaorigen = ?, Personaorigen = ?, Destino = ?, Temas = ?, Fecha_ingreso = ?, Hora_ingreso = ?, Usuario_ingreso = ?, Fecha_modificacion = ?, Usuario_modificacion = ?  where Codigo = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1,DocpredocumentoDAL.getCodigo());
			smt.setString(2,DocpredocumentoDAL.getCodigoorigen());
			smt.setString(3,DocpredocumentoDAL.getEmpresaorigen());
			smt.setString(4,DocpredocumentoDAL.getPersonaorigen());
			smt.setString(5,DocpredocumentoDAL.getDestino());
			smt.setString(6,DocpredocumentoDAL.getTemas());
			smt.setString(7,DocpredocumentoDAL.getFecha_ingreso());
			smt.setString(8,DocpredocumentoDAL.getHora_ingreso());
			smt.setString(9,DocpredocumentoDAL.getUsuario_ingreso());
			smt.setString(10,DocpredocumentoDAL.getFecha_modificacion());
			smt.setString(11,DocpredocumentoDAL.getUsuario_modificacion());
			smt.setInt(12,DocpredocumentoDAL.getCodigo());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public DocpredocumentoMd busca(int sCodigo)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		DocpredocumentoMd Buscar = new DocpredocumentoMd(0, null, null, null, null, null, null, null, null, null, null);

		String sql = "Select * from cnee.doc_predocumento where Codigo = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sCodigo);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setCodigo(result.getInt("codigo"));
			Buscar.setCodigoorigen(result.getString("codigoorigen"));
			Buscar.setEmpresaorigen(result.getString("empresaorigen"));
			Buscar.setPersonaorigen(result.getString("personaorigen"));
			Buscar.setDestino(result.getString("destino"));
			Buscar.setTemas(result.getString("temas"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setHora_ingreso(result.getString("hora_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(int sCodigo)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Codigo from cnee.doc_predocumento where Codigo = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sCodigo);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(int sCodigo)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.doc_predocumento where Codigo = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1, sCodigo);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<DocpredocumentoMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<DocpredocumentoMd> lstDocpredocumento = new ArrayList<DocpredocumentoMd>();

		try{
			sql = "select * from cnee.doc_predocumento order by 1";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			DocpredocumentoMd Buscar = new DocpredocumentoMd(0, null, null, null, null, null, null, null, null, null, null);

			Buscar.setCodigo(result.getInt("codigo"));
			Buscar.setCodigoorigen(result.getString("codigoorigen"));
			Buscar.setEmpresaorigen(result.getString("empresaorigen"));
			Buscar.setPersonaorigen(result.getString("personaorigen"));
			Buscar.setDestino(result.getString("destino"));
			Buscar.setTemas(result.getString("temas"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setHora_ingreso(result.getString("hora_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			lstDocpredocumento.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstDocpredocumento;
	}
        
        
        
        
         public int NextValue()
	{
            PreparedStatement smt = null;
            Connection conn;
            conexion conex = new conexion();
            conn = conex.getConnection();
            int maximo =0;

            String sql = "Select ifnull(max(Codigo),0)+1 as codigo"
                    + " from cnee.doc_predocumento";

            try {
                    smt = conn.prepareStatement(sql);
                    ResultSet result = smt.executeQuery();

                    while (result.next())
                    {
                        maximo = result.getInt("codigo");
                    }
            }catch(Exception e){
	   }
            return maximo;
	}

}

