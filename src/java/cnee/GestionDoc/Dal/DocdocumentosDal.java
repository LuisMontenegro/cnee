

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.DocdocumentosMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class DocdocumentosDal {

	public int Crear(DocdocumentosMd DocdocumentosDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.doc_documentos values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setInt(1,DocdocumentosDAL.getCodigo());
			smt.setString(2,DocdocumentosDAL.getCoddependencia());
			smt.setString(3,DocdocumentosDAL.getCodtema());
			smt.setString(4,DocdocumentosDAL.getCodtipodocto());
			smt.setString(5,DocdocumentosDAL.getDocnum());
			smt.setString(6,DocdocumentosDAL.getDocref());
			smt.setString(7,DocdocumentosDAL.getFechaemision());
			smt.setString(8,DocdocumentosDAL.getFecharecep());
			smt.setString(9,DocdocumentosDAL.getHorarecep());
			smt.setString(10,DocdocumentosDAL.getDescripcion());
			smt.setString(11,DocdocumentosDAL.getOrigpersona());
			smt.setString(12,DocdocumentosDAL.getOrigempresa());
			smt.setString(13,DocdocumentosDAL.getOrigdependencia());
			smt.setInt(14,DocdocumentosDAL.getDestpersona());
			smt.setInt(15,DocdocumentosDAL.getDestempresa());
			smt.setString(16,DocdocumentosDAL.getDestdependencia());
			smt.setInt(17,DocdocumentosDAL.getCodestado());
			smt.setString(18,DocdocumentosDAL.getObservaciones());
			smt.setInt(19,DocdocumentosDAL.getActivo());
			smt.setString(20,DocdocumentosDAL.getFecha_ingreso());
			smt.setString(21,DocdocumentosDAL.getUsuario_ingreso());
			smt.setString(22,DocdocumentosDAL.getFecha_modificacion());
			smt.setString(23,DocdocumentosDAL.getUsuario_modificacion());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(DocdocumentosMd DocdocumentosDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.doc_documentos set Codigo = ?, Coddependencia = ?, Codtema = ?, Codtipodocto = ?, Docnum = ?, Docref = ?, Fechaemision = ?, Fecharecep = ?, Horarecep = ?, Descripcion = ?, Origpersona = ?, Origempresa = ?, Origdependencia = ?, Destpersona = ?, Destempresa = ?, Destdependencia = ?, Codestado = ?, Observaciones = ?, Activo = ?, Fecha_ingreso = ?, Usuario_ingreso = ?, Fecha_modificacion = ?, Usuario_modificacion = ?  where Codigo = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1,DocdocumentosDAL.getCodigo());
			smt.setString(2,DocdocumentosDAL.getCoddependencia());
			smt.setString(3,DocdocumentosDAL.getCodtema());
			smt.setString(4,DocdocumentosDAL.getCodtipodocto());
			smt.setString(5,DocdocumentosDAL.getDocnum());
			smt.setString(6,DocdocumentosDAL.getDocref());
			smt.setString(7,DocdocumentosDAL.getFechaemision());
			smt.setString(8,DocdocumentosDAL.getFecharecep());
			smt.setString(9,DocdocumentosDAL.getHorarecep());
			smt.setString(10,DocdocumentosDAL.getDescripcion());
			smt.setString(11,DocdocumentosDAL.getOrigpersona());
			smt.setString(12,DocdocumentosDAL.getOrigempresa());
			smt.setString(13,DocdocumentosDAL.getOrigdependencia());
			smt.setInt(14,DocdocumentosDAL.getDestpersona());
			smt.setInt(15,DocdocumentosDAL.getDestempresa());
			smt.setString(16,DocdocumentosDAL.getDestdependencia());
			smt.setInt(17,DocdocumentosDAL.getCodestado());
			smt.setString(18,DocdocumentosDAL.getObservaciones());
			smt.setInt(19,DocdocumentosDAL.getActivo());
			smt.setString(20,DocdocumentosDAL.getFecha_ingreso());
			smt.setString(21,DocdocumentosDAL.getUsuario_ingreso());
			smt.setString(22,DocdocumentosDAL.getFecha_modificacion());
			smt.setString(23,DocdocumentosDAL.getUsuario_modificacion());
			smt.setInt(24,DocdocumentosDAL.getCodigo());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public DocdocumentosMd busca(int sCodigo)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		DocdocumentosMd Buscar = new DocdocumentosMd(0, null, null, null, null, null, null, null, null, null, null, null, null, 0, 0, null, 0, null, 0, null, null, null, null);

		String sql = "Select * from cnee.doc_documentos where Codigo = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sCodigo);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setCodigo(result.getInt("codigo"));
			Buscar.setCoddependencia(result.getString("coddependencia"));
			Buscar.setCodtema(result.getString("codtema"));
			Buscar.setCodtipodocto(result.getString("codtipodocto"));
			Buscar.setDocnum(result.getString("docnum"));
			Buscar.setDocref(result.getString("docref"));
			Buscar.setFechaemision(result.getString("fechaemision"));
			Buscar.setFecharecep(result.getString("fecharecep"));
			Buscar.setHorarecep(result.getString("horarecep"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setOrigpersona(result.getString("origpersona"));
			Buscar.setOrigempresa(result.getString("origempresa"));
			Buscar.setOrigdependencia(result.getString("origdependencia"));
			Buscar.setDestpersona(result.getInt("destpersona"));
			Buscar.setDestempresa(result.getInt("destempresa"));
			Buscar.setDestdependencia(result.getString("destdependencia"));
			Buscar.setCodestado(result.getInt("codestado"));
			Buscar.setObservaciones(result.getString("observaciones"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(int sCodigo)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Codigo from cnee.doc_documentos where Codigo = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sCodigo);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(int sCodigo)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.doc_documentos where Codigo = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1, sCodigo);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<DocdocumentosMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<DocdocumentosMd> lstDocdocumentos = new ArrayList<DocdocumentosMd>();

		try{
			sql = "select * from cnee.doc_documentos order by 1";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			DocdocumentosMd Buscar = new DocdocumentosMd(0, null, null, null, null, null, null, null, null, null, null, null, null, 0, 0, null, 0, null, 0, null, null, null, null);

			Buscar.setCodigo(result.getInt("codigo"));
			Buscar.setCoddependencia(result.getString("coddependencia"));
			Buscar.setCodtema(result.getString("codtema"));
			Buscar.setCodtipodocto(result.getString("codtipodocto"));
			Buscar.setDocnum(result.getString("docnum"));
			Buscar.setDocref(result.getString("docref"));
			Buscar.setFechaemision(result.getString("fechaemision"));
			Buscar.setFecharecep(result.getString("fecharecep"));
			Buscar.setHorarecep(result.getString("horarecep"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setOrigpersona(result.getString("origpersona"));
			Buscar.setOrigempresa(result.getString("origempresa"));
			Buscar.setOrigdependencia(result.getString("origdependencia"));
			Buscar.setDestpersona(result.getInt("destpersona"));
			Buscar.setDestempresa(result.getInt("destempresa"));
			Buscar.setDestdependencia(result.getString("destdependencia"));
			Buscar.setCodestado(result.getInt("codestado"));
			Buscar.setObservaciones(result.getString("observaciones"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			lstDocdocumentos.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstDocdocumentos;
	}
}

