

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.SegperfilusuarioMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class SegperfilusuarioDal {

	public int Crear(SegperfilusuarioMd SegperfilusuarioDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.seg_perfil_usuario (id_perfil, id_usuario,activo)  values (?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setInt(1,SegperfilusuarioDAL.getId_perfil());
			smt.setInt(2,SegperfilusuarioDAL.getId_usuario());
			smt.setInt(3,SegperfilusuarioDAL.getActivo());
			//smt.setString(4,SegperfilusuarioDAL.getFecha_ingreso());
			//smt.setString(5,SegperfilusuarioDAL.getUsuario_ingreso());
			//smt.setString(6,SegperfilusuarioDAL.getFecha_modificacion());
			//smt.setString(7,SegperfilusuarioDAL.getUsuario_modificacion());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(SegperfilusuarioMd SegperfilusuarioDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.seg_perfil_usuario set Id_perfil = ?, Id_usuario = ?, Activo = ?  where Id_usuario = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1,SegperfilusuarioDAL.getId_perfil());
			smt.setInt(2,SegperfilusuarioDAL.getId_usuario());
			smt.setInt(3,SegperfilusuarioDAL.getActivo());
			smt.setInt(4,SegperfilusuarioDAL.getId_usuario());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public SegperfilusuarioMd busca(int sId_perfil_usuario)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		SegperfilusuarioMd Buscar = new SegperfilusuarioMd(0, 0, 0, 0, null, null, null, null);

		String sql = "Select * from cnee.seg_perfil_usuario where Id_perfil_usuario = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_perfil_usuario);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setId_perfil_usuario(result.getInt("id_perfil_usuario"));
			Buscar.setId_perfil(result.getInt("id_perfil"));
			Buscar.setId_usuario(result.getInt("id_usuario"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(int idUsuario)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Id_perfil_usuario from cnee.seg_perfil_usuario where id_usuario  = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, idUsuario);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(int sId_perfil_usuario)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.seg_perfil_usuario where Id_perfil_usuario = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_perfil_usuario);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<SegperfilusuarioMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<SegperfilusuarioMd> lstSegperfilusuario = new ArrayList<SegperfilusuarioMd>();

		try{
			sql = "select * from cnee.seg_perfil_usuario order by 1";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			SegperfilusuarioMd Buscar = new SegperfilusuarioMd(0, 0, 0, 0, null, null, null, null);

			Buscar.setId_perfil_usuario(result.getInt("id_perfil_usuario"));
			Buscar.setId_perfil(result.getInt("id_perfil"));
			Buscar.setId_usuario(result.getInt("id_usuario"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			lstSegperfilusuario.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstSegperfilusuario;
	}
}

