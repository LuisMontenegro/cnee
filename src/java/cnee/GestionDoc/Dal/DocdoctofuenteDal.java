

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.DocdoctofuenteMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class DocdoctofuenteDal {

	public int Crear(DocdoctofuenteMd DocdoctofuenteDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.doc_doctofuente values (?,?,?,?,?,?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setInt(1,DocdoctofuenteDAL.getCodigo());
			smt.setInt(2,DocdoctofuenteDAL.getDoctoorigen());
			smt.setString(3,DocdoctofuenteDAL.getFechacreado());
			smt.setString(4,DocdoctofuenteDAL.getHoracreado());
			smt.setString(5,DocdoctofuenteDAL.getUsuario());
			smt.setString(6,DocdoctofuenteDAL.getDocubica());
			smt.setString(7,DocdoctofuenteDAL.getObservaciones());
			smt.setString(8,DocdoctofuenteDAL.getFormato());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(DocdoctofuenteMd DocdoctofuenteDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.doc_doctofuente set Codigo = ?, Doctoorigen = ?, Fechacreado = ?, Horacreado = ?, Usuario = ?, Docubica = ?, Observaciones = ?, Formato = ?  where Codigo = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1,DocdoctofuenteDAL.getCodigo());
			smt.setInt(2,DocdoctofuenteDAL.getDoctoorigen());
			smt.setString(3,DocdoctofuenteDAL.getFechacreado());
			smt.setString(4,DocdoctofuenteDAL.getHoracreado());
			smt.setString(5,DocdoctofuenteDAL.getUsuario());
			smt.setString(6,DocdoctofuenteDAL.getDocubica());
			smt.setString(7,DocdoctofuenteDAL.getObservaciones());
			smt.setString(8,DocdoctofuenteDAL.getFormato());
			smt.setInt(9,DocdoctofuenteDAL.getCodigo());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public DocdoctofuenteMd busca(int sCodigo)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		DocdoctofuenteMd Buscar = new DocdoctofuenteMd(0, 0, null, null, null, null, null, null);

		String sql = "Select * from cnee.doc_doctofuente where Codigo = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sCodigo);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setCodigo(result.getInt("codigo"));
			Buscar.setDoctoorigen(result.getInt("doctoorigen"));
			Buscar.setFechacreado(result.getString("fechacreado"));
			Buscar.setHoracreado(result.getString("horacreado"));
			Buscar.setUsuario(result.getString("usuario"));
			Buscar.setDocubica(result.getString("docubica"));
			Buscar.setObservaciones(result.getString("observaciones"));
			Buscar.setFormato(result.getString("formato"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(int sCodigo)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Codigo from cnee.doc_doctofuente where Codigo = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sCodigo);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(int sCodigo)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.doc_doctofuente where Codigo = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1, sCodigo);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<DocdoctofuenteMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<DocdoctofuenteMd> lstDocdoctofuente = new ArrayList<DocdoctofuenteMd>();

		try{
			sql = "select * from cnee.doc_doctofuente order by 1";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			DocdoctofuenteMd Buscar = new DocdoctofuenteMd(0, 0, null, null, null, null, null, null);

			Buscar.setCodigo(result.getInt("codigo"));
			Buscar.setDoctoorigen(result.getInt("doctoorigen"));
			Buscar.setFechacreado(result.getString("fechacreado"));
			Buscar.setHoracreado(result.getString("horacreado"));
			Buscar.setUsuario(result.getString("usuario"));
			Buscar.setDocubica(result.getString("docubica"));
			Buscar.setObservaciones(result.getString("observaciones"));
			Buscar.setFormato(result.getString("formato"));
			lstDocdoctofuente.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstDocdoctofuente;
	}
}

