

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.CattemasMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class CattemasDal {

	public int Crear(CattemasMd CattemasDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.cat_temas values (?,?,?,?,?,?,?,?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setInt(1,CattemasDAL.getId());
			smt.setString(2,CattemasDAL.getDepencod());
			smt.setString(3,CattemasDAL.getTema());
			smt.setString(4,CattemasDAL.getTemades());
			smt.setString(5,CattemasDAL.getClasif());
			smt.setString(6,CattemasDAL.getActivo());
			smt.setString(7,CattemasDAL.getUsuario_ingreso());
			smt.setString(8,CattemasDAL.getFecha_ingreso());
			smt.setString(9,CattemasDAL.getFecha_modificacion());
			smt.setString(10,CattemasDAL.getUsuario_modificacion());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(CattemasMd CattemasDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.cat_temas set Id = ?, Depencod = ?, Tema = ?, Temades = ?, Clasif = ?, Activo = ?, Usuario_ingreso = ?, Fecha_ingreso = ?, Fecha_modificacion = ?, Usuario_modificacion = ?  where Id = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1,CattemasDAL.getId());
			smt.setString(2,CattemasDAL.getDepencod());
			smt.setString(3,CattemasDAL.getTema());
			smt.setString(4,CattemasDAL.getTemades());
			smt.setString(5,CattemasDAL.getClasif());
			smt.setString(6,CattemasDAL.getActivo());
			smt.setString(7,CattemasDAL.getUsuario_ingreso());
			smt.setString(8,CattemasDAL.getFecha_ingreso());
			smt.setString(9,CattemasDAL.getFecha_modificacion());
			smt.setString(10,CattemasDAL.getUsuario_modificacion());
			smt.setInt(11,CattemasDAL.getId());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public CattemasMd busca(int sId)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		CattemasMd Buscar = new CattemasMd(0, null, null, null, null, null, null, null, null, null);

		String sql = "Select * from cnee.cat_temas where Id = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setId(result.getInt("id"));
			Buscar.setDepencod(result.getString("depencod"));
			Buscar.setTema(result.getString("tema"));
			Buscar.setTemades(result.getString("temades"));
			Buscar.setClasif(result.getString("clasif"));
			Buscar.setActivo(result.getString("activo"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(int sId)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Id from cnee.cat_temas where Id = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(int sId)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.cat_temas where Id = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<CattemasMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<CattemasMd> lstCattemas = new ArrayList<CattemasMd>();

		try{
			sql = "select id, ifnull(depencod,'') depencod, ifnull(tema,'') tema, ifnull(temades,'') temades, ifnull(clasif,'') clasif, ifnull(activo,'') activo \n" +
                              " from cnee.cat_temas	";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			CattemasMd Buscar = new CattemasMd(0, null, null, null, null, null, null, null, null, null);

			Buscar.setId(result.getInt("id"));
			Buscar.setDepencod(result.getString("depencod"));
			Buscar.setTema(result.getString("tema"));
			Buscar.setTemades(result.getString("temades"));
			Buscar.setClasif(result.getString("clasif"));
			Buscar.setActivo(result.getString("activo"));
			Buscar.setUsuario_ingreso("");
			Buscar.setFecha_ingreso("");
			Buscar.setFecha_modificacion("");
			Buscar.setUsuario_modificacion("");
			lstCattemas.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstCattemas;
	}
}

