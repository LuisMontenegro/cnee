

package cnee.GestionDoc.dal;

import cnee.GestionDoc.model.SegobjetoMd; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import cnee.GestionDoc.conexion.conexion;
import java.sql.Connection; 
import java.sql.ResultSet; 
import java.util.ArrayList; 
import java.util.List; 


public class SegobjetoDal {

	public int Crear(SegobjetoMd SegobjetoDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "insert into cnee.seg_objeto values (?,?,?,?,?,?,?,?,?,?,?)";
		
			smt = conn.prepareStatement(sql);
			smt.setInt(1,SegobjetoDAL.getId_objeto());
			smt.setInt(2,SegobjetoDAL.getId_tipo_objeto());
			smt.setString(3,SegobjetoDAL.getNombre());
			smt.setString(4,SegobjetoDAL.getDescripcion());
			smt.setInt(5,SegobjetoDAL.getActivo());
			smt.setString(6,SegobjetoDAL.getFecha_ingreso());
			smt.setString(7,SegobjetoDAL.getUsuario_ingreso());
			smt.setString(8,SegobjetoDAL.getFecha_modificacion());
			smt.setString(9,SegobjetoDAL.getUsuario_modificacion());
			smt.setInt(10,SegobjetoDAL.getId_padre());
			smt.setString(11,SegobjetoDAL.getUrl());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}

	}
	}


	public int Modificar(SegobjetoMd SegobjetoDAL)throws SQLException{
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=0;

		try {
			String sql = "update cnee.seg_objeto set Id_objeto = ?, Id_tipo_objeto = ?, Nombre = ?, Descripcion = ?, Activo = ?, Fecha_ingreso = ?, Usuario_ingreso = ?, Fecha_modificacion = ?, Usuario_modificacion = ?, Id_padre = ?, Url = ?  where Id_objeto = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1,SegobjetoDAL.getId_objeto());
			smt.setInt(2,SegobjetoDAL.getId_tipo_objeto());
			smt.setString(3,SegobjetoDAL.getNombre());
			smt.setString(4,SegobjetoDAL.getDescripcion());
			smt.setInt(5,SegobjetoDAL.getActivo());
			smt.setString(6,SegobjetoDAL.getFecha_ingreso());
			smt.setString(7,SegobjetoDAL.getUsuario_ingreso());
			smt.setString(8,SegobjetoDAL.getFecha_modificacion());
			smt.setString(9,SegobjetoDAL.getUsuario_modificacion());
			smt.setInt(10,SegobjetoDAL.getId_padre());
			smt.setString(11,SegobjetoDAL.getUrl());
			smt.setInt(12,SegobjetoDAL.getId_objeto());

			result = smt.executeUpdate();
			return result;

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	}
	}


	public SegobjetoMd busca(int sId_objeto)
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		SegobjetoMd Buscar = new SegobjetoMd(0, 0, null, null, 0, null, null, null, null, 0, null);

		String sql = "Select * from cnee.seg_objeto where Id_objeto = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_objeto);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
			Buscar.setId_objeto(result.getInt("id_objeto"));
			Buscar.setId_tipo_objeto(result.getInt("id_tipo_objeto"));
			Buscar.setNombre(result.getString("nombre"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso(result.getString("fecha_ingreso"));
			Buscar.setUsuario_ingreso(result.getString("usuario_ingreso"));
			Buscar.setFecha_modificacion(result.getString("fecha_modificacion"));
			Buscar.setUsuario_modificacion(result.getString("usuario_modificacion"));
			Buscar.setId_padre(result.getInt("id_padre"));
			Buscar.setUrl(result.getString("url"));
			}
		}catch(Exception e){
	   }
	return Buscar;
	}


	public boolean existe(int sId_objeto)
	{
		boolean existente = false;
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();

		String sql = "Select Id_objeto from cnee.seg_objeto where Id_objeto = ? ";

		try {
			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_objeto);

			ResultSet result = smt.executeQuery();

			while (result.next())
			{
				existente = true;
			}
		}catch(Exception e){
	   }
	return existente;
	}


	public int Eliminar(int sId_objeto)throws SQLException
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		int result=-1;

		try {
		String sql = "delete  from cnee.seg_objeto where Id_objeto = ? ";

			smt = conn.prepareStatement(sql);
			smt.setInt(1, sId_objeto);

			result = smt.executeUpdate();

		} finally {
				if(smt != null){
					smt.close();
					conex.desconectar();
				}
		}
	return result;

	}

	public List<SegobjetoMd> getAll()
	{
		PreparedStatement smt = null;
		Connection conn;
		conexion conex = new conexion();
		conn = conex.getConnection();
		String sql = null;

		List<SegobjetoMd> lstSegobjeto = new ArrayList<SegobjetoMd>();

		try{
			sql = "select id_objeto, id_tipo_objeto, nombre, descripcion, activo, id_padre, ifnull(url,'') url  from cnee.seg_objeto order by 1";

			smt = conn.prepareStatement(sql);
			ResultSet result = smt.executeQuery();

		while (result.next())
		{
			SegobjetoMd Buscar = new SegobjetoMd(0, 0, null, null, 0, null, null, null, null, 0, null);

			Buscar.setId_objeto(result.getInt("id_objeto"));
			Buscar.setId_tipo_objeto(result.getInt("id_tipo_objeto"));
			Buscar.setNombre(result.getString("nombre"));
			Buscar.setDescripcion(result.getString("descripcion"));
			Buscar.setActivo(result.getInt("activo"));
			Buscar.setFecha_ingreso("");
			Buscar.setUsuario_ingreso("");
			Buscar.setFecha_modificacion("");
			Buscar.setUsuario_modificacion("");
			Buscar.setId_padre(result.getInt("id_padre"));
			Buscar.setUrl(result.getString("url"));
			lstSegobjeto.add(Buscar);		}
		}catch(Exception e){
	   }
	return lstSegobjeto;
	}
        
        
          
        public int NextValue()
	{
            PreparedStatement smt = null;
            Connection conn;
            conexion conex = new conexion();
            conn = conex.getConnection();
            int maximo =0;

            String sql = "Select Max(id_objeto)+1 as codigo"
                    + " from cnee.seg_objeto";

            try {
                    smt = conn.prepareStatement(sql);
                    ResultSet result = smt.executeQuery();

                    while (result.next())
                    {
                        maximo = result.getInt("codigo");
                    }
            }catch(Exception e){
	   }
            return maximo;
	}
}

