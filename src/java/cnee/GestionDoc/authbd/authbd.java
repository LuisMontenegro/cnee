/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cnee.GestionDoc.authbd;

import cnee.GestionDoc.dal.SegusuarioDal;
import cnee.GestionDoc.model.SegusuarioMd;
import cnee.GestionDoc.util.Util;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author carlosd
 */
public class authbd {
    
    Util Decryptar = new Util();
    
    
    public String[] autUser(String user, String pass) throws Exception{
    	
        String[] result = new String[2];
        result[0] = "fail";
        result[1] = "No se pudo validar el usuario, ocurrio un error al conectar con la base de datos.";

    	Properties prop = new Properties();
    	InputStream is = authbd.class.getResourceAsStream("config.properties");
    	
    	String driver = "";
    	String db = "";
    	String port = "";
    	String sid = "";
        String sUser = "";
        String sPwd = "";
        String sPwdDecryot = "";
        String sPwdEncrypt = "";
    	
    	Connection conn = null;
		
        try {
			prop.load(is);
			driver = prop.getProperty("driver");
                        db = prop.getProperty("ipdb");
                        port = prop.getProperty("port");
                        sid = prop.getProperty("sid");
                        sUser = prop.getProperty("user");
                        sPwd = prop.getProperty("pwd");

                        Class.forName(driver);

                        conn = DriverManager.getConnection ("jdbc:mysql://localhost/cnee","root", "");
			
                        if (conn != null) {
                               SegusuarioMd Usuario = null;
                               Usuario = new SegusuarioMd(0, null, null, null, null, null, 0, null, null, null, null);
                               SegusuarioDal UserValidar = new SegusuarioDal();
                               Usuario = UserValidar.busca(user);
                               sPwdEncrypt = Decryptar.Encriptar(pass);
                                
                                if (Usuario.getPassword() == null) {
                                    result[0] = "fail";
                                    result[1] = "Credenciales no válidas";
                                } else {
                                    
                                    if (Usuario.getActivo()== 1) {
                                        String sPassword  = Decryptar.Desencriptar(Usuario.getPassword());
                                        
                                        if (sPassword.trim().equals(pass))  {
                                            result[0] = "ok";
                                            result[1] = "Usuario valido";
                                        } else {
                                            result[0] = "fail";
                                            result[1] = "Password No válido";
                                        }
                                    } else {
                                        result[0] = "fail";
                                        result[1] = "Usuario Bloqueado";
                                    }
                                }
                        }
                            
                        } catch (IOException e) {
                                result[0] = "fail";
                                result[1] = "error " + e.getMessage();
                                return result;
                        } catch (ClassNotFoundException e) {
                                result[0] = "fail";
                                result[1] = "error " + e.getMessage();
                                return result;
                        } catch (SQLException e) {
                                if(e.getErrorCode() == 1017){
                                        result[0] = "fail";
                                        result[1] = "Usuario o clave no validos";	
                                } 
                                if(e.getErrorCode() == 28001){
                                        result[0] = "fail";
                                        result[1] = "Su clave expiró";	
                                }
                                return result; 
                        } finally {			
                                try {
                                        if(conn != null ){
                                                conn.close();
                                                conn = null;
                                        }
                                } catch (Exception e2) {
                                        e2.printStackTrace();
                                        result[0] = "fail";
                                        result[1] = "error " + e2.getMessage();
                                        return result;
                                }
                        }
                return result;
            }
    
    
	
	public String autUserStr(String user, String pass) throws Exception{
		String[] resp = autUser(user, pass);
		
		return resp[0]+"|"+resp[1];
	}
       
        
        
    
}
