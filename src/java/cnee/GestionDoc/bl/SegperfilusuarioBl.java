

package cnee.GestionDoc.bl;

import cnee.GestionDoc.model.SegperfilusuarioMd; 


public class SegperfilusuarioBl {

	public String Validar(SegperfilusuarioMd SegperfilusuarioblValidar){

		String sMensaje ="";
		String sCr= "\r\n";

		if (SegperfilusuarioblValidar.getId_perfil_usuario()==0)
		{
			sMensaje = sMensaje + "- Id_perfil_usuario no es opcional / nulo " + sCr;
		}

		if (SegperfilusuarioblValidar.getId_perfil()==0)
		{
			sMensaje = sMensaje + "- Id_perfil no es opcional / nulo " + sCr;
		}

		if (SegperfilusuarioblValidar.getId_usuario()==0)
		{
			sMensaje = sMensaje + "- Id_usuario no es opcional / nulo " + sCr;
		}

		if (SegperfilusuarioblValidar.getActivo()==-1)
		{
			sMensaje = sMensaje + "- Activo no es opcional / nulo " + sCr;
		}


		return sMensaje;

	}

} 
