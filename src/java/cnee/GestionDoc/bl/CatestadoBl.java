

package cnee.GestionDoc.bl;

import cnee.GestionDoc.model.CatestadoMd; 


public class CatestadoBl {

	public String Validar(CatestadoMd CatestadoblValidar){

		String sMensaje ="";
		String sCr= "\r\n";

		if (CatestadoblValidar.getId()==0)
		{
			sMensaje = sMensaje + "- Id no es opcional / nulo " + sCr;
		}

		if (CatestadoblValidar.getCoddep().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Coddep no es opcional / nulo " + sCr;
		}

		if (CatestadoblValidar.getEstado().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Estado no es opcional / nulo " + sCr;
		}

		if (CatestadoblValidar.getDescripcion().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Descripcion no es opcional / nulo " + sCr;
		}

		if (CatestadoblValidar.getDias()==0)
		{
			sMensaje = sMensaje + "- Dias no es opcional / nulo " + sCr;
		}

		if (CatestadoblValidar.getHoras().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Horas no es opcional / nulo " + sCr;
		}

		if (CatestadoblValidar.getActivo().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Activo no es opcional / nulo " + sCr;
		}

		if (CatestadoblValidar.getEnviamail().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Enviamail no es opcional / nulo " + sCr;
		}

		if (CatestadoblValidar.getUsuario_ingreso().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Usuario_ingreso no es opcional / nulo " + sCr;
		}

		if (CatestadoblValidar.getFecha_ingreso().isEmpty())
		{
			sMensaje = sMensaje + "- Fecha_ingreso no es opcional / nulo " + sCr;
		}

		if (CatestadoblValidar.getFecha_modificacion().isEmpty())
		{
			sMensaje = sMensaje + "- Fecha_modificacion no es opcional / nulo " + sCr;
		}

		if (CatestadoblValidar.getUsuario_modificacion().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Usuario_modificacion no es opcional / nulo " + sCr;
		}

		return sMensaje;

	}

} 
