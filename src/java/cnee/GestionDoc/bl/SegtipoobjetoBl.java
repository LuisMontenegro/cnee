

package cnee.GestionDoc.bl;

import cnee.GestionDoc.model.SegtipoobjetoMd; 


public class SegtipoobjetoBl {

	public String Validar(SegtipoobjetoMd SegtipoobjetoblValidar){

		String sMensaje ="";
		String sCr= "\r\n";

		if (SegtipoobjetoblValidar.getId_tipo_objeto()==0)
		{
			sMensaje = sMensaje + "- Id_tipo_objeto no es opcional / nulo " + sCr;
		}

		if (SegtipoobjetoblValidar.getNombre().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Nombre no es opcional / nulo " + sCr;
		}

		if (SegtipoobjetoblValidar.getDescripcion().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Descripcion no es opcional / nulo " + sCr;
		}

		
		return sMensaje;

	}

} 
