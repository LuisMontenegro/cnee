

package cnee.GestionDoc.bl;

import cnee.GestionDoc.model.SegperfilobjetoMd; 


public class SegperfilobjetoBl {

	public String Validar(SegperfilobjetoMd SegperfilobjetoblValidar){

		String sMensaje ="";
		String sCr= "\r\n";

		if (SegperfilobjetoblValidar.getId_perfil_objeto()==0)
		{
			sMensaje = sMensaje + "- Id_perfil_objeto no es opcional / nulo " + sCr;
		}

		if (SegperfilobjetoblValidar.getId_perfil()==0)
		{
			sMensaje = sMensaje + "- Id_perfil no es opcional / nulo " + sCr;
		}

		if (SegperfilobjetoblValidar.getId_objeto()==0)
		{
			sMensaje = sMensaje + "- Id_objeto no es opcional / nulo " + sCr;
		}

		if (SegperfilobjetoblValidar.getActivo()==-1)
		{
			sMensaje = sMensaje + "- Activo no es opcional / nulo " + sCr;
		}

		
		return sMensaje;

	}

} 
