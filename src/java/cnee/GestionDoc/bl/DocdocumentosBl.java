

package cnee.GestionDoc.bl;

import cnee.GestionDoc.model.DocdocumentosMd; 


public class DocdocumentosBl {

	public String Validar(DocdocumentosMd DocdocumentosblValidar){

		String sMensaje ="";
		String sCr= "\r\n";

		if (DocdocumentosblValidar.getCodigo()==0)
		{
			sMensaje = sMensaje + "- Codigo no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getCoddependencia().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Coddependencia no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getCodtema().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Codtema no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getCodtipodocto().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Codtipodocto no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getDocnum().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Docnum no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getDocref().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Docref no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getFechaemision().isEmpty())
		{
			sMensaje = sMensaje + "- Fechaemision no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getFecharecep().isEmpty())
		{
			sMensaje = sMensaje + "- Fecharecep no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getHorarecep().isEmpty())
		{
			sMensaje = sMensaje + "- Horarecep no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getDescripcion().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Descripcion no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getOrigpersona().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Origpersona no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getOrigempresa().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Origempresa no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getOrigdependencia().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Origdependencia no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getDestpersona()==0)
		{
			sMensaje = sMensaje + "- Destpersona no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getDestempresa()==0)
		{
			sMensaje = sMensaje + "- Destempresa no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getDestdependencia().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Destdependencia no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getCodestado()==0)
		{
			sMensaje = sMensaje + "- Codestado no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getObservaciones().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Observaciones no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getActivo()==0)
		{
			sMensaje = sMensaje + "- Activo no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getFecha_ingreso().isEmpty())
		{
			sMensaje = sMensaje + "- Fecha_ingreso no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getUsuario_ingreso().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Usuario_ingreso no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getFecha_modificacion().isEmpty())
		{
			sMensaje = sMensaje + "- Fecha_modificacion no es opcional / nulo " + sCr;
		}

		if (DocdocumentosblValidar.getUsuario_modificacion().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Usuario_modificacion no es opcional / nulo " + sCr;
		}

		return sMensaje;

	}

} 
