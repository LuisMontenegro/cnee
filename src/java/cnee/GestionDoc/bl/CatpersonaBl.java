

package cnee.GestionDoc.bl;

import cnee.GestionDoc.model.CatpersonaMd; 


public class CatpersonaBl {

	public String Validar(CatpersonaMd CatpersonablValidar){

		String sMensaje ="";
		String sCr= "\r\n";

		if (CatpersonablValidar.getId()==0)
		{
			sMensaje = sMensaje + "- Id no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getCodigo().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Codigo no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getCodempresa().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Codempresa no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getNombres().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Nombres no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getApellidos().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Apellidos no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getSexo().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Sexo no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getTitulo().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Titulo no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getPuesto().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Puesto no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getDireccion().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Direccion no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getEmail().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Email no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getAreageografica()==0)
		{
			sMensaje = sMensaje + "- Areageografica no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getTelefono().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Telefono no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getTelefonomovil().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Telefonomovil no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getEmercontacto().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Emercontacto no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getEmercontactotel().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Emercontactotel no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getFecha_ingreso().isEmpty())
		{
			sMensaje = sMensaje + "- Fecha_ingreso no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getUsuario_ingreso().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Usuario_ingreso no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getFecha_modificacion().isEmpty())
		{
			sMensaje = sMensaje + "- Fecha_modificacion no es opcional / nulo " + sCr;
		}

		if (CatpersonablValidar.getUsuario_modificacion().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Usuario_modificacion no es opcional / nulo " + sCr;
		}

		return sMensaje;

	}

} 
