

package cnee.GestionDoc.bl;

import cnee.GestionDoc.model.CattemasMd; 


public class CattemasBl {

	public String Validar(CattemasMd CattemasblValidar){

		String sMensaje ="";
		String sCr= "\r\n";

		if (CattemasblValidar.getId()==0)
		{
			sMensaje = sMensaje + "- Id no es opcional / nulo " + sCr;
		}

		if (CattemasblValidar.getDepencod().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Depencod no es opcional / nulo " + sCr;
		}

		if (CattemasblValidar.getTema().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Tema no es opcional / nulo " + sCr;
		}

		if (CattemasblValidar.getTemades().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Temades no es opcional / nulo " + sCr;
		}

		if (CattemasblValidar.getClasif().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Clasif no es opcional / nulo " + sCr;
		}

		if (CattemasblValidar.getActivo().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Activo no es opcional / nulo " + sCr;
		}

		if (CattemasblValidar.getUsuario_ingreso().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Usuario_ingreso no es opcional / nulo " + sCr;
		}

		if (CattemasblValidar.getFecha_ingreso().isEmpty())
		{
			sMensaje = sMensaje + "- Fecha_ingreso no es opcional / nulo " + sCr;
		}

		if (CattemasblValidar.getFecha_modificacion().isEmpty())
		{
			sMensaje = sMensaje + "- Fecha_modificacion no es opcional / nulo " + sCr;
		}

		if (CattemasblValidar.getUsuario_modificacion().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Usuario_modificacion no es opcional / nulo " + sCr;
		}

		return sMensaje;

	}

} 
