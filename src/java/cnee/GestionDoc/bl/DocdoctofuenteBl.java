

package cnee.GestionDoc.bl;

import cnee.GestionDoc.model.DocdoctofuenteMd; 


public class DocdoctofuenteBl {

	public String Validar(DocdoctofuenteMd DocdoctofuenteblValidar){

		String sMensaje ="";
		String sCr= "\r\n";

		if (DocdoctofuenteblValidar.getCodigo()==0)
		{
			sMensaje = sMensaje + "- Codigo no es opcional / nulo " + sCr;
		}

		if (DocdoctofuenteblValidar.getDoctoorigen()==0)
		{
			sMensaje = sMensaje + "- Doctoorigen no es opcional / nulo " + sCr;
		}

		if (DocdoctofuenteblValidar.getFechacreado().isEmpty())
		{
			sMensaje = sMensaje + "- Fechacreado no es opcional / nulo " + sCr;
		}

		if (DocdoctofuenteblValidar.getHoracreado().isEmpty())
		{
			sMensaje = sMensaje + "- Horacreado no es opcional / nulo " + sCr;
		}

		if (DocdoctofuenteblValidar.getUsuario().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Usuario no es opcional / nulo " + sCr;
		}

		if (DocdoctofuenteblValidar.getDocubica().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Docubica no es opcional / nulo " + sCr;
		}

		if (DocdoctofuenteblValidar.getObservaciones().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Observaciones no es opcional / nulo " + sCr;
		}

		if (DocdoctofuenteblValidar.getFormato().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Formato no es opcional / nulo " + sCr;
		}

		return sMensaje;

	}

} 
