

package cnee.GestionDoc.bl;

import cnee.GestionDoc.model.SegusuarioMd; 


public class SegusuarioBl {

	public String Validar(SegusuarioMd SegusuarioblValidar){

		String sMensaje ="";
		String sCr= "\r\n";

		if (SegusuarioblValidar.getId_usuario()==0)
		{
			sMensaje = sMensaje + "- Id_usuario no es opcional / nulo " + sCr;
		}

	
		if (SegusuarioblValidar.getUsuario_login().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Usuario_login no es opcional / nulo " + sCr;
		}

		if (SegusuarioblValidar.getPassword().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Password no es opcional / nulo " + sCr;
		}

		if (SegusuarioblValidar.getNombre().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Nombre no es opcional / nulo " + sCr;
		}

		if (SegusuarioblValidar.getApellido().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Apellido no es opcional / nulo " + sCr;
		}

		if (SegusuarioblValidar.getActivo()==-1)
		{
			sMensaje = sMensaje + "- Activo no es opcional / nulo " + sCr;
		}

		

		return sMensaje;

	}

} 
