

package cnee.GestionDoc.bl;

import cnee.GestionDoc.model.SegobjetoMd; 


public class SegobjetoBl {

	public String Validar(SegobjetoMd SegobjetoblValidar){

		String sMensaje ="";
		String sCr= "\r\n";

		if (SegobjetoblValidar.getId_objeto()==0)
		{
			sMensaje = sMensaje + "- Id_objeto no es opcional / nulo " + sCr;
		}

		if (SegobjetoblValidar.getId_tipo_objeto()==0)
		{
			sMensaje = sMensaje + "- Id_tipo_objeto no es opcional / nulo " + sCr;
		}

		if (SegobjetoblValidar.getNombre().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Nombre no es opcional / nulo " + sCr;
		}

		if (SegobjetoblValidar.getDescripcion().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Descripcion no es opcional / nulo " + sCr;
		}

		if (SegobjetoblValidar.getActivo()==-1)
		{
			sMensaje = sMensaje + "- Activo no es opcional / nulo " + sCr;
		}

		
		if (SegobjetoblValidar.getId_padre()==0)
		{
			sMensaje = sMensaje + "- Id_padre no es opcional / nulo " + sCr;
		}

		if (SegobjetoblValidar.getUrl().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Url no es opcional / nulo " + sCr;
		}

		return sMensaje;

	}

} 
