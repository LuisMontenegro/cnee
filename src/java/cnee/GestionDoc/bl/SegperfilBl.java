

package cnee.GestionDoc.bl;

import cnee.GestionDoc.model.SegperfilMd; 


public class SegperfilBl {

	public String Validar(SegperfilMd SegperfilblValidar){

		String sMensaje ="";
		String sCr= "\r\n";

		if (SegperfilblValidar.getId_perfil()==0)
		{
			sMensaje = sMensaje + "- Id_perfil no es opcional / nulo " + sCr;
		}

		if (SegperfilblValidar.getNombre().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Nombre no es opcional / nulo " + sCr;
		}

		if (SegperfilblValidar.getDescripcion().toString().isEmpty())
		{
			sMensaje = sMensaje + "- Descripcion no es opcional / nulo " + sCr;
		}

		

		return sMensaje;

	}

} 
