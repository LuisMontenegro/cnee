


package cnee.GestionDoc.model;




public class DocpredocumentoMd {

	 private int Codigo;
	 private String Codigoorigen;
	 private String Empresaorigen;
	 private String Personaorigen;
	 private String Destino;
	 private String Temas;
	 private String Fecha_ingreso;
	 private String Hora_ingreso;
	 private String Usuario_ingreso;
	 private String Fecha_modificacion;
	 private String Usuario_modificacion;

	public DocpredocumentoMd( int CodigoL, String CodigoorigenL, String EmpresaorigenL, String PersonaorigenL, 
                                  String DestinoL, String TemasL, String Fecha_ingresoL, String Hora_ingresoL,
                                  String Usuario_ingresoL, String Fecha_modificacionL, String Usuario_modificacionL)	{
		 this.Codigo = CodigoL;
		 this.Codigoorigen = CodigoorigenL;
		 this.Empresaorigen = EmpresaorigenL;
		 this.Personaorigen = PersonaorigenL;
		 this.Destino = DestinoL;
		 this.Temas = TemasL;
		 this.Fecha_ingreso = Fecha_ingresoL;
		 this.Hora_ingreso = Hora_ingresoL;
		 this.Usuario_ingreso = Usuario_ingresoL;
		 this.Fecha_modificacion = Fecha_modificacionL;
		 this.Usuario_modificacion = Usuario_modificacionL;
	}

	 public int getCodigo() {
		 return Codigo;
	}

	 public void setCodigo(int Codigo) {
		 this.Codigo = Codigo;
	}

	 public String getCodigoorigen() {
		 return Codigoorigen;
	}

	 public void setCodigoorigen(String Codigoorigen) {
		 this.Codigoorigen = Codigoorigen;
	}

	 public String getEmpresaorigen() {
		 return Empresaorigen;
	}

	 public void setEmpresaorigen(String Empresaorigen) {
		 this.Empresaorigen = Empresaorigen;
	}

	 public String getPersonaorigen() {
		 return Personaorigen;
	}

	 public void setPersonaorigen(String Personaorigen) {
		 this.Personaorigen = Personaorigen;
	}

	 public String getDestino() {
		 return Destino;
	}

	 public void setDestino(String Destino) {
		 this.Destino = Destino;
	}

	 public String getTemas() {
		 return Temas;
	}

	 public void setTemas(String Temas) {
		 this.Temas = Temas;
	}

	 public String getFecha_ingreso() {
		 return Fecha_ingreso;
	}

	 public void setFecha_ingreso(String Fecha_ingreso) {
		 this.Fecha_ingreso = Fecha_ingreso;
	}

	 public String getHora_ingreso() {
		 return Hora_ingreso;
	}

	 public void setHora_ingreso(String Hora_ingreso) {
		 this.Hora_ingreso = Hora_ingreso;
	}

	 public String getUsuario_ingreso() {
		 return Usuario_ingreso;
	}

	 public void setUsuario_ingreso(String Usuario_ingreso) {
		 this.Usuario_ingreso = Usuario_ingreso;
	}

	 public String getFecha_modificacion() {
		 return Fecha_modificacion;
	}

	 public void setFecha_modificacion(String Fecha_modificacion) {
		 this.Fecha_modificacion = Fecha_modificacion;
	}

	 public String getUsuario_modificacion() {
		 return Usuario_modificacion;
	}

	 public void setUsuario_modificacion(String Usuario_modificacion) {
		 this.Usuario_modificacion = Usuario_modificacion;
	}

}
