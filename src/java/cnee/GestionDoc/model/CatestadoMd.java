


package cnee.GestionDoc.model;




public class CatestadoMd {

	 private int Id;
	 private String Coddep;
	 private String Estado;
	 private String Descripcion;
	 private int Dias;
	 private String Horas;
	 private String Activo;
	 private String Enviamail;
	 private String Usuario_ingreso;
	 private String Fecha_ingreso;
	 private String Fecha_modificacion;
	 private String Usuario_modificacion;

	public CatestadoMd( int IdL, String CoddepL, String EstadoL, String DescripcionL, int DiasL, String HorasL, String ActivoL, String EnviamailL, String Usuario_ingresoL, String Fecha_ingresoL, String Fecha_modificacionL, String Usuario_modificacionL)	{
		 this.Id = IdL;
		 this.Coddep = CoddepL;
		 this.Estado = EstadoL;
		 this.Descripcion = DescripcionL;
		 this.Dias = DiasL;
		 this.Horas = HorasL;
		 this.Activo = ActivoL;
		 this.Enviamail = EnviamailL;
		 this.Usuario_ingreso = Usuario_ingresoL;
		 this.Fecha_ingreso = Fecha_ingresoL;
		 this.Fecha_modificacion = Fecha_modificacionL;
		 this.Usuario_modificacion = Usuario_modificacionL;
	}

	 public int getId() {
		 return Id;
	}

	 public void setId(int Id) {
		 this.Id = Id;
	}

	 public String getCoddep() {
		 return Coddep;
	}

	 public void setCoddep(String Coddep) {
		 this.Coddep = Coddep;
	}

	 public String getEstado() {
		 return Estado;
	}

	 public void setEstado(String Estado) {
		 this.Estado = Estado;
	}

	 public String getDescripcion() {
		 return Descripcion;
	}

	 public void setDescripcion(String Descripcion) {
		 this.Descripcion = Descripcion;
	}

	 public int getDias() {
		 return Dias;
	}

	 public void setDias(int Dias) {
		 this.Dias = Dias;
	}

	 public String getHoras() {
		 return Horas;
	}

	 public void setHoras(String Horas) {
		 this.Horas = Horas;
	}

	 public String getActivo() {
		 return Activo;
	}

	 public void setActivo(String Activo) {
		 this.Activo = Activo;
	}

	 public String getEnviamail() {
		 return Enviamail;
	}

	 public void setEnviamail(String Enviamail) {
		 this.Enviamail = Enviamail;
	}

	 public String getUsuario_ingreso() {
		 return Usuario_ingreso;
	}

	 public void setUsuario_ingreso(String Usuario_ingreso) {
		 this.Usuario_ingreso = Usuario_ingreso;
	}

	 public String getFecha_ingreso() {
		 return Fecha_ingreso;
	}

	 public void setFecha_ingreso(String Fecha_ingreso) {
		 this.Fecha_ingreso = Fecha_ingreso;
	}

	 public String getFecha_modificacion() {
		 return Fecha_modificacion;
	}

	 public void setFecha_modificacion(String Fecha_modificacion) {
		 this.Fecha_modificacion = Fecha_modificacion;
	}

	 public String getUsuario_modificacion() {
		 return Usuario_modificacion;
	}

	 public void setUsuario_modificacion(String Usuario_modificacion) {
		 this.Usuario_modificacion = Usuario_modificacion;
	}

}
