


package cnee.GestionDoc.model;




public class SegperfilMd {

	 private int Id_perfil;
	 private String Nombre;
	 private String Descripcion;
	 private int Estatus;
	 private String Fecha_ingreso;
	 private String Usuario_ingreso;
	 private String Fecha_modificacion;
	 private String Usuario_modificacion;

	public SegperfilMd( int Id_perfilL, String NombreL, String DescripcionL, int EstatusL, String Fecha_ingresoL, String Usuario_ingresoL, String Fecha_modificacionL, String Usuario_modificacionL)	{
		 this.Id_perfil = Id_perfilL;
		 this.Nombre = NombreL;
		 this.Descripcion = DescripcionL;
		 this.Estatus = EstatusL;
		 this.Fecha_ingreso = Fecha_ingresoL;
		 this.Usuario_ingreso = Usuario_ingresoL;
		 this.Fecha_modificacion = Fecha_modificacionL;
		 this.Usuario_modificacion = Usuario_modificacionL;
	}

	 public int getId_perfil() {
		 return Id_perfil;
	}

	 public void setId_perfil(int Id_perfil) {
		 this.Id_perfil = Id_perfil;
	}

	 public String getNombre() {
		 return Nombre;
	}

	 public void setNombre(String Nombre) {
		 this.Nombre = Nombre;
	}

	 public String getDescripcion() {
		 return Descripcion;
	}

	 public void setDescripcion(String Descripcion) {
		 this.Descripcion = Descripcion;
	}

	 public int getEstatus() {
		 return Estatus;
	}

	 public void setEstatus(int Estatus) {
		 this.Estatus = Estatus;
	}

	 public String getFecha_ingreso() {
		 return Fecha_ingreso;
	}

	 public void setFecha_ingreso(String Fecha_ingreso) {
		 this.Fecha_ingreso = Fecha_ingreso;
	}

	 public String getUsuario_ingreso() {
		 return Usuario_ingreso;
	}

	 public void setUsuario_ingreso(String Usuario_ingreso) {
		 this.Usuario_ingreso = Usuario_ingreso;
	}

	 public String getFecha_modificacion() {
		 return Fecha_modificacion;
	}

	 public void setFecha_modificacion(String Fecha_modificacion) {
		 this.Fecha_modificacion = Fecha_modificacion;
	}

	 public String getUsuario_modificacion() {
		 return Usuario_modificacion;
	}

	 public void setUsuario_modificacion(String Usuario_modificacion) {
		 this.Usuario_modificacion = Usuario_modificacion;
	}

}
