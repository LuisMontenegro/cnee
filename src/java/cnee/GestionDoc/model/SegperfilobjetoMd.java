


package cnee.GestionDoc.model;




public class SegperfilobjetoMd {

	 private int Id_perfil_objeto;
	 private int Id_perfil;
	 private int Id_objeto;
	 private int Activo;
	 private String Fecha_ingreso;
	 private String Usuario_ingreso;
	 private String Fecha_modificacion;
	 private String Usuario_modificacion;

	public SegperfilobjetoMd( int Id_perfil_objetoL, int Id_perfilL, int Id_objetoL, int ActivoL, String Fecha_ingresoL, String Usuario_ingresoL, String Fecha_modificacionL, String Usuario_modificacionL)	{
		 this.Id_perfil_objeto = Id_perfil_objetoL;
		 this.Id_perfil = Id_perfilL;
		 this.Id_objeto = Id_objetoL;
		 this.Activo = ActivoL;
		 this.Fecha_ingreso = Fecha_ingresoL;
		 this.Usuario_ingreso = Usuario_ingresoL;
		 this.Fecha_modificacion = Fecha_modificacionL;
		 this.Usuario_modificacion = Usuario_modificacionL;
	}

	 public int getId_perfil_objeto() {
		 return Id_perfil_objeto;
	}

	 public void setId_perfil_objeto(int Id_perfil_objeto) {
		 this.Id_perfil_objeto = Id_perfil_objeto;
	}

	 public int getId_perfil() {
		 return Id_perfil;
	}

	 public void setId_perfil(int Id_perfil) {
		 this.Id_perfil = Id_perfil;
	}

	 public int getId_objeto() {
		 return Id_objeto;
	}

	 public void setId_objeto(int Id_objeto) {
		 this.Id_objeto = Id_objeto;
	}

	 public int getActivo() {
		 return Activo;
	}

	 public void setActivo(int Activo) {
		 this.Activo = Activo;
	}

	 public String getFecha_ingreso() {
		 return Fecha_ingreso;
	}

	 public void setFecha_ingreso(String Fecha_ingreso) {
		 this.Fecha_ingreso = Fecha_ingreso;
	}

	 public String getUsuario_ingreso() {
		 return Usuario_ingreso;
	}

	 public void setUsuario_ingreso(String Usuario_ingreso) {
		 this.Usuario_ingreso = Usuario_ingreso;
	}

	 public String getFecha_modificacion() {
		 return Fecha_modificacion;
	}

	 public void setFecha_modificacion(String Fecha_modificacion) {
		 this.Fecha_modificacion = Fecha_modificacion;
	}

	 public String getUsuario_modificacion() {
		 return Usuario_modificacion;
	}

	 public void setUsuario_modificacion(String Usuario_modificacion) {
		 this.Usuario_modificacion = Usuario_modificacion;
	}

}
