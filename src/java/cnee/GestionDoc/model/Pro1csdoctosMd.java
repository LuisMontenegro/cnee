


package cnee.GestionDoc.model;




public class Pro1csdoctosMd {

	 private String Tiporeg;
	 private String Depencod;
	 private String Doccod;
	 private String Docnum;
	 private String Docref;
	 private String Fechaemision;
	 private String Fecharecep;
	 private String Horarecep;
	 private String Tema;
	 private String Descripcion;
	 private String Origpersona;
	 private String Origempresa;
	 private String Origdependencia;
	 private String Destpersona;
	 private String Destempresa;
	 private String Destdependencia;
	 private String Estado;
	 private String Observaciones;
	 private String Docusovis;
	 private String Lastdepencod;
	 private String Lastpersona;
	 private String Lastfecha;
	 private String Lasthora;
	 private String Docfojas;
	 private int Monto;
	 private String Usuariosaccesos;

	public Pro1csdoctosMd( String TiporegL, String DepencodL, String DoccodL, String DocnumL, String DocrefL, String FechaemisionL, String FecharecepL, String HorarecepL, String TemaL, String DescripcionL, String OrigpersonaL, String OrigempresaL, String OrigdependenciaL, String DestpersonaL, String DestempresaL, String DestdependenciaL, String EstadoL, String ObservacionesL, String DocusovisL, String LastdepencodL, String LastpersonaL, String LastfechaL, String LasthoraL, String DocfojasL, int MontoL, String UsuariosaccesosL)	{
		 this.Tiporeg = TiporegL;
		 this.Depencod = DepencodL;
		 this.Doccod = DoccodL;
		 this.Docnum = DocnumL;
		 this.Docref = DocrefL;
		 this.Fechaemision = FechaemisionL;
		 this.Fecharecep = FecharecepL;
		 this.Horarecep = HorarecepL;
		 this.Tema = TemaL;
		 this.Descripcion = DescripcionL;
		 this.Origpersona = OrigpersonaL;
		 this.Origempresa = OrigempresaL;
		 this.Origdependencia = OrigdependenciaL;
		 this.Destpersona = DestpersonaL;
		 this.Destempresa = DestempresaL;
		 this.Destdependencia = DestdependenciaL;
		 this.Estado = EstadoL;
		 this.Observaciones = ObservacionesL;
		 this.Docusovis = DocusovisL;
		 this.Lastdepencod = LastdepencodL;
		 this.Lastpersona = LastpersonaL;
		 this.Lastfecha = LastfechaL;
		 this.Lasthora = LasthoraL;
		 this.Docfojas = DocfojasL;
		 this.Monto = MontoL;
		 this.Usuariosaccesos = UsuariosaccesosL;
	}

	 public String getTiporeg() {
		 return Tiporeg;
	}

	 public void setTiporeg(String Tiporeg) {
		 this.Tiporeg = Tiporeg;
	}

	 public String getDepencod() {
		 return Depencod;
	}

	 public void setDepencod(String Depencod) {
		 this.Depencod = Depencod;
	}

	 public String getDoccod() {
		 return Doccod;
	}

	 public void setDoccod(String Doccod) {
		 this.Doccod = Doccod;
	}

	 public String getDocnum() {
		 return Docnum;
	}

	 public void setDocnum(String Docnum) {
		 this.Docnum = Docnum;
	}

	 public String getDocref() {
		 return Docref;
	}

	 public void setDocref(String Docref) {
		 this.Docref = Docref;
	}

	 public String getFechaemision() {
		 return Fechaemision;
	}

	 public void setFechaemision(String Fechaemision) {
		 this.Fechaemision = Fechaemision;
	}

	 public String getFecharecep() {
		 return Fecharecep;
	}

	 public void setFecharecep(String Fecharecep) {
		 this.Fecharecep = Fecharecep;
	}

	 public String getHorarecep() {
		 return Horarecep;
	}

	 public void setHorarecep(String Horarecep) {
		 this.Horarecep = Horarecep;
	}

	 public String getTema() {
		 return Tema;
	}

	 public void setTema(String Tema) {
		 this.Tema = Tema;
	}

	 public String getDescripcion() {
		 return Descripcion;
	}

	 public void setDescripcion(String Descripcion) {
		 this.Descripcion = Descripcion;
	}

	 public String getOrigpersona() {
		 return Origpersona;
	}

	 public void setOrigpersona(String Origpersona) {
		 this.Origpersona = Origpersona;
	}

	 public String getOrigempresa() {
		 return Origempresa;
	}

	 public void setOrigempresa(String Origempresa) {
		 this.Origempresa = Origempresa;
	}

	 public String getOrigdependencia() {
		 return Origdependencia;
	}

	 public void setOrigdependencia(String Origdependencia) {
		 this.Origdependencia = Origdependencia;
	}

	 public String getDestpersona() {
		 return Destpersona;
	}

	 public void setDestpersona(String Destpersona) {
		 this.Destpersona = Destpersona;
	}

	 public String getDestempresa() {
		 return Destempresa;
	}

	 public void setDestempresa(String Destempresa) {
		 this.Destempresa = Destempresa;
	}

	 public String getDestdependencia() {
		 return Destdependencia;
	}

	 public void setDestdependencia(String Destdependencia) {
		 this.Destdependencia = Destdependencia;
	}

	 public String getEstado() {
		 return Estado;
	}

	 public void setEstado(String Estado) {
		 this.Estado = Estado;
	}

	 public String getObservaciones() {
		 return Observaciones;
	}

	 public void setObservaciones(String Observaciones) {
		 this.Observaciones = Observaciones;
	}

	 public String getDocusovis() {
		 return Docusovis;
	}

	 public void setDocusovis(String Docusovis) {
		 this.Docusovis = Docusovis;
	}

	 public String getLastdepencod() {
		 return Lastdepencod;
	}

	 public void setLastdepencod(String Lastdepencod) {
		 this.Lastdepencod = Lastdepencod;
	}

	 public String getLastpersona() {
		 return Lastpersona;
	}

	 public void setLastpersona(String Lastpersona) {
		 this.Lastpersona = Lastpersona;
	}

	 public String getLastfecha() {
		 return Lastfecha;
	}

	 public void setLastfecha(String Lastfecha) {
		 this.Lastfecha = Lastfecha;
	}

	 public String getLasthora() {
		 return Lasthora;
	}

	 public void setLasthora(String Lasthora) {
		 this.Lasthora = Lasthora;
	}

	 public String getDocfojas() {
		 return Docfojas;
	}

	 public void setDocfojas(String Docfojas) {
		 this.Docfojas = Docfojas;
	}

	 public int getMonto() {
		 return Monto;
	}

	 public void setMonto(int Monto) {
		 this.Monto = Monto;
	}

	 public String getUsuariosaccesos() {
		 return Usuariosaccesos;
	}

	 public void setUsuariosaccesos(String Usuariosaccesos) {
		 this.Usuariosaccesos = Usuariosaccesos;
	}

}
