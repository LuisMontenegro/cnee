


package cnee.GestionDoc.model;




public class SegusuarioMd {

	 private int Id_usuario;
	 private String Coddep;
	 private String Usuario_login;
	 private String Password;
	 private String Nombre;
	 private String Apellido;
	 private int Activo;
	 private String Fecha_ingreso;
	 private String Usuario_ingreso;
	 private String Fecha_modificacion;
	 private String Usuario_modificacion;

         
	public SegusuarioMd( int Id_usuarioL, 
                String CoddepL, 
                String Usuario_loginL, 
                String PasswordL, 
                String NombreL, 
                String ApellidoL, 
                int ActivoL, 
                String Fecha_ingresoL, 
                String Usuario_ingresoL, 
                String Fecha_modificacionL,
                String Usuario_modificacionL)	
        {
		 this.Id_usuario = Id_usuarioL;
		 this.Coddep = CoddepL;
		 this.Usuario_login = Usuario_loginL;
		 this.Password = PasswordL;
		 this.Nombre = NombreL;
		 this.Apellido = ApellidoL;
		 this.Activo = ActivoL;
		 this.Fecha_ingreso = Fecha_ingresoL;
		 this.Usuario_ingreso = Usuario_ingresoL;
		 this.Fecha_modificacion = Fecha_modificacionL;
		 this.Usuario_modificacion = Usuario_modificacionL;
	}

	 public int getId_usuario() {
		 return Id_usuario;
	}

	 public void setId_usuario(int Id_usuario) {
		 this.Id_usuario = Id_usuario;
	}

	 public String getCoddep() {
		 return Coddep;
	}

	 public void setCoddep(String Coddep) {
		 this.Coddep = Coddep;
	}

	 public String getUsuario_login() {
		 return Usuario_login;
	}

	 public void setUsuario_login(String Usuario_login) {
		 this.Usuario_login = Usuario_login;
	}

	 public String getPassword() {
		 return Password;
	}

	 public void setPassword(String Password) {
		 this.Password = Password;
	}

	 public String getNombre() {
		 return Nombre;
	}

	 public void setNombre(String Nombre) {
		 this.Nombre = Nombre;
	}

	 public String getApellido() {
		 return Apellido;
	}

	 public void setApellido(String Apellido) {
		 this.Apellido = Apellido;
	}

	 public int getActivo() {
		 return Activo;
	}

	 public void setActivo(int Activo) {
		 this.Activo = Activo;
	}

	 public String getFecha_ingreso() {
		 return Fecha_ingreso;
	}

	 public void setFecha_ingreso(String Fecha_ingreso) {
		 this.Fecha_ingreso = Fecha_ingreso;
	}

	 public String getUsuario_ingreso() {
		 return Usuario_ingreso;
	}

	 public void setUsuario_ingreso(String Usuario_ingreso) {
		 this.Usuario_ingreso = Usuario_ingreso;
	}

	 public String getFecha_modificacion() {
		 return Fecha_modificacion;
	}

	 public void setFecha_modificacion(String Fecha_modificacion) {
		 this.Fecha_modificacion = Fecha_modificacion;
	}

	 public String getUsuario_modificacion() {
		 return Usuario_modificacion;
	}

	 public void setUsuario_modificacion(String Usuario_modificacion) {
		 this.Usuario_modificacion = Usuario_modificacion;
	}

}
