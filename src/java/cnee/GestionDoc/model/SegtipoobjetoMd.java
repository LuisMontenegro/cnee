


package cnee.GestionDoc.model;




public class SegtipoobjetoMd {

	 private int Id_tipo_objeto;
	 private String Nombre;
	 private String Descripcion;
	 private int Activo;
	 private String Fecha_ingreso;
	 private String Usuario_ingreso;
	 private String Fecha_modificacion;
	 private String Usuario_modificacion;

	public SegtipoobjetoMd( int Id_tipo_objetoL, String NombreL, String DescripcionL, int ActivoL, String Fecha_ingresoL, String Usuario_ingresoL, String Fecha_modificacionL, String Usuario_modificacionL)	{
		 this.Id_tipo_objeto = Id_tipo_objetoL;
		 this.Nombre = NombreL;
		 this.Descripcion = DescripcionL;
		 this.Activo = ActivoL;
		 this.Fecha_ingreso = Fecha_ingresoL;
		 this.Usuario_ingreso = Usuario_ingresoL;
		 this.Fecha_modificacion = Fecha_modificacionL;
		 this.Usuario_modificacion = Usuario_modificacionL;
	}

	 public int getId_tipo_objeto() {
		 return Id_tipo_objeto;
	}

	 public void setId_tipo_objeto(int Id_tipo_objeto) {
		 this.Id_tipo_objeto = Id_tipo_objeto;
	}

	 public String getNombre() {
		 return Nombre;
	}

	 public void setNombre(String Nombre) {
		 this.Nombre = Nombre;
	}

	 public String getDescripcion() {
		 return Descripcion;
	}

	 public void setDescripcion(String Descripcion) {
		 this.Descripcion = Descripcion;
	}

	 public int getActivo() {
		 return Activo;
	}

	 public void setActivo(int Activo) {
		 this.Activo = Activo;
	}

	 public String getFecha_ingreso() {
		 return Fecha_ingreso;
	}

	 public void setFecha_ingreso(String Fecha_ingreso) {
		 this.Fecha_ingreso = Fecha_ingreso;
	}

	 public String getUsuario_ingreso() {
		 return Usuario_ingreso;
	}

	 public void setUsuario_ingreso(String Usuario_ingreso) {
		 this.Usuario_ingreso = Usuario_ingreso;
	}

	 public String getFecha_modificacion() {
		 return Fecha_modificacion;
	}

	 public void setFecha_modificacion(String Fecha_modificacion) {
		 this.Fecha_modificacion = Fecha_modificacion;
	}

	 public String getUsuario_modificacion() {
		 return Usuario_modificacion;
	}

	 public void setUsuario_modificacion(String Usuario_modificacion) {
		 this.Usuario_modificacion = Usuario_modificacion;
	}

}
