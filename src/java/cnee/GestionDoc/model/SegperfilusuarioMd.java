


package cnee.GestionDoc.model;




public class SegperfilusuarioMd {

	 private int Id_perfil_usuario;
	 private int Id_perfil;
	 private int Id_usuario;
	 private int Activo;
	 private String Fecha_ingreso;
	 private String Usuario_ingreso;
	 private String Fecha_modificacion;
	 private String Usuario_modificacion;

	public SegperfilusuarioMd( int Id_perfil_usuarioL, int Id_perfilL, int Id_usuarioL, int ActivoL, String Fecha_ingresoL, String Usuario_ingresoL, String Fecha_modificacionL, String Usuario_modificacionL)	{
		 this.Id_perfil_usuario = Id_perfil_usuarioL;
		 this.Id_perfil = Id_perfilL;
		 this.Id_usuario = Id_usuarioL;
		 this.Activo = ActivoL;
		 this.Fecha_ingreso = Fecha_ingresoL;
		 this.Usuario_ingreso = Usuario_ingresoL;
		 this.Fecha_modificacion = Fecha_modificacionL;
		 this.Usuario_modificacion = Usuario_modificacionL;
	}

	 public int getId_perfil_usuario() {
		 return Id_perfil_usuario;
	}

	 public void setId_perfil_usuario(int Id_perfil_usuario) {
		 this.Id_perfil_usuario = Id_perfil_usuario;
	}

	 public int getId_perfil() {
		 return Id_perfil;
	}

	 public void setId_perfil(int Id_perfil) {
		 this.Id_perfil = Id_perfil;
	}

	 public int getId_usuario() {
		 return Id_usuario;
	}

	 public void setId_usuario(int Id_usuario) {
		 this.Id_usuario = Id_usuario;
	}

	 public int getActivo() {
		 return Activo;
	}

	 public void setActivo(int Activo) {
		 this.Activo = Activo;
	}

	 public String getFecha_ingreso() {
		 return Fecha_ingreso;
	}

	 public void setFecha_ingreso(String Fecha_ingreso) {
		 this.Fecha_ingreso = Fecha_ingreso;
	}

	 public String getUsuario_ingreso() {
		 return Usuario_ingreso;
	}

	 public void setUsuario_ingreso(String Usuario_ingreso) {
		 this.Usuario_ingreso = Usuario_ingreso;
	}

	 public String getFecha_modificacion() {
		 return Fecha_modificacion;
	}

	 public void setFecha_modificacion(String Fecha_modificacion) {
		 this.Fecha_modificacion = Fecha_modificacion;
	}

	 public String getUsuario_modificacion() {
		 return Usuario_modificacion;
	}

	 public void setUsuario_modificacion(String Usuario_modificacion) {
		 this.Usuario_modificacion = Usuario_modificacion;
	}

}
