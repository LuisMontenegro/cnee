


package cnee.GestionDoc.model;




public class CattemasMd {

	 private int Id;
	 private String Depencod;
	 private String Tema;
	 private String Temades;
	 private String Clasif;
	 private String Activo;
	 private String Usuario_ingreso;
	 private String Fecha_ingreso;
	 private String Fecha_modificacion;
	 private String Usuario_modificacion;

	public CattemasMd( int IdL, String DepencodL, String TemaL, String TemadesL, String ClasifL, String ActivoL, String Usuario_ingresoL, String Fecha_ingresoL, String Fecha_modificacionL, String Usuario_modificacionL)	{
		 this.Id = IdL;
		 this.Depencod = DepencodL;
		 this.Tema = TemaL;
		 this.Temades = TemadesL;
		 this.Clasif = ClasifL;
		 this.Activo = ActivoL;
		 this.Usuario_ingreso = Usuario_ingresoL;
		 this.Fecha_ingreso = Fecha_ingresoL;
		 this.Fecha_modificacion = Fecha_modificacionL;
		 this.Usuario_modificacion = Usuario_modificacionL;
	}

	 public int getId() {
		 return Id;
	}

	 public void setId(int Id) {
		 this.Id = Id;
	}

	 public String getDepencod() {
		 return Depencod;
	}

	 public void setDepencod(String Depencod) {
		 this.Depencod = Depencod;
	}

	 public String getTema() {
		 return Tema;
	}

	 public void setTema(String Tema) {
		 this.Tema = Tema;
	}

	 public String getTemades() {
		 return Temades;
	}

	 public void setTemades(String Temades) {
		 this.Temades = Temades;
	}

	 public String getClasif() {
		 return Clasif;
	}

	 public void setClasif(String Clasif) {
		 this.Clasif = Clasif;
	}

	 public String getActivo() {
		 return Activo;
	}

	 public void setActivo(String Activo) {
		 this.Activo = Activo;
	}

	 public String getUsuario_ingreso() {
		 return Usuario_ingreso;
	}

	 public void setUsuario_ingreso(String Usuario_ingreso) {
		 this.Usuario_ingreso = Usuario_ingreso;
	}

	 public String getFecha_ingreso() {
		 return Fecha_ingreso;
	}

	 public void setFecha_ingreso(String Fecha_ingreso) {
		 this.Fecha_ingreso = Fecha_ingreso;
	}

	 public String getFecha_modificacion() {
		 return Fecha_modificacion;
	}

	 public void setFecha_modificacion(String Fecha_modificacion) {
		 this.Fecha_modificacion = Fecha_modificacion;
	}

	 public String getUsuario_modificacion() {
		 return Usuario_modificacion;
	}

	 public void setUsuario_modificacion(String Usuario_modificacion) {
		 this.Usuario_modificacion = Usuario_modificacion;
	}

}
