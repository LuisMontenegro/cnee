


package cnee.GestionDoc.model;




public class CatempresaMd {

	 private String Codigo;
	 private String Nombre;
	 private String Direccion;
	 private String Telefono;
	 private String Fax;
	 private String Email;
	 private String Web;
	 private String Areageo;
	 private String Codcategoria;
	 private int Activo;
	 private String Fecha_ingreso;
	 private String Usuario_ingreso;
	 private String Fecha_modificacion;
	 private String Usuario_modificacion;

	public CatempresaMd( String CodigoL, 
                            String NombreL, String DireccionL, String TelefonoL, String FaxL, String EmailL, String WebL, String AreageoL, String CodcategoriaL, int ActivoL, String Fecha_ingresoL, String Usuario_ingresoL, String Fecha_modificacionL, String Usuario_modificacionL)	{
		 this.Codigo = CodigoL;
		 this.Nombre = NombreL;
		 this.Direccion = DireccionL;
		 this.Telefono = TelefonoL;
		 this.Fax = FaxL;
		 this.Email = EmailL;
		 this.Web = WebL;
		 this.Areageo = AreageoL;
		 this.Codcategoria = CodcategoriaL;
		 this.Activo = ActivoL;
		 this.Fecha_ingreso = Fecha_ingresoL;
		 this.Usuario_ingreso = Usuario_ingresoL;
		 this.Fecha_modificacion = Fecha_modificacionL;
		 this.Usuario_modificacion = Usuario_modificacionL;
	}

	 public String getCodigo() {
		 return Codigo;
	}

	 public void setCodigo(String Codigo) {
		 this.Codigo = Codigo;
	}

	 public String getNombre() {
		 return Nombre;
	}

	 public void setNombre(String Nombre) {
		 this.Nombre = Nombre;
	}

	 public String getDireccion() {
		 return Direccion;
	}

	 public void setDireccion(String Direccion) {
		 this.Direccion = Direccion;
	}

	 public String getTelefono() {
		 return Telefono;
	}

	 public void setTelefono(String Telefono) {
		 this.Telefono = Telefono;
	}

	 public String getFax() {
		 return Fax;
	}

	 public void setFax(String Fax) {
		 this.Fax = Fax;
	}

	 public String getEmail() {
		 return Email;
	}

	 public void setEmail(String Email) {
		 this.Email = Email;
	}

	 public String getWeb() {
		 return Web;
	}

	 public void setWeb(String Web) {
		 this.Web = Web;
	}

	 public String getAreageo() {
		 return Areageo;
	}

	 public void setAreageo(String Areageo) {
		 this.Areageo = Areageo;
	}

	 public String getCodcategoria() {
		 return Codcategoria;
	}

	 public void setCodcategoria(String Codcategoria) {
		 this.Codcategoria = Codcategoria;
	}

	 public int getActivo() {
		 return Activo;
	}

	 public void setActivo(int Activo) {
		 this.Activo = Activo;
	}

	 public String getFecha_ingreso() {
		 return Fecha_ingreso;
	}

	 public void setFecha_ingreso(String Fecha_ingreso) {
		 this.Fecha_ingreso = Fecha_ingreso;
	}

	 public String getUsuario_ingreso() {
		 return Usuario_ingreso;
	}

	 public void setUsuario_ingreso(String Usuario_ingreso) {
		 this.Usuario_ingreso = Usuario_ingreso;
	}

	 public String getFecha_modificacion() {
		 return Fecha_modificacion;
	}

	 public void setFecha_modificacion(String Fecha_modificacion) {
		 this.Fecha_modificacion = Fecha_modificacion;
	}

	 public String getUsuario_modificacion() {
		 return Usuario_modificacion;
	}

	 public void setUsuario_modificacion(String Usuario_modificacion) {
		 this.Usuario_modificacion = Usuario_modificacion;
	}

}
