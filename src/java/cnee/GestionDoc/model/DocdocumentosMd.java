


package cnee.GestionDoc.model;




public class DocdocumentosMd {

	 private int Codigo;
	 private String Coddependencia;
	 private String Codtema;
	 private String Codtipodocto;
	 private String Docnum;
	 private String Docref;
	 private String Fechaemision;
	 private String Fecharecep;
	 private String Horarecep;
	 private String Descripcion;
	 private String Origpersona;
	 private String Origempresa;
	 private String Origdependencia;
	 private int Destpersona;
	 private int Destempresa;
	 private String Destdependencia;
	 private int Codestado;
	 private String Observaciones;
	 private int Activo;
	 private String Fecha_ingreso;
	 private String Usuario_ingreso;
	 private String Fecha_modificacion;
	 private String Usuario_modificacion;

	public DocdocumentosMd( int CodigoL, String CoddependenciaL, String CodtemaL, String CodtipodoctoL, String DocnumL, String DocrefL, String FechaemisionL, String FecharecepL, String HorarecepL, String DescripcionL, String OrigpersonaL, String OrigempresaL, String OrigdependenciaL, int DestpersonaL, int DestempresaL, String DestdependenciaL, int CodestadoL, String ObservacionesL, int ActivoL, String Fecha_ingresoL, String Usuario_ingresoL, String Fecha_modificacionL, String Usuario_modificacionL)	{
		 this.Codigo = CodigoL;
		 this.Coddependencia = CoddependenciaL;
		 this.Codtema = CodtemaL;
		 this.Codtipodocto = CodtipodoctoL;
		 this.Docnum = DocnumL;
		 this.Docref = DocrefL;
		 this.Fechaemision = FechaemisionL;
		 this.Fecharecep = FecharecepL;
		 this.Horarecep = HorarecepL;
		 this.Descripcion = DescripcionL;
		 this.Origpersona = OrigpersonaL;
		 this.Origempresa = OrigempresaL;
		 this.Origdependencia = OrigdependenciaL;
		 this.Destpersona = DestpersonaL;
		 this.Destempresa = DestempresaL;
		 this.Destdependencia = DestdependenciaL;
		 this.Codestado = CodestadoL;
		 this.Observaciones = ObservacionesL;
		 this.Activo = ActivoL;
		 this.Fecha_ingreso = Fecha_ingresoL;
		 this.Usuario_ingreso = Usuario_ingresoL;
		 this.Fecha_modificacion = Fecha_modificacionL;
		 this.Usuario_modificacion = Usuario_modificacionL;
	}

	 public int getCodigo() {
		 return Codigo;
	}

	 public void setCodigo(int Codigo) {
		 this.Codigo = Codigo;
	}

	 public String getCoddependencia() {
		 return Coddependencia;
	}

	 public void setCoddependencia(String Coddependencia) {
		 this.Coddependencia = Coddependencia;
	}

	 public String getCodtema() {
		 return Codtema;
	}

	 public void setCodtema(String Codtema) {
		 this.Codtema = Codtema;
	}

	 public String getCodtipodocto() {
		 return Codtipodocto;
	}

	 public void setCodtipodocto(String Codtipodocto) {
		 this.Codtipodocto = Codtipodocto;
	}

	 public String getDocnum() {
		 return Docnum;
	}

	 public void setDocnum(String Docnum) {
		 this.Docnum = Docnum;
	}

	 public String getDocref() {
		 return Docref;
	}

	 public void setDocref(String Docref) {
		 this.Docref = Docref;
	}

	 public String getFechaemision() {
		 return Fechaemision;
	}

	 public void setFechaemision(String Fechaemision) {
		 this.Fechaemision = Fechaemision;
	}

	 public String getFecharecep() {
		 return Fecharecep;
	}

	 public void setFecharecep(String Fecharecep) {
		 this.Fecharecep = Fecharecep;
	}

	 public String getHorarecep() {
		 return Horarecep;
	}

	 public void setHorarecep(String Horarecep) {
		 this.Horarecep = Horarecep;
	}

	 public String getDescripcion() {
		 return Descripcion;
	}

	 public void setDescripcion(String Descripcion) {
		 this.Descripcion = Descripcion;
	}

	 public String getOrigpersona() {
		 return Origpersona;
	}

	 public void setOrigpersona(String Origpersona) {
		 this.Origpersona = Origpersona;
	}

	 public String getOrigempresa() {
		 return Origempresa;
	}

	 public void setOrigempresa(String Origempresa) {
		 this.Origempresa = Origempresa;
	}

	 public String getOrigdependencia() {
		 return Origdependencia;
	}

	 public void setOrigdependencia(String Origdependencia) {
		 this.Origdependencia = Origdependencia;
	}

	 public int getDestpersona() {
		 return Destpersona;
	}

	 public void setDestpersona(int Destpersona) {
		 this.Destpersona = Destpersona;
	}

	 public int getDestempresa() {
		 return Destempresa;
	}

	 public void setDestempresa(int Destempresa) {
		 this.Destempresa = Destempresa;
	}

	 public String getDestdependencia() {
		 return Destdependencia;
	}

	 public void setDestdependencia(String Destdependencia) {
		 this.Destdependencia = Destdependencia;
	}

	 public int getCodestado() {
		 return Codestado;
	}

	 public void setCodestado(int Codestado) {
		 this.Codestado = Codestado;
	}

	 public String getObservaciones() {
		 return Observaciones;
	}

	 public void setObservaciones(String Observaciones) {
		 this.Observaciones = Observaciones;
	}

	 public int getActivo() {
		 return Activo;
	}

	 public void setActivo(int Activo) {
		 this.Activo = Activo;
	}

	 public String getFecha_ingreso() {
		 return Fecha_ingreso;
	}

	 public void setFecha_ingreso(String Fecha_ingreso) {
		 this.Fecha_ingreso = Fecha_ingreso;
	}

	 public String getUsuario_ingreso() {
		 return Usuario_ingreso;
	}

	 public void setUsuario_ingreso(String Usuario_ingreso) {
		 this.Usuario_ingreso = Usuario_ingreso;
	}

	 public String getFecha_modificacion() {
		 return Fecha_modificacion;
	}

	 public void setFecha_modificacion(String Fecha_modificacion) {
		 this.Fecha_modificacion = Fecha_modificacion;
	}

	 public String getUsuario_modificacion() {
		 return Usuario_modificacion;
	}

	 public void setUsuario_modificacion(String Usuario_modificacion) {
		 this.Usuario_modificacion = Usuario_modificacion;
	}

}
