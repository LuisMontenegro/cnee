


package cnee.GestionDoc.model;




public class DocdoctofuenteMd {

	 private int Codigo;
	 private int Doctoorigen;
	 private String Fechacreado;
	 private String Horacreado;
	 private String Usuario;
	 private String Docubica;
	 private String Observaciones;
	 private String Formato;

	public DocdoctofuenteMd( int CodigoL, int DoctoorigenL, String FechacreadoL, String HoracreadoL, String UsuarioL, String DocubicaL, String ObservacionesL, String FormatoL)	{
		 this.Codigo = CodigoL;
		 this.Doctoorigen = DoctoorigenL;
		 this.Fechacreado = FechacreadoL;
		 this.Horacreado = HoracreadoL;
		 this.Usuario = UsuarioL;
		 this.Docubica = DocubicaL;
		 this.Observaciones = ObservacionesL;
		 this.Formato = FormatoL;
	}

	 public int getCodigo() {
		 return Codigo;
	}

	 public void setCodigo(int Codigo) {
		 this.Codigo = Codigo;
	}

	 public int getDoctoorigen() {
		 return Doctoorigen;
	}

	 public void setDoctoorigen(int Doctoorigen) {
		 this.Doctoorigen = Doctoorigen;
	}

	 public String getFechacreado() {
		 return Fechacreado;
	}

	 public void setFechacreado(String Fechacreado) {
		 this.Fechacreado = Fechacreado;
	}

	 public String getHoracreado() {
		 return Horacreado;
	}

	 public void setHoracreado(String Horacreado) {
		 this.Horacreado = Horacreado;
	}

	 public String getUsuario() {
		 return Usuario;
	}

	 public void setUsuario(String Usuario) {
		 this.Usuario = Usuario;
	}

	 public String getDocubica() {
		 return Docubica;
	}

	 public void setDocubica(String Docubica) {
		 this.Docubica = Docubica;
	}

	 public String getObservaciones() {
		 return Observaciones;
	}

	 public void setObservaciones(String Observaciones) {
		 this.Observaciones = Observaciones;
	}

	 public String getFormato() {
		 return Formato;
	}

	 public void setFormato(String Formato) {
		 this.Formato = Formato;
	}

}
