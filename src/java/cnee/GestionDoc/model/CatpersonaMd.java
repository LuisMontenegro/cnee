


package cnee.GestionDoc.model;




public class CatpersonaMd {

	 private int Id;
	 private String Codigo;
	 private String Codempresa;
	 private String Nombres;
	 private String Apellidos;
	 private String Sexo;
	 private String Titulo;
	 private String Puesto;
	 private String Direccion;
	 private String Email;
	 private int Areageografica;
	 private String Telefono;
	 private String Telefonomovil;
	 private String Emercontacto;
	 private String Emercontactotel;
	 private String Fecha_ingreso;
	 private String Usuario_ingreso;
	 private String Fecha_modificacion;
	 private String Usuario_modificacion;

	public CatpersonaMd( int IdL, String CodigoL, String CodempresaL, String NombresL, String ApellidosL, String SexoL, String TituloL, String PuestoL, String DireccionL, String EmailL, int AreageograficaL, String TelefonoL, String TelefonomovilL, String EmercontactoL, String EmercontactotelL, String Fecha_ingresoL, String Usuario_ingresoL, String Fecha_modificacionL, String Usuario_modificacionL)	{
		 this.Id = IdL;
		 this.Codigo = CodigoL;
		 this.Codempresa = CodempresaL;
		 this.Nombres = NombresL;
		 this.Apellidos = ApellidosL;
		 this.Sexo = SexoL;
		 this.Titulo = TituloL;
		 this.Puesto = PuestoL;
		 this.Direccion = DireccionL;
		 this.Email = EmailL;
		 this.Areageografica = AreageograficaL;
		 this.Telefono = TelefonoL;
		 this.Telefonomovil = TelefonomovilL;
		 this.Emercontacto = EmercontactoL;
		 this.Emercontactotel = EmercontactotelL;
		 this.Fecha_ingreso = Fecha_ingresoL;
		 this.Usuario_ingreso = Usuario_ingresoL;
		 this.Fecha_modificacion = Fecha_modificacionL;
		 this.Usuario_modificacion = Usuario_modificacionL;
	}

	 public int getId() {
		 return Id;
	}

	 public void setId(int Id) {
		 this.Id = Id;
	}

	 public String getCodigo() {
		 return Codigo;
	}

	 public void setCodigo(String Codigo) {
		 this.Codigo = Codigo;
	}

	 public String getCodempresa() {
		 return Codempresa;
	}

	 public void setCodempresa(String Codempresa) {
		 this.Codempresa = Codempresa;
	}

	 public String getNombres() {
		 return Nombres;
	}

	 public void setNombres(String Nombres) {
		 this.Nombres = Nombres;
	}

	 public String getApellidos() {
		 return Apellidos;
	}

	 public void setApellidos(String Apellidos) {
		 this.Apellidos = Apellidos;
	}

	 public String getSexo() {
		 return Sexo;
	}

	 public void setSexo(String Sexo) {
		 this.Sexo = Sexo;
	}

	 public String getTitulo() {
		 return Titulo;
	}

	 public void setTitulo(String Titulo) {
		 this.Titulo = Titulo;
	}

	 public String getPuesto() {
		 return Puesto;
	}

	 public void setPuesto(String Puesto) {
		 this.Puesto = Puesto;
	}

	 public String getDireccion() {
		 return Direccion;
	}

	 public void setDireccion(String Direccion) {
		 this.Direccion = Direccion;
	}

	 public String getEmail() {
		 return Email;
	}

	 public void setEmail(String Email) {
		 this.Email = Email;
	}

	 public int getAreageografica() {
		 return Areageografica;
	}

	 public void setAreageografica(int Areageografica) {
		 this.Areageografica = Areageografica;
	}

	 public String getTelefono() {
		 return Telefono;
	}

	 public void setTelefono(String Telefono) {
		 this.Telefono = Telefono;
	}

	 public String getTelefonomovil() {
		 return Telefonomovil;
	}

	 public void setTelefonomovil(String Telefonomovil) {
		 this.Telefonomovil = Telefonomovil;
	}

	 public String getEmercontacto() {
		 return Emercontacto;
	}

	 public void setEmercontacto(String Emercontacto) {
		 this.Emercontacto = Emercontacto;
	}

	 public String getEmercontactotel() {
		 return Emercontactotel;
	}

	 public void setEmercontactotel(String Emercontactotel) {
		 this.Emercontactotel = Emercontactotel;
	}

	 public String getFecha_ingreso() {
		 return Fecha_ingreso;
	}

	 public void setFecha_ingreso(String Fecha_ingreso) {
		 this.Fecha_ingreso = Fecha_ingreso;
	}

	 public String getUsuario_ingreso() {
		 return Usuario_ingreso;
	}

	 public void setUsuario_ingreso(String Usuario_ingreso) {
		 this.Usuario_ingreso = Usuario_ingreso;
	}

	 public String getFecha_modificacion() {
		 return Fecha_modificacion;
	}

	 public void setFecha_modificacion(String Fecha_modificacion) {
		 this.Fecha_modificacion = Fecha_modificacion;
	}

	 public String getUsuario_modificacion() {
		 return Usuario_modificacion;
	}

	 public void setUsuario_modificacion(String Usuario_modificacion) {
		 this.Usuario_modificacion = Usuario_modificacion;
	}

}
